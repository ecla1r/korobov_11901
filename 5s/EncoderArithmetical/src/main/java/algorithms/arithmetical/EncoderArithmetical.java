package algorithms.arithmetical;

import utils.Interval;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.zip.GZIPOutputStream;

public class EncoderArithmetical {

    HashMap<Character, BigDecimal> probMap;
    Map<Character, Interval> initIntervals;

    public Map<Character, Interval> getInitIntervals() {
        return initIntervals;
    }

    public EncoderArithmetical() {

    }

    public void encode(StringBuffer s) throws IOException {
        probMap = calcProb(s);
        initIntervals = fillInitArray(probMap);
        intervalsWriteFile(initIntervals);
        countRawCode(s, initIntervals);
        StringBuffer res = encodeRaw(s);
        writeFile(res);
        writeTxtFile(res);
    }

    public HashMap<Character, BigDecimal> calcProb(StringBuffer s) {
        HashMap<Character, BigDecimal> res = new HashMap<>();
        for(int i = 0; i < s.length(); i++) {
            if(!res.containsKey(s.charAt(i))) {
                res.put(s.charAt(i), BigDecimal.valueOf(1.0).divide(BigDecimal.valueOf(s.length()), 10, RoundingMode.UP));
            } else {
                BigDecimal d = res.get(s.charAt(i)).add(BigDecimal.valueOf(1.0).divide(BigDecimal.valueOf(s.length()), 10, RoundingMode.UP));
                res.put(s.charAt(i), d);
            }
        }
        BigDecimal sum = BigDecimal.valueOf(0);
        for (BigDecimal d : res.values()) {
            sum = sum.add(d);
        }
        return res;
    }

    public Map<Character, Interval> fillInitArray(HashMap<Character, BigDecimal> map) {
        HashMap<Character, Interval> initIntervals = new HashMap<>();
        BigDecimal curBottom = BigDecimal.valueOf(0);
        int i = 0;
        for(Map.Entry<Character, BigDecimal> entry : map.entrySet()) {
            Interval interval = new Interval(curBottom, curBottom.add(entry.getValue()), entry.getKey());
            initIntervals.put(entry.getKey(), interval);
            curBottom = curBottom.add(entry.getValue());
            i++;
        }
        return initIntervals;
    }

    public void countRawCode(StringBuffer s, Map<Character, Interval> initArr) {
        char c;
        for(int i = 0; i < s.length(); i++) {
            c = s.charAt(i);
            Interval interval = initArr.get(c);
            Interval cur = new Interval(interval.getBottom(), interval.getTop(), interval.getCh());
            BigDecimal begin = cur.getBottom();
            BigDecimal end;
            for(Interval x : initArr.values()) {
                BigDecimal len = x.getLen();
                BigDecimal NLen = len.multiply(cur.getLen());
                x.setBottom(begin);
                end = begin.add(NLen);
                x.setTop(end);
                begin = end;
            }
        }
    }

    public StringBuffer encodeRaw(StringBuffer s) {
        Interval interval = initIntervals.get(s.charAt(0));
        BigDecimal check = new BigDecimal(String.valueOf(0));
        StringBuffer res = new StringBuffer();
        BigDecimal div = new BigDecimal(String.valueOf(0.5));
        final BigDecimal TWO = new BigDecimal(2);
        while (true) {
            if(check.add(div).compareTo(interval.getTop()) >= 0) {
                res.append(0);
            } else {
                check = check.add(div);
                res.append(1);
            }
            if(check.compareTo(interval.getTop()) < 0 && check.compareTo(interval.getBottom()) > 0) {
                break;
            } else div = div.divide(TWO);
        }
        res.append(1);
        return res;
    }

    public void intervalsWriteFile(Map<Character, Interval> map) throws IOException {
        File file = new File("intervals.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        for(Map.Entry<Character, Interval> e : map.entrySet()) {
            bw.write(e.getKey() + "," + e.getValue().getBottom() + "," + e.getValue().getTop());
            bw.newLine();
        }
        bw.close();
    }

    public void writeFile(StringBuffer s) throws IOException {
        File file = new File("encoded.atm");
        GZIPOutputStream bw = new GZIPOutputStream(new FileOutputStream(file));
        bw.write(String.valueOf(s).getBytes());
        bw.close();
    }

    public void writeTxtFile(StringBuffer s) throws IOException {
        File file = new File("encoded.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(String.valueOf(s));
        bw.close();
    }

}
