package algorithms.arithmetical;

import utils.Interval;

import java.math.BigDecimal;
import java.util.Map;

public class DecoderArithmetical {

    StringBuffer encoded;
    Map<Character, Interval> initIntervals;

    public DecoderArithmetical(StringBuffer encoded, Map<Character, Interval> initIntervals) {
        this.encoded = encoded;
        this.initIntervals = initIntervals;
    }

    public StringBuffer decode() {
        StringBuffer decoded;
        BigDecimal start = countInit(encoded);
        decoded = decodeLength(start);
        return decoded;
    }

    public BigDecimal countInit(StringBuffer s) {
        BigDecimal res = BigDecimal.valueOf(0);
        BigDecimal div = new BigDecimal(String.valueOf(0.5));
        final BigDecimal TWO = new BigDecimal(2);
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '1') {
                res = res.add(div);
            }
            div = div.divide(TWO);
        }
        return res;
    }

    public StringBuffer decodeLength(BigDecimal num) {
        StringBuffer res = new StringBuffer();
        Character ch = ' ';
        while(true) {
            for(Map.Entry<Character, Interval> e : initIntervals.entrySet()) {
                if ((e.getValue().getBottom().compareTo(num) <= 0 && e.getValue().getTop().compareTo(num) >= 0)) {
                    ch = e.getKey();
                    break;
                }
            }
            Interval interval = initIntervals.get(ch);
            Interval cur = new Interval(interval.getBottom(), interval.getTop(), interval.getCh());
            BigDecimal begin = cur.getBottom();
            BigDecimal end;
            for(Interval x : initIntervals.values()) {
                BigDecimal len = x.getLen();
                BigDecimal NLen = len.multiply(cur.getLen());
                x.setBottom(begin);
                end = begin.add(NLen);
                x.setTop(end);
                begin = end;
            }
            if(ch.equals('~')) break;
            else res.append(ch);
        }
        return res;
    }
}
