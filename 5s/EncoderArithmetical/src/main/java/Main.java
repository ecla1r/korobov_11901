import algorithms.arithmetical.DecoderArithmetical;
import algorithms.arithmetical.EncoderArithmetical;
import utils.Interval;

import java.io.*;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;

import static java.lang.System.out;

public class Main {
    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        out.println("Кодировать?(1), Декодировать?(2)");
        int button = sc.nextInt();
        sc.nextLine();
        if (button == 1) {
            out.println("Путь к файлу для кодирования:");
            String path = sc.nextLine();
            StringBuffer input = readFile(path);
            EncoderArithmetical encoder = new EncoderArithmetical();
            encoder.encode(input);
        } else if (button == 2) {
            out.println("Путь к файлу для декодирования:");
            String pathToEncoded = sc.nextLine();
            out.println("Путь к файлу со словарем:");
            String pathToInts = sc.nextLine();
            DecoderArithmetical decoder = new DecoderArithmetical(readEncoded(pathToEncoded), readIntervals(pathToInts));
            StringBuffer output = decoder.decode();
            writeDecoded(output);
        }
    }

    public static StringBuffer readFile(String path) {
        File file = new File(path);
        StringBuffer s = new StringBuffer();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while((line = reader.readLine()) != null) {
                s.append(line);
                s.append("\n");
            }
            s.deleteCharAt(s.length() - 1);
            reader.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Wrong File Name");
        } catch (IOException e) {
            throw new IllegalArgumentException("Error");
        }
        s.append("~");
        return s;
    }

    public static Map<Character, Interval> readIntervals(String path) {
        File file = new File(path);
        HashMap<Character, Interval> s = new HashMap<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while((line = reader.readLine()) != null) {
                try {
                    if (line.charAt(0) != ',') {
                        String[] a = line.split(",");
                        s.put(a[0].charAt(0), new Interval(BigDecimal.valueOf(Double.parseDouble(a[1])), BigDecimal.valueOf(Double.parseDouble(a[2])), a[0].charAt(0)));
                    } else {
                        String[] a = line.split(",");
                        s.put(",".charAt(0), new Interval(BigDecimal.valueOf(Double.parseDouble(a[2])), BigDecimal.valueOf(Double.parseDouble(a[3])), ",".charAt(0)));
                    }
                } catch (IndexOutOfBoundsException e) {
                    line = reader.readLine();
                    String[] a = line.split(",");
                    s.put("\n".charAt(0), new Interval(BigDecimal.valueOf(Double.parseDouble(a[1])), BigDecimal.valueOf(Double.parseDouble(a[2])), "\n".charAt(0)));
                }

            }
            reader.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Wrong File Name");
        } catch (IOException e) {
            throw new IllegalArgumentException("Error");
        }
        return s;
    }

    public static StringBuffer readEncoded(String path) throws IOException {
        File file = new File(path);
        StringBuffer out = new StringBuffer();
        BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))));
        String line;
        while((line = br.readLine()) != null) {
            out.append(line);
        }
        return out;
    }

    public static void writeDecoded(StringBuffer s) throws IOException {
        File file = new File("output.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(String.valueOf(s));
        bw.close();
    }
}
