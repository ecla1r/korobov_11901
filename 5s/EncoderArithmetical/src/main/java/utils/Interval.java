package utils;

import java.math.BigDecimal;

public class Interval {

    private BigDecimal bottom;
    private BigDecimal top;
    private Character ch;
    private BigDecimal len;

    public Interval(BigDecimal bottom, BigDecimal top, Character ch) {
        this.bottom = bottom;
        this.top = top;
        this.ch = ch;
        this.len = top.subtract(bottom);
    }

    public Interval() {
        this.bottom = null;
        this.top = null;
        this.ch = null;
        this.len = null;
    }

    public BigDecimal getBottom() {
        return bottom;
    }

    public void setBottom(BigDecimal bottom) {
        this.bottom = bottom;
    }

    public BigDecimal getTop() {
        return top;
    }

    public void setTop(BigDecimal top) {
        this.top = top;
    }

    public Character getCh() {
        return ch;
    }

    public void setCh(Character ch) {
        this.ch = ch;
    }

    public BigDecimal getLen() {
        return len;
    }

    public void setLen(BigDecimal len) {
        this.len = len;
    }
}
