package main.utils.HuffmanTree;

public class Node {
    int value;
    Character ch;
    Node left;
    Node right;
    Boolean linked;
    String bytecode;

    public Node(int value, Character ch) {
        this.value = value;
        this.ch = ch;
        right = null;
        left = null;
        linked = false;
        bytecode = "";
    }

    public Node(int value, Node right, Node left) {
        this.value = value;
        this.right = right;
        this.left = left;
        linked = false;
        bytecode = "";
    }

    public Node(String bytecode, char ch) {
        value = 0;
        this.ch = ch;
        right = null;
        left = null;
        linked = false;
        this.bytecode = bytecode;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Character getCh() {
        return ch;
    }

    public void setCh(Character ch) {
        this.ch = ch;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public Boolean getLinked() {
        return linked;
    }

    public void setLinked(Boolean linked) {
        this.linked = linked;
    }

    public String getBytecode() {
        return bytecode;
    }

    public void setBytecode(String bytecode) {
        this.bytecode = bytecode;
    }

    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                ", ch=" + ch +
                ", bytecode='" + bytecode + '\'' +
                '}';
    }
}
