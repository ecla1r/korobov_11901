package main;

import main.algorithms.huffman.DecoderHuffman;
import main.algorithms.huffman.EncoderHuffman;

import java.io.*;
import java.util.Scanner;

import static java.lang.System.out;

public class Main {
    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        out.println("Кодировать?(1), Декодировать?(2)");
        int button = sc.nextInt();
        sc.nextLine();
        if (button == 1) {
            out.println("Путь к файлу для кодирования:");
            String path = sc.nextLine();
            StringBuffer input = readFile(path);
            EncoderHuffman encoder = new EncoderHuffman(path);
            encoder.encode(input);
        } else if (button == 2) {
            out.println("Путь к файлу для декодирования:");
            String pathToEncoded = sc.nextLine();
            out.println("Путь к файлу со словарем:");
            String pathToInts = sc.nextLine();
            DecoderHuffman decoder = new DecoderHuffman(pathToInts, pathToEncoded);
            decoder.decode();
        }
    }

    public static StringBuffer readFile(String path) {
        File file = new File(path);
        StringBuffer s = new StringBuffer();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while((line = reader.readLine()) != null) {
                s.append(line);
                s.append("\n");
            }
            reader.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Wrong File Name");
        } catch (IOException e) {
            throw new IllegalArgumentException("Error");
        }
        return s;
    }
}
