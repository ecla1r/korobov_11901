package main.algorithms.huffman;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import main.utils.HuffmanTree.Node;
import main.utils.MapSortUtil;

public class EncoderHuffman {

    LinkedList<Node> dict = new LinkedList<>();
    StringBuffer encodedText = new StringBuffer();
    String path;

    public EncoderHuffman(String path) {
        this.path = path;
    }

    public void encode(StringBuffer s) throws IOException {
        LinkedHashMap<Character, Integer> table = scan(s);
        dictBuilder(table);
        encodedText = byteEncoder(s);
        encodedTextWriter(encodedText);
        writeFile(encodedText);
        encodedDictWriter();
    }

    public LinkedHashMap<Character, Integer> scan(StringBuffer s) {
        LinkedHashMap<Character, Integer> result = new LinkedHashMap();
        for(int i = 0; i < s.length(); i++) {
            Character ch = s.charAt(i);
            if(!result.containsKey(ch)) result.put(ch, 1);
            else {
                int old = result.get(ch);
                old++;
                result.replace(ch, old);
            }
        }
        result = (LinkedHashMap<Character, Integer>) MapSortUtil.sortByValue(result);
        return result;
    }

    public void dictBuilder(LinkedHashMap<Character, Integer> table) {
        LinkedList<Node> nodeList = new LinkedList<>();
        for(Map.Entry<Character, Integer> e : table.entrySet()) {
            Node n = new Node(e.getValue(), e.getKey());
            nodeList.add(n);
        }
        Node curL;
        Node curR;
        while(!nodeList.get(1).getLinked()) {
            curL = nodeList.get(0);
            curR = nodeList.get(1);
            Node newNode = new Node(curL.getValue() + curR.getValue(), curL, curR);
            for(int i = 0; i < nodeList.size(); i++) {
                if (newNode.getValue() >= nodeList.get(i).getValue() && (newNode.getValue() < nodeList.get(i + 1).getValue() || nodeList.get(i + 1).getLinked())) {
                    nodeList.add(i + 1, newNode);
                    break;
                }
            }
            curL.setLinked(true);
            curR.setLinked(true);
            nodeList.add(curL);
            nodeList.add(curR);
            nodeList.removeFirst();
            nodeList.removeFirst();
        }
        setCodes(nodeList.getFirst(), "");
    }

    public void setCodes(Node root, String curCode) {
        if(!(root.getLeft() == null || root.getRight() == null)) {
            setCodes(root.getLeft(), curCode + "0");
            setCodes(root.getRight(), curCode + "1");
        } else {
            root.setBytecode(curCode);
            dict.add(root);
        }
    }

    public StringBuffer byteEncoder(StringBuffer s) {
        StringBuffer res = new StringBuffer();
        for(int i = 0; i < s.length(); i++) {
            for (Node n : dict) {
                if (n.getCh().equals(s.charAt(i))) {
                    res.append(n.getBytecode());
                    break;
                }
            }
        }
        return res;
    }

    public void encodedTextWriter(StringBuffer s) throws IOException {
        File file = new File("encoded.huf");
        GZIPOutputStream bw = new GZIPOutputStream(new FileOutputStream(file));
        bw.write(String.valueOf(s).getBytes());
        bw.close();
    }

    public void writeFile(StringBuffer s) throws IOException {
        File file = new File("encoded.txt");
        GZIPOutputStream bw = new GZIPOutputStream(new FileOutputStream(file));
        bw.write(String.valueOf(s).getBytes());
        bw.close();
    }

    public void encodedDictWriter() {
        File file = new File("dict.txt");
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            for(Node n : dict) {
                writer.write(n.getCh() + "," + n.getBytecode());
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
