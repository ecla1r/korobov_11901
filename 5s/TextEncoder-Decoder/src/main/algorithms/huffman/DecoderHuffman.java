package main.algorithms.huffman;

import main.utils.HuffmanTree.Node;

import java.io.*;
import java.util.LinkedList;
import java.util.zip.GZIPInputStream;

public class DecoderHuffman {

    String dictPath;
    String encodedPath;

    public DecoderHuffman(String dict_path, String encoded_path) {
        this.dictPath = dict_path;
        this.encodedPath = encoded_path;
    }

    public void decode() throws IOException {
        StringBuffer s = readText(encodedPath);
        LinkedList<Node> dict = readDict();
        StringBuffer decoded = byteDecoder(s, dict);
        writeDecoded(decoded);
    }

    public StringBuffer byteDecoder(StringBuffer s, LinkedList<Node> dict) {
        StringBuffer out = new StringBuffer();
        String curByteCode = "";
        for(int i = 0; i < s.length(); i++) {
            curByteCode += s.charAt(i);
            for (Node n : dict) {
                if (n.getBytecode().equals(curByteCode)) {
                    out.append(n.getCh());
                    curByteCode = "";
                    break;
                }
            }
        }
        return out;
    }

    public static StringBuffer readText(String path) throws IOException {
        File file = new File(path);
        StringBuffer out = new StringBuffer();
        BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))));
        String line;
        while((line = br.readLine()) != null) {
            out.append(line);
        }
        return out;
    }

    public LinkedList<Node> readDict() {
        LinkedList<Node> out = new LinkedList<>();
        File file = new File(dictPath);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            String[] str;
            while((line = reader.readLine()) != null) {
                try {
                    if (line.charAt(0) == ',') {
                        Node n = new Node(line.substring(2),',');
                        out.add(n);
                    } else {
                        str = line.split(",");
                        try {
                            Node n = new Node(str[1], str[0].charAt(0));
                            out.add(n);
                        } catch (StringIndexOutOfBoundsException e) {
                            Node n = new Node(str[1], "\0".charAt(0));
                            out.add(n);
                        }
                        catch (IndexOutOfBoundsException e) {
                            line = reader.readLine();
                            str = line.split(",");
                            Node n = new Node(str[1], "\n".charAt(0));
                            out.add(n);
                        }
                    }
                } catch (StringIndexOutOfBoundsException e) {
                    line = reader.readLine();
                    str = line.split(",");
                    Node n = new Node(str[1], "\n".charAt(0));
                    out.add(n);
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("File Not Found");
        } catch (IOException e) {
            throw new IllegalArgumentException("Error Reading The File");
        }
        return out;
    }

    public void writeDecoded(StringBuffer decoded) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"));
            writer.write(decoded.toString());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
