package algorithms.hamming74;

import java.util.ArrayList;

public class EncoderHamming {

    public EncoderHamming() {
    }

    public StringBuffer encode(StringBuffer in) {
        StringBuffer res = new StringBuffer();
        ArrayList<String> vectors = readVectors(in);
        addCheckers(vectors);
        addErrors(vectors);
        fixErrors(vectors);
        deleteCheckers(vectors);
        for (String s : vectors) {
            res.append(s);
        }
        return res;
    }

    public ArrayList<String> readVectors(StringBuffer in) {
        ArrayList<String> res = new ArrayList<>();
        for (int i = 0; i < in.length(); i+=4) {
            try {
                res.add(in.substring(i, i + 4));
            } catch (StringIndexOutOfBoundsException e) {
                //ignore
            }
        }
        return res;
    }

    public void addCheckers(ArrayList<String> list) {
        for(int j = 0; j < list.size(); j++) {
            int ones = 0;
            for (int i = 0; i < list.get(j).length(); i++) {
                if(list.get(j).charAt(i) == '1') ones++;
            }
            String c1 = String.valueOf((ones - Integer.parseInt(String.valueOf(list.get(j).charAt(2)))) % 2);
            String c2 = String.valueOf((ones - Integer.parseInt(String.valueOf(list.get(j).charAt(1)))) % 2);
            String c4 = String.valueOf((ones - Integer.parseInt(String.valueOf(list.get(j).charAt(0)))) % 2);
            list.set(j, c1 + c2 + list.get(j).charAt(0) + c4 + list.get(j).substring(1, 4));
        }
    }

    public void addErrors(ArrayList<String> list) {
        for(int i = 0; i < list.size(); i++) {
            int err = (int) (Math.random() * 7);
            StringBuilder newS = new StringBuilder();
            if (list.get(i).charAt(err) == '1') {
                newS.append(list.get(i));
                newS.replace(err, err, "0");
                newS.deleteCharAt(err + 1);
            }
            else {
                newS.append(list.get(i));
                newS.replace(err, err, "1");
                newS.deleteCharAt(err + 1);
            }
            list.set(i, String.valueOf(newS));
        }
    }

    public void fixErrors(ArrayList<String> list) {
        for(int i = 0; i < list.size(); i++) {
            String s = list.get(i);
            int s1 = Integer.parseInt(String.valueOf(s.charAt(0)));
            int s2 = Integer.parseInt(String.valueOf(s.charAt(1)));
            int s3 = Integer.parseInt(String.valueOf(s.charAt(2)));
            int s4 = Integer.parseInt(String.valueOf(s.charAt(3)));
            int s5 = Integer.parseInt(String.valueOf(s.charAt(4)));
            int s6 = Integer.parseInt(String.valueOf(s.charAt(5)));
            int s7 = Integer.parseInt(String.valueOf(s.charAt(6)));
            boolean bool1 = s1 == (s3 + s5 + s7) % 2;
            boolean bool2 = s2 == (s3 + s6 + s7) % 2;
            boolean bool4 = s4 == (s5 + s6 + s7) % 2;
            if(!bool1 && bool2 && bool4) {
                if(s1 == 0) s1 = 1;
                else s1 = 0;
            }
            if(bool1 && !bool2 && bool4) {
                if(s2 == 0) s2 = 1;
                else s2 = 0;
            }
            if(bool1 && bool2 && !bool4) {
                if(s4 == 0) s4 = 1;
                else s4 = 0;
            }
            if(!bool1 && !bool2 && !bool4) {
                if(s7 == 0) s7 = 1;
                else s7 = 0;
            }
            if(bool1 && !bool2 && !bool4) {
                if(s6 == 0) s6 = 1;
                else s6 = 0;
            }
            if(!bool1 && bool2 && !bool4) {
                if(s5 == 0) s5 = 1;
                else s5 = 0;
            }
            if(!bool1 && !bool2 && bool4) {
                if(s3 == 0) s3 = 1;
                else s3 = 0;
            }
            String ss1 = String.valueOf(s1);
            String ss2 = String.valueOf(s2);
            String ss3 = String.valueOf(s3);
            String ss4 = String.valueOf(s4);
            String ss5 = String.valueOf(s5);
            String ss6 = String.valueOf(s6);
            String ss7 = String.valueOf(s7);
            list.set(i, ss1 + ss2 + ss3 + ss4 + ss5 + ss6 + ss7);
        }
    }

    public void deleteCheckers(ArrayList<String> list) {
        for (int i = 0; i < list.size(); i++) {
            String newS = list.get(i).substring(2, 3) + list.get(i).substring(4, 7);
            list.set(i, newS);
        }
    }
}
