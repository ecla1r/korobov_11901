import algorithms.hamming74.EncoderHamming;

import java.io.*;
import java.util.Scanner;

import static java.lang.System.out;

public class Main {

    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        out.println("Путь к файлу для кодирования:");
        String path = sc.nextLine();
        StringBuffer input = readFile(path);
        EncoderHamming encoder = new EncoderHamming();
        StringBuffer out = encoder.encode(input);
        writeFile(out);
    }

    public static StringBuffer readFile(String path) {
        File file = new File(path);
        StringBuffer s = new StringBuffer();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while((line = reader.readLine()) != null) {
                s.append(line);
                s.append("\n");
            }
            reader.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Wrong File Name");
        } catch (IOException e) {
            throw new IllegalArgumentException("Error");
        }
        return s;
    }

    public static void writeFile(StringBuffer out) throws IOException {
        File file = new File("output.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(String.valueOf(out));
        bw.close();
    }
}
