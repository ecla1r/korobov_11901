package Tests;

import Generator.XMLReader;
import Helpers.ApplicationManager;
import Models.Account;
import Models.Poll;
import org.junit.Assert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.testng.annotations.Parameters;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class PollTestCase extends AuthBase {

    @ParameterizedTest
    @MethodSource("parseXML")
    public void createPollTest(String name, String desc, String ans1, String ans2) throws InterruptedException {
        Poll poll = new Poll(name, desc, ans1, ans2);
        applicationManager.getPollHelper().createPoll(poll);
        Poll newPoll = applicationManager.getPollHelper().getData();
        Assert.assertEquals(name, newPoll.getName());
        Assert.assertEquals(desc, newPoll.getDescription());
    }

    @ParameterizedTest
    @MethodSource("parseXML")
    public void deletePollTest(String name, String desc, String ans1, String ans2) {
        applicationManager.getPollHelper().deletePoll(name);
    }

    @Parameters
    public static Stream<Arguments> parseXML() {
        String filepath = "C:\\Users\\ecla1\\IntelliJIdeaProjects\\tests_01\\src\\main\\resources\\result.xml";
        File xmlFile = new File(filepath);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        List<Poll> prList = new ArrayList<>();
        try {
            builder = factory.newDocumentBuilder();
            Document document = builder.parse(xmlFile);
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("poll");
            for (int i = 0; i < nodeList.getLength(); i++) {
                prList.add(XMLReader.getRLanguage(nodeList.item(i)));
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return Stream.of(
                Arguments.of(prList.get(0).getName(), prList.get(0).getDescription(), prList.get(0).getAnswer1(), prList.get(0).getAnswer2())
        );
    }
}
