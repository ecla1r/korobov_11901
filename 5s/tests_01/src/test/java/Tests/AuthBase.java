package Tests;

import Helpers.ApplicationManager;
import Models.Account;
import org.junit.jupiter.api.BeforeAll;

public class AuthBase extends TestBase {

    @BeforeAll
    public static void setUp() {
        applicationManager = ApplicationManager.getInstance();
        applicationManager.getLoginHelper().login(new Account(ApplicationManager.getTestEmail(), ApplicationManager.getTestPassword(), ApplicationManager.getTestCredentials()));
    }
}
