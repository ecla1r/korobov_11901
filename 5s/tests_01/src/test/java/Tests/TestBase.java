package Tests;

import Helpers.ApplicationManager;
import org.junit.jupiter.api.BeforeAll;

public class TestBase {

    public static ApplicationManager applicationManager;

    @BeforeAll
    public static void setUp() {
        applicationManager = ApplicationManager.getInstance();
        applicationManager.getNavigationHelper().openHomePage();
    }

}
