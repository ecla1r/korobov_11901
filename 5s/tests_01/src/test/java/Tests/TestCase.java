package Tests;

import Helpers.ApplicationManager;
import Models.Account;
import Models.Poll;
import org.junit.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;

import org.junit.jupiter.api.Order;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;

import java.time.Duration;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCase extends TestBase {

    @Test
    @Order(1)
    public void t1loginTest() {
        applicationManager.getNavigationHelper().openHomePage();
        Account user = new Account("Ecla111r@gmail.com", "12345678");
        applicationManager.getLoginHelper().login(user);
    }

    @Test
    @Order(2)
    public void t2createPollTest() throws InterruptedException {
        applicationManager.getNavigationHelper().openHomePage();
        Account user = new Account("Ecla111r@gmail.com", "12345678");
        String name = "Test Poll";
        String desc = "This is a test!";
        Poll poll = new Poll(name, desc, "Ans1", "Ans2");
        applicationManager.getLoginHelper().login(user);
        applicationManager.getPollHelper().createPoll(poll);
        Poll newPoll = applicationManager.getPollHelper().getData();
        Assert.assertEquals(name, newPoll.getName());
        Assert.assertEquals(desc, newPoll.getDescription());
    }

    @Test
    @Order(3)
    public void t3deletePollTest() {
        applicationManager.getNavigationHelper().openHomePage();
        Account user = new Account("Ecla111r@gmail.com", "12345678");
        String name = "Test Poll";
        applicationManager.getLoginHelper().login(user);
        applicationManager.getPollHelper().deletePoll(name);
    }
}



