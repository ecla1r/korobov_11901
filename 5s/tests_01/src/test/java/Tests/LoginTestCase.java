package Tests;

import Generator.XMLReader;
import Helpers.ApplicationManager;
import Models.Account;
import Models.Poll;
import org.junit.*;

import org.junit.jupiter.api.Order;
import org.junit.runners.MethodSorters;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.testng.annotations.Parameters;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.*;
import java.util.stream.Stream;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginTestCase extends TestBase {

    @ParameterizedTest
    @MethodSource("parseXML")
    public void loginWithValidData() {
        Account user = new Account(ApplicationManager.getTestEmail(), ApplicationManager.getTestPassword(), ApplicationManager.getTestCredentials());
        applicationManager.getLoginHelper().login(user);
    }

    @ParameterizedTest
    @MethodSource("parseXML")
    public void loginWithInvalidData() {
        Account user = new Account(ApplicationManager.getTestEmail() + "err", ApplicationManager.getTestPassword(), ApplicationManager.getTestCredentials());
        applicationManager.getLoginHelper().loginWithError(user);
    }

    @Parameters
    public static Stream<Arguments> parseXML() {
        String filepath = "C:\\Users\\ecla1\\IntelliJIdeaProjects\\tests_01\\src\\main\\resources\\result.xml";
        File xmlFile = new File(filepath);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        List<Poll> prList = new ArrayList<>();
        try {
            builder = factory.newDocumentBuilder();
            Document document = builder.parse(xmlFile);
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("poll");
            for (int i = 0; i < nodeList.getLength(); i++) {
                prList.add(XMLReader.getRLanguage(nodeList.item(i)));
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return Stream.of(
                Arguments.of(prList.get(0).getName(), prList.get(0).getDescription(), prList.get(0).getAnswer1(), prList.get(0).getAnswer2())
        );
    }
}



