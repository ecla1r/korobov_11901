package Helpers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.JavascriptExecutor;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class ApplicationManager {

    private WebDriver driver;
    private Map<String, Object> vars;
    JavascriptExecutor js;
    private String baseURL;
    private NavigationHelper navigationHelper;
    private LoginHelper loginHelper;
    private PollHelper pollHelper;
    private static ThreadLocal<ApplicationManager> app = new ThreadLocal<>();
    private Properties p = new Properties();
    private static String testEmail;
    private static String testPassword;
    private static String testCredentials;

    private ApplicationManager() {
        setDriver();
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();
        baseURL = "https://strawpoll.com/en/";
        try {
            FileReader reader = new FileReader("C:\\Users\\ecla1\\IntelliJIdeaProjects\\tests_01\\src\\main\\resources\\testProps.properties");
            p.load(reader);
        } catch (FileNotFoundException e) {
            System.out.println("No File");
        } catch (IOException e) {
            e.printStackTrace();
        }
        testCredentials = p.getProperty("test.credentials");
        testEmail = p.getProperty("test.email");
        testPassword = p.getProperty("test.password");
        driver.manage().timeouts().implicitlyWait(5000,
                TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(10000,
                TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(5000,
                TimeUnit.MILLISECONDS);
        navigationHelper = new NavigationHelper(this);
        loginHelper = new LoginHelper(this);
        pollHelper = new PollHelper(this);
    }

    public void setDriver() {
        System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
        driver = new ChromeDriver();
    }

    public static ApplicationManager getInstance() {
        ApplicationManager instance = app.get();
        if (instance == null) {
            instance = new ApplicationManager();
            instance.getNavigationHelper().openHomePage();
            app.set(instance);
        }
        return instance;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public NavigationHelper getNavigationHelper() {
        return navigationHelper;
    }

    public LoginHelper getLoginHelper() {
        return loginHelper;
    }

    public PollHelper getPollHelper() {
        return pollHelper;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public void stop() {
        driver.quit();
    }

    public static void setApp(ThreadLocal<ApplicationManager> app) {
        ApplicationManager.app = app;
    }

    public static String getTestEmail() {
        return testEmail;
    }

    public static String getTestPassword() {
        return testPassword;
    }

    public static String getTestCredentials() {
        return testCredentials;
    }
}
