package Helpers;

import Models.Account;
import Models.Poll;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;

import java.time.Duration;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class PollHelper extends HelperBase {

    public PollHelper(ApplicationManager applicationManager) {
        super(applicationManager);
    }

    public void createPoll(Poll poll) throws InterruptedException {
        driver.findElement(By.cssSelector(".navbar-item > svg")).click();
        driver.findElement(By.linkText("Create a Poll")).click();
        driver.findElement(By.cssSelector(".input:nth-child(2)")).sendKeys(poll.name);
        driver.findElement(By.cssSelector(".textarea")).click();
        driver.findElement(By.cssSelector(".textarea")).sendKeys(poll.description);
        driver.findElement(By.cssSelector(".field:nth-child(1) > .input:nth-child(1)")).click();
        driver.findElement(By.cssSelector(".field:nth-child(1) > .input:nth-child(1)")).sendKeys(poll.answer1);
        driver.findElement(By.cssSelector(".field:nth-child(2) > .input")).click();
        driver.findElement(By.cssSelector(".field:nth-child(2) > .input")).sendKeys(poll.answer2);
        driver.findElement(By.cssSelector(".block:nth-child(4) > .field:nth-child(2) .check")).click();
        driver.findElement(By.id("create-poll")).click();
        Thread.sleep(2000);
    }

    public void deletePoll(String pollName) {
        driver.findElement(By.cssSelector(".navbar-item:nth-child(1) > .navbar-link")).click();
        driver.findElement(By.linkText(pollName)).click();
        driver.findElement(By.cssSelector(".options-button")).click();
        driver.findElement(By.cssSelector(".dropdown-item:nth-child(5) h3")).click();
        driver.findElement(By.cssSelector(".is-danger")).click();
        driver.findElement(By.linkText("Go to the start page")).click();
    }

    public Poll getData() {
        driver.findElement(By.cssSelector(".navbar-link > .is-next-to-icon")).click();
        String name = driver.findElement(By.cssSelector(".break-word")).getText();
        driver.findElement(By.cssSelector(".break-word")).click();
        String desc = driver.findElement(By.cssSelector(".pd")).getText();
        return new Poll(name, desc);
    }
}
