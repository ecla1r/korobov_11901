package Helpers;

import Models.Account;
import Models.Poll;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;

import java.time.Duration;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class LoginHelper extends HelperBase {

    public LoginHelper(ApplicationManager applicationManager) {
        super(applicationManager);
    }

    public void login(Account user) {
        if(isLoggedIn()) {
            if (isLoggedIn(user.credentials)) return;
            logout();
        }
        try {
            driver.findElement(By.linkText("Log In")).click();
            driver.findElement(By.cssSelector(".field:nth-child(1) .input")).click();
            driver.findElement(By.cssSelector(".field:nth-child(1) .input")).sendKeys(user.login);
            driver.findElement(By.cssSelector(".has-icons-right > .input")).click();
            driver.findElement(By.cssSelector(".has-icons-right > .input")).sendKeys(user.password);
            driver.findElement(By.cssSelector(".is-primary")).click();
        } catch (NoSuchElementException e) {
            //ignore
        }
    }

    public void loginWithError(Account user) {
        if(isLoggedIn()) {
            logout();
        }
        login(user);
        driver.findElement(By.linkText("Log In")).click();
    }

    public void logout() {
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(By.xpath("//div[@id='header']/nav/div[3]/div[2]/div/a/span")));
            actions.perform();
            driver.findElement(By.linkText("Logout")).click();
    }

    public boolean isLoggedIn() {
        try {
            driver.findElement(By.linkText("Log In")).click();
            return false;
        } catch (NoSuchElementException e) {
            return true;
        }
    }

    public boolean isLoggedIn(String credentials) {
        return driver.findElement(By.xpath("//span[contains(.,'" + credentials + "')]")).isDisplayed();
    }
}
