package Models;

public class Poll {

    public String name;
    public String description;
    public String answer1;
    public String answer2;

    public Poll(String name, String description, String answer1, String answer2) {
        this.name = name;
        this.description = description;
        this.answer1 = answer1;
        this.answer2 = answer2;
    }

    public Poll(String name, String description) {
        this.name = name;
        this.description = description;
        this.answer1 = null;
        this.answer2 = null;
    }

    public Poll() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }
}
