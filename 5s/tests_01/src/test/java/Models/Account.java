package Models;

public class Account {

    public String login;
    public String password;
    public String credentials;

    public Account (String login, String password, String credentials) {
        this.login = login;
        this.password = password;
        this.credentials = credentials;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }
}
