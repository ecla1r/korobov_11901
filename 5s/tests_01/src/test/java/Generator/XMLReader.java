package Generator;

import Models.Poll;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLReader {

    public static Poll getRLanguage(Node node) {
        Poll poll = new Poll();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            poll.setName(getTagValue("name", element));
            poll.setDescription(getTagValue("desc", element));
            poll.setAnswer1(getTagValue("ans1", element));
            poll.setAnswer2(getTagValue("ans2", element));
        }
        return poll;
    }

    public static String getTagValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodeList.item(0);
        return node.getNodeValue();
    }
}
