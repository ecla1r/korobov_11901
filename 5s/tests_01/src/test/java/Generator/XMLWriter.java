package Generator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class XMLWriter {

    // метод для создания нового узла XML-файла
    public static Node getWLanguage(Document doc, String id, String name, String desc, String ans1, String ans2) {
        Element language = doc.createElement("poll");

        language.setAttribute("id", id);

        language.appendChild(getLanguageElements(doc, language, "name", name));

        language.appendChild(getLanguageElements(doc, language, "desc", desc));

        language.appendChild(getLanguageElements(doc, language, "ans1", ans1));

        language.appendChild(getLanguageElements(doc, language, "ans2", ans2));
        return language;
    }


    // утилитный метод для создание нового узла XML-файла
    public static Node getLanguageElements(Document doc, Element element, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }
}
