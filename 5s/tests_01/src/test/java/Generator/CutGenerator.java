package Generator;

import Models.Poll;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CutGenerator {

    private static final String[] polls = {"Banana Poll", "Cherry Poll", "Grape Poll"};

    public static void main(String[] args) {
        String filename = "result";
        int count = 1;
        generateForProducts(count, filename);
    }

    public static void generateForProducts(int count, String filename) {
        List<Poll> listOfPolls = new ArrayList<>();
        int indexOfPoll;
        String desc;
        String ans1;
        String ans2;
        for (int i = 0; i < count; i++) {
            indexOfPoll = (int) ( Math.random() * polls.length);
            int num = (int) (Math.random() * 10);
            desc = "This is a description for " + polls[indexOfPoll];
            ans1 = "Answer " + num;
            ans2 = "Another answer " + num;
            listOfPolls.add(new Poll(polls[indexOfPoll], desc, ans1, ans2));
        }
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();
            Element rootElement =
                    doc.createElementNS("Polls", "root");
            doc.appendChild(rootElement);
            int c1 = 0;
            for (int i = 0; i < listOfPolls.size(); i++) {
                rootElement.appendChild(XMLWriter.getWLanguage(doc, String.valueOf(c1), listOfPolls.get(i).getName(),
                        String.valueOf(listOfPolls.get(i).getDescription()),
                        String.valueOf(listOfPolls.get(i).getAnswer1()),
                        String.valueOf(listOfPolls.get(i).getAnswer2())));
                c1++;
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult console = new StreamResult(System.out);
            StreamResult file = new StreamResult(new File("C:\\Users\\ecla1\\IntelliJIdeaProjects\\tests_01\\src\\main\\resources\\" + filename + ".xml"));
            transformer.transform(source, console);
            transformer.transform(source, file);
            System.out.println("Создание XML файла закончено");
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

}
