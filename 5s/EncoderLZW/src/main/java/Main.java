import algorithms.lzw.DecoderLZW;
import algorithms.lzw.EncoderLZW;

import java.io.*;
import java.util.Scanner;

import static java.lang.System.out;

public class Main {
    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        out.println("Кодировать?(1), Декодировать?(2)");
        int button = sc.nextInt();
        sc.nextLine();
        if (button == 1) {
            out.println("Путь к файлу для кодирования:");
            String path = sc.nextLine();
            StringBuffer input = readFile(path);
            EncoderLZW encoder = new EncoderLZW();
            encoder.encode(input);
        } else if (button == 2) {
            out.println("Путь к файлу для декодирования:");
            String pathToEncoded = sc.nextLine();
            out.println("Путь к файлу со словарем:");
            String pathToInts = sc.nextLine();
            DecoderLZW decoder = new DecoderLZW();
            StringBuffer output = decoder.decode(pathToEncoded, pathToInts);
            writeFile(output);
        }
    }

    public static StringBuffer readFile(String path) {
        File file = new File(path);
        StringBuffer s = new StringBuffer();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while((line = reader.readLine()) != null) {
                s.append(line);
                s.append("\n");
            }
            s.deleteCharAt(s.length() - 1);
            reader.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Wrong File Name");
        } catch (IOException e) {
            throw new IllegalArgumentException("Error");
        }
        return s;
    }

    public static void writeFile(StringBuffer out) throws IOException {
        File file = new File("output.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(String.valueOf(out));
        bw.close();
    }
}
