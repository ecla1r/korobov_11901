package algorithms.lzw;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class DecoderLZW {

    public DecoderLZW() {
    }

    public StringBuffer decode(String encodedPath, String dictPath) throws IOException {
        HashMap<String, String> dict = dictReader(dictPath);
        StringBuffer encoded = encodedReader(encodedPath);
        return decodeBytes(encoded, dict);
    }

    public StringBuffer decodeBytes(StringBuffer encoded, HashMap<String, String> dict) {
        StringBuffer res = new StringBuffer();
        int len = (int) Math.ceil(Math.log(dict.size() + 1) / Math.log(2));
        String x = dict.get(encoded.substring(0, len));
        String buf = x;
        for (int i = len; i < encoded.length(); i+=len) {
            String y;
            try {
                if(dict.containsKey(encoded.substring(i, i + len))) {
                    y = dict.get(encoded.substring(i, i + len));
                    if(y.equals("~")) {
                        res.append(x);
                    }
                    else {
                        try {
                            buf += y.charAt(0);
                        } catch (StringIndexOutOfBoundsException e) {
                            buf += "\n";
                        }
                        if(dict.containsValue(buf)) {
                            x = buf;
                        }
                        else {
                            res.append(x);
                            dict.put(Integer.toBinaryString(dict.size()), buf);
                            x = y;
                            buf = x;
                            if (dict.size() == Math.pow(2, len)) {
                                HashMap<String, String> newDict = new HashMap<>();
                                for (Map.Entry<String, String> e : dict.entrySet()) {
                                    if (e.getKey().length() < len + 1) {
                                        newDict.put("0" + e.getKey(), e.getValue());
                                    } else newDict.put(e.getKey(), e.getValue());
                                }
                                dict = newDict;
                                len++;
                                i -= 1;
                            }
                        }
                    }
                } else {
                    int newLen = x.length() + 1;
                    y = (x + x).substring(0, newLen);
                    try {
                        buf = y;
                    } catch (StringIndexOutOfBoundsException e) {
                        buf += "\n";
                    }
                    if(dict.containsValue(buf)) {
                        x = buf;
                    }
                    else {
                        res.append(x);
                        dict.put(Integer.toBinaryString(dict.size()), buf);
                        x = y;
                        buf = x;
                        if (dict.size() == Math.pow(2, len)) {
                            HashMap<String, String> newDict = new HashMap<>();
                            for (Map.Entry<String, String> e : dict.entrySet()) {
                                if (e.getKey().length() < len + 1) {
                                    newDict.put("0" + e.getKey(), e.getValue());
                                } else newDict.put(e.getKey(), e.getValue());
                            }
                            dict = newDict;
                            len++;
                            i -= 1;
                        }
                    }
                }
            } catch (StringIndexOutOfBoundsException e) {
                System.out.println("OUT");
            }
        }
        return res;
    }

    public HashMap<String, String> dictReader(String path) throws IOException {
        HashMap<String, String> res = new HashMap<>();
        File file = new File(path);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        String str[];
        while ((line = br.readLine()) != null) {
            try {
                str = line.split("↻");
                res.put(str[1], str[0]);
            } catch (ArrayIndexOutOfBoundsException e) {
                line = br.readLine();
                str = line.split("↻");
                res.put(str[1], "\n");
            }

        }
        br.close();
        return res;
    }

    public StringBuffer encodedReader(String path) throws IOException {
        File file = new File(path);
        StringBuffer out = new StringBuffer();
        BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))));
        String line;
        while((line = br.readLine()) != null) {
            out.append(line);
        }
        return out;
    }
}
