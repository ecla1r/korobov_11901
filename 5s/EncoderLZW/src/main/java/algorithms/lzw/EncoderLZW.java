package algorithms.lzw;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

public class EncoderLZW {

    HashMap<String, String> dict;

    public EncoderLZW() {
    }

    public void encode(StringBuffer input) throws IOException {
        input.append("~");
        dict = fillInitDict(input);
        writeDict(dict);
        StringBuffer res = encodeText(input, dict);
        writeEncoded(res);
        writeTxtFile(res);
    }

    public HashMap<String, String> fillInitDict(StringBuffer input) {
        HashMap<String, String> res = new HashMap<>();
        HashSet<Character> set = new HashSet<>();
        for(int i = 0; i < input.length(); i++) {
            set.add(input.charAt(i));
        }
        int n = set.size();
        int pow = 0;
        while (n > Math.pow(2, pow)) {
            pow++;
        }
        int i = 0;
        for(Character c : set) {
            String bc = Integer.toBinaryString(i);
            if (bc.length() < pow) {
                String add = "";
                for(int j = 0; j < pow - bc.length(); j++) {
                    add = add.concat("0");
                }
                bc = add.concat(bc);
            }
            res.put(c.toString(), bc);
            i++;
        }
        return res;
    }

    public StringBuffer encodeText(StringBuffer input, HashMap<String, String> dict) {
        StringBuffer res = new StringBuffer();
        String x = String.valueOf(input.charAt(0));
        String buf = x;
        for(int i = 1; i < input.length(); i++) {
            String y = String.valueOf(input.charAt(i));
            if (y.equals("~")) {
                res.append(dict.get(x));
                res.append(dict.get("~"));
            }
            else {
                buf += y;
                if (dict.containsKey(buf)) {
                    x = buf;
                }
                else {
                    res.append(dict.get(x));
                    dict.put(buf, Integer.toBinaryString(dict.size()));
                    x = y;
                    buf = x;
                    int p = 0;
                    int size = dict.size() - 1;
                    while(size >= Math.pow(2, p)) {
                        if(size == Math.pow(2, p)) {
                            for (Map.Entry<String, String> e : dict.entrySet()) {
                                if (e.getValue().length() < p + 1) {
                                    e.setValue("0" + e.getValue());
                                }
                            }
                        }
                        p++;
                    }
                }
            }
        }
        return res;
    }

    public void writeDict(HashMap<String, String> dict) throws IOException {
        File file = new File("dict.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        for (Map.Entry<String, String> e : dict.entrySet()) {
            bw.write(e.getKey() + "↻" + e.getValue());
            bw.newLine();
        }
        bw.close();
    }

    public void writeEncoded(StringBuffer s) throws IOException {
        File file = new File("encoded.lzw");
        GZIPOutputStream bw = new GZIPOutputStream(new FileOutputStream(file));
        bw.write(String.valueOf(s).getBytes());
        bw.close();
    }

    public void writeTxtFile(StringBuffer s) throws IOException {
        File file = new File("encoded.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(String.valueOf(s));
        bw.close();
    }
}
