package ru.itis.docspdfgenerator;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class SecondTypeGenerator {

    public static void generate(String content) throws DocumentException, FileNotFoundException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(new File("/usr/bin" + content + "-type2.pdf")));

        document.open();
        Font font = FontFactory.getFont(FontFactory.HELVETICA, 30, BaseColor.RED);
        Paragraph p = new Paragraph(content + " is the most influential person in the world (official)", font);

        document.add(p);
        document.close();
    }
}
