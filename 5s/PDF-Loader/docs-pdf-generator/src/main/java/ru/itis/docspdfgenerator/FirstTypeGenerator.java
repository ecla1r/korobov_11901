package ru.itis.docspdfgenerator;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class FirstTypeGenerator {

    public static void generate(String content) throws DocumentException, FileNotFoundException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(new File("/usr/bin" + content + "-type1.pdf")));

        document.open();
        Font font = FontFactory.getFont(FontFactory.TIMES, 30, BaseColor.BLACK);
        Paragraph p = new Paragraph((content + " is the most stupid person in the world (unofficial)"), font);

        document.add(p);
        document.close();
    }
}
