package ru.itis.docspdfgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocsPdfGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(DocsPdfGeneratorApplication.class, args);
    }

}
