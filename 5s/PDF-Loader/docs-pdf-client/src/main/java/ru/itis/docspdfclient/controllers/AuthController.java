package ru.itis.docspdfclient.controllers;

import ru.itis.docspdfclient.dto.AuthDto;
import ru.itis.docspdfclient.dto.TokenDto;
import ru.itis.docspdfclient.services.AuthorizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthController {

    private final AuthorizationService authorizationService;

    @PostMapping(value = "/signUp", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TokenDto signUp(@RequestBody AuthDto authorizationDto) {

        return authorizationService.signUp(authorizationDto.getUsername(), authorizationDto.getPassword());
    }

    @PostMapping(value = "/signIn", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TokenDto signIn(@RequestBody AuthDto authorizationDto) {
        return authorizationService.signIn(authorizationDto.getUsername(), authorizationDto.getPassword());
    }
}
