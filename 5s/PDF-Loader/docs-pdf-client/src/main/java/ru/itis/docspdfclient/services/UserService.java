package ru.itis.docspdfclient.services;

import ru.itis.docspdfclient.models.User;

public interface UserService {

    User save(String username, String password);

    User findById(Long id);

    User findByUsername(String username);
}
