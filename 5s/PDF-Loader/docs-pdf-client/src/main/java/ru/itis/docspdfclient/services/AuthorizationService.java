package ru.itis.docspdfclient.services;

import ru.itis.docspdfclient.dto.TokenDto;

public interface AuthorizationService {

    TokenDto signUp(String username, String password);

    TokenDto signIn(String username, String password);
}
