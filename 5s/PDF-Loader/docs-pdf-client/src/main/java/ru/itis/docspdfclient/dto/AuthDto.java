package ru.itis.docspdfclient.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AuthDto {

    private final String username;
    private final String password;
}
