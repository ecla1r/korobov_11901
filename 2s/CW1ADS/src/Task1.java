import java.util.HashSet;
import java.util.Scanner;

public class Task1 {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        int n = sc.nextInt();
        int [] arr = new int[n];
        for (int i = 0; i < n; i++) {
            int cur = sc.nextInt();
            int out = 0;
            while (cur > 0) {
                out += cur % k;
                cur /= k;
            }
            arr[i] = out;
        }
        HashSet<Integer> finalset = new HashSet<>();
        for (int x : arr) {
            finalset.add(x);
        }
        System.out.println(finalset.size());
    }
}