import java.util.Scanner;

public class Task2 {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int [] arr = new int [n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        int prev = 3;
        int chill = 0;

        for (int i = 0; i < arr.length; i++) {
            if (prev != 3 && arr[i] == prev) {
                arr[i] = 0;
            }
            else if (arr[i] == 3 && prev != 3) {
                arr[i] -= prev;
            }
            if (arr[i] == 0) {
                chill++;
            }
            prev = arr[i];
        }
        System.out.println(chill);
    }
}
