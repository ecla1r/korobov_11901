import java.util.Scanner;

public class TaskC {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int m = sc.nextInt();
        int [] a = new int[n];

        int index;
        int duration = 0;
        int pivot = 0;

        for (int i = 0; i < n; i++) {
            index = sc.nextInt();
            duration += sc.nextInt() * index;
            a[i] = duration;
        }
        for (int i = 0; i < m; i++) {
            index = sc.nextInt();
            for (int j = pivot; j < n; j++) {
                if (index <= a[j]) {
                    System.out.print(j + 1 + "\n");
                    pivot = j;
                    break;
                }
            }
        }
    }
}
