import java.util.Scanner;

public class TaskA {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int [] price = new int[(int) 1e6 + 1];

        for (int i = 0; i < n; i++) {
            price[sc.nextInt()]++;
        }

        int sum = 0;

        for (int i = 0; i < price.length; i++) {
            sum += price[i];
            price[i] = sum;
        }

        int q = sc.nextInt();
        for (int i = 0; i < q; i++) {
            System.out.println(price[Math.min(sc.nextInt(), price.length - 1)]);
        }
    }
}
