import java.util.*;

public class TaskB {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int l = sc.nextInt();
        TreeSet<Integer> lamps = new TreeSet<>();

        for (int i = 0; i < n; i++) {
            lamps.add(sc.nextInt());
        }

        double rad = 0;
        if (lamps.first() > rad) {
            rad = lamps.first();
        }
        if (l - lamps.last() > lamps.first() && l - lamps.last() > rad) {
            rad = l - lamps.last();
        }
        int prev = lamps.first();
        for (int x : lamps) {
            if ((double)(x - prev) / 2 > rad) {
                rad =  (double)(x - prev) / 2;
            }
            prev = x;
        }
        System.out.println(rad);
    }
}
