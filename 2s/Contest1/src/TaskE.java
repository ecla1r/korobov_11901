import java.util.ArrayList;
import java.util.Scanner;

public class TaskE {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        sc.nextLine();
        ArrayList<String> pol = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            pol.add(sc.nextLine());
        }

        int common = 0;
        ArrayList<String> pol2 = new ArrayList<>();
        for (int j = 0; j < m; j++) {
            String w = sc.nextLine();
            if (pol.contains(w)) {
                common++;
            }
            pol2.add(w);
        }

        if (pol.size() > pol2.size()) {
            System.out.println("YES");
        }
        else if (pol.size() == pol2.size()) {
            if (common % 2 == 0) {
                System.out.println("NO");
            } else System.out.println("YES");
        } else System.out.println("NO");
    }
}
