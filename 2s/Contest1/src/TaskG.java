import java.util.Scanner;

public class TaskG {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        final int size = (int) 1e6 + 1;
        int[] lights = new int[size];
        int[] f = new int[size];
        for (int i = 0; i < n; i++)
            lights[in.nextInt()] = in.nextInt();

        f[0] = lights[0] > 0 ? 1 : 0;
        int max = 0;

        for (int i = 1; i < size; i++) {
            if (lights[i] == 0)
                f[i] = f[i - 1];
            else {
                if (lights[i] >= i)
                    f[i] = 1;
                else
                    f[i] = f[i - lights[i] - 1] + 1;
            }
            max = Math.max(f[i], max);
        }

        System.out.println(n - max);
    }
}