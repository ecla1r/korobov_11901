import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class TaskD {

    public static void main(String[] args) {

        Map<Integer, Integer> cities = new TreeMap<>();
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int population = in.nextInt();
        int all = 0;

        for (int i = 0; i < n; i++) {
            int x = in.nextInt();
            int y = in.nextInt();
            Integer key = x * x + y * y;
            Integer popl = in.nextInt();
            all += popl;
            cities.put(key, cities.get(key) == null ? popl : cities.get(key) + popl);
        }

        if (1e+6 > population + all)
            System.out.println(-1);
        else {
            for (Map.Entry<Integer, Integer> entry : cities.entrySet()) {
                if (entry.getValue() + population >= 1e+6) {
                    Double x = Math.sqrt(entry.getKey());
                    System.out.println(x);
                    break;
                } else
                    population += entry.getValue();
            }
        }
    }
}