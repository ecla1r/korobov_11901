import java.util.LinkedHashSet;
import java.util.Scanner;

public class TaskF {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        String [] arr = new String[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextLine();
        }
        LinkedHashSet<String> hs = new LinkedHashSet<>();
        for (int i = 0; i < n; i++) {
            hs.add(arr[n - 1 - i]);
        }
        for (String x : hs) {
            System.out.println(x);
        }
    }
}
