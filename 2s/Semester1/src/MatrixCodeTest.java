import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class MatrixCodeTest {

    @Test
    public void decodeTest() {
        int [][] a = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
        MatrixCode test = new MatrixCode(a);
        Assert.assertArrayEquals(a, test.decode());
    }

    @Test
    public void insertTest() {
        int [][] a = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
        MatrixCode test = new MatrixCode(a);
        int i = (int) Math.random() * test.getSize();
        int j = (int) Math.random() * test.getSize();
        int value = (int) Math.random() * 100;
        test.insert(i, j, value);
        Elem p = test.getList().get(i * test.getSize() + j);
        Assert.assertEquals(value, p.getValue());
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void insertExceptionTest1() {
        int [][] a = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
        MatrixCode test = new MatrixCode(a);
        test.insert(-1, 1, 10);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void insertExceptionTest2() {
        int [][] a = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
        MatrixCode test = new MatrixCode(a);
        test.insert(1, 3, 10);
    }

    @Test
    public void deleteTest() {
        int [][] a = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
        MatrixCode test = new MatrixCode(a);
        int i = (int) Math.random() * test.getSize();
        int j = (int) Math.random() * test.getSize();
        test.delete(i, j);
        Elem p = test.getList().get(i * test.getSize() + j);
        Assert.assertEquals(0, p.getValue());
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void deleteExceptionTest1() {
        int [][] a = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
        MatrixCode test = new MatrixCode(a);
        test.delete(-1, 1);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void deleteExceptionTest2() {
        int [][] a = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
        MatrixCode test = new MatrixCode(a);
        test.delete(1, 3);
    }

    @Test
    public void minList() {
        int [][] a = {{0, 7, 3}, {4, 8, 1}, {3, 3, 12}};
        MatrixCode test = new MatrixCode(a);
        java.util.ArrayList<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(3);
        list.add(1);
        Assert.assertEquals(list.get(0), test.minList().get(0));
        Assert.assertEquals(list.get(1), test.minList().get(1));
        Assert.assertEquals(list.get(2), test.minList().get(2));
    }

    @Test
    public void sumDiagTestOdd() {
        int [][] a = {{15, 30, 45}, {22, 44, 66}, {4, 16, 64}};
        MatrixCode test = new MatrixCode(a);
        Assert.assertEquals(172, test.sumDiag());
    }

    @Test
    public void sumDiagTestEven() {
        int [][] a = {{15, 30, 45, 60}, {22, 44, 66, 88}, {4, 16, 64, 256}, {121, 144, 169, 196}};
        MatrixCode test = new MatrixCode(a);
        Assert.assertEquals(582, test.sumDiag());
    }

    @Test
    public void transpTest() {
        int [][] a1 = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
        int [][] a2 = a1;
        MatrixCode test1 = new MatrixCode(a1);
        MatrixCode test2 = new MatrixCode(a2);
        test1.transp();
        for (int i = 0; i < test1.getSize(); i++) {
            for (int j = 0; j < test1.getSize(); j++) {
                Assert.assertEquals(test1.getList().get(i * test1.getSize() + j).getValue(),
                                    test2.getList().get(j * test2.getSize() + i).getValue());
            }
        }
    }

    @Test
    public void sumColsTest() {
        int [][] a = {{15, 30, 45, 60}, {22, 44, 66, 88}, {4, 16, 64, 256}, {121, 144, 169, 196}};
        MatrixCode test = new MatrixCode(a);
        int j1 = 2;
        int j2 = 3;
        test.sumCols(j1, j2);
        Assert.assertEquals(105, test.getList().get(0 * test.getSize() + j1).getValue());
        Assert.assertEquals(154, test.getList().get(1 * test.getSize() + j1).getValue());
        Assert.assertEquals(320, test.getList().get(2 * test.getSize() + j1).getValue());
        Assert.assertEquals(365, test.getList().get(3 * test.getSize() + j1).getValue());
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void sumColsExceptionTest1() {
        int [][] a = {{15, 30, 45, 60}, {22, 44, 66, 88}, {4, 16, 64, 256}, {121, 144, 169, 196}};
        MatrixCode test = new MatrixCode(a);
        int j1 = -1;
        int j2 = 3;
        test.sumCols(j1, j2);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void sumColsExceptionTest2() {
        int [][] a = {{15, 30, 45, 60}, {22, 44, 66, 88}, {4, 16, 64, 256}, {121, 144, 169, 196}};
        MatrixCode test = new MatrixCode(a);
        int j1 = 2;
        int j2 = 4;
        test.sumCols(j1, j2);
    }
}
