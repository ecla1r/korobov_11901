import java.util.Scanner;

public class Main {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        final int N = sc.nextInt(); // matrix's  size
        int [][] a = new int [N][N]; //input array
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                a[i][j] = sc.nextInt();
            }
        }

        MatrixCode matrix = new MatrixCode(a); //конструктор
        System.out.println("Matrix:");
        matrix.printMatrix();

        int [][] arr = matrix.decode(); //decode
        System.out.println("Array After decode():");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        matrix.insert(1, 1, 9); //insert
        System.out.println("Insert : Number 9 on position(1, 1):");
        matrix.printMatrix();

        matrix.delete(2, 1); //delete
        System.out.println("Delete : Element from position(2, 1):");
        matrix.printMatrix();

        System.out.println(matrix.minList().get(0) + " - first column's minimum\n"); //minList - 3 times
        System.out.println(matrix.minList().get(1) + " - second column's minimum\n");
        System.out.println(matrix.minList().get(2) + " - third column's minimum\n");

        System.out.println(matrix.sumDiag() + " - Diagonal Elements' sum\n"); //sumDiag

        matrix.transp(); //transp
        System.out.println("Transposed Matrix:");
        matrix.printMatrix();

        matrix.sumCols(1, 2); //sumCols
        System.out.println("Added Second Column's values to the first:");
        matrix.printMatrix();
    }
}
