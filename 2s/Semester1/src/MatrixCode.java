import java.util.ArrayList;

public class MatrixCode {

    private ArrayList<Elem> a1 = new ArrayList<>();
    private int size;

    MatrixCode(int [][] matrix) {
        this.size = matrix.length;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                Elem p = new Elem(i, j, matrix[i][j]);
                this.a1.add(p);
            }
        }
    }

    public void printMatrix() {
        for (int i = 0; i < this.getSize(); i++) {
            for (int j = 0; j < this.getSize(); j++) {
                System.out.print(this.decode()[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public int [][] decode() {
        int [][] a = new int [size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                a[i][j] = a1.get(i * size + j).getValue();
            }
        }
        return a;
    }

    public void insert(int i, int j, int value) {
        if (i < 0 || i > size - 1 || j < 0 || j > size - 1) {
            throw new IndexOutOfBoundsException("Wrong Matrix Coordinate");
        }
        Elem p = a1.get(i * size + j);
        p.setValue(value);
        a1.set(i * size + j, p);
    }

    public void delete(int i, int j) {
        if (i < 0 || i > size - 1 || j < 0 || j > size - 1) {
            throw new IndexOutOfBoundsException("Wrong Matrix Coordinate");
        }
        Elem p = a1.get(i * size + j);
        p.setValue(0);
        a1.set(i * size + j, p);
    }

    public ArrayList<Integer> minList() {
        int min;
        ArrayList<Integer> mins = new ArrayList<>();
        for (int j = 0; j < size; j++) {
            min = 2147483647;
            for (int i = 0; i < size; i++) {
                if (a1.get(i * size + j).getValue() < min) {
                    min = a1.get(i * size + j).getValue();
                }
            }
            mins.add(min);
        }
        return mins;
    }

    public int sumDiag() {
        int sum = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (a1.get(i * size + j).getI() == a1.get(i * size + j).getJ() || //main diag
                    a1.get(i * size + j).getI() + a1.get(i * size + j).getJ() == size - 1) { //secondary diag
                        sum += a1.get(i * size + j).getValue();
                }
            }
        }
        return sum;
    }

    public void transp() {
        ArrayList<Elem> trans = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                trans.add(i * size + j, a1.get(j * size + i));
            }
        }
        a1 = trans;
    }

    public void sumCols(int j1, int j2) {
        if (j1 < 0 || j1 > size - 1 || j2 < 0 || j2 > size - 1) {
            throw new IndexOutOfBoundsException("Wrong Matrix Coordinate");
        }
        if (j1 == j2) {
            return;
        }
        for (int i = 0; i < size; i++) {
            a1.get(i * size + j1).setValue(a1.get(i * size + j1).getValue() + a1.get(i * size + j2).getValue());
        }
    }

    public int getSize() {
        return size;
    }

    public ArrayList<Elem> getList() {
        return a1;
    }
}
