import java.io.*;
import java.util.LinkedList;
import java.util.Scanner;

  public class Main {

    public static void main (String [] args) throws IOException {

        //ARRAY

        arrayInput();
        FileReader fr = new FileReader("arrayDataInput.txt");
        FileWriter fw = new FileWriter("arrayDataOutput.txt");
        Scanner sc = new Scanner(fr);
        int count = 0;
        while (sc.hasNextLine()) {
            count++;
            String s = sc.nextLine();
            String [] str = s.split(" ");
            int [] a = new int [str.length];
            for (int i = 0; i < str.length; i++) {
                a[i] = Integer.parseInt(str[i]);
            }
            long t1 = System.nanoTime();
            arrayMergeSort(a, a.length);
            long t2 = System.nanoTime();
            fw.write(count + " - ");
            fw.write(a.length + ("               "));
            fw.write((int) (t2 - t1) + "    ");
            fw.write(arrIterations + "\n");
            arrIterations = 0;
        }
        fr.close();
        fw.close();

        //LINKED LIST

        llInput();
        FileReader fr2 = new FileReader("llDataInput.txt");
        FileWriter fw2 = new FileWriter("llDataOutput.txt");
        Scanner sc2 = new Scanner(fr2);
        count = 0;
        while(sc2.hasNextLine()) {
            count++;
            String s = sc2.nextLine();
            String [] str = s.split(" ");
            LinkedList<Integer> a = new LinkedList<>();
            for (int i = 0; i < str.length; i++) {
                a.add(i, Integer.parseInt(str[i]));
            }
            long t1 = System.nanoTime();
            llMergeSort(a, a.size());
            long t2 = System.nanoTime();
            fw2.write(count + " - ");
            fw2.write(a.size() + ("               "));
            fw2.write((int) (t2 - t1) + "    ");
            fw2.write(llIterations + "\n");
            llIterations = 0;
        }
        fr2.close();
        fw2.close();
    }

    public static int arrIterations = 0;
    public static void arrayMergeSort(int[] a, int n) {
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];

        for (int i = 0; i < mid; i++) {
            l[i] = a[i];
        }
        for (int i = mid; i < n; i++) {
            r[i - mid] = a[i];
        }
        arrayMergeSort(l, mid);
        arrayMergeSort(r, n - mid);

        arrayMerge(a, l, r, mid, n - mid);
    }

    public static void arrayMerge(int[] a, int[] l, int[] r, int left, int right) {
        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                a[k++] = l[i++];
            }
            else {
                a[k++] = r[j++];
            }
            arrIterations++;
        }

        while (i < left) {
            a[k++] = l[i++];
            arrIterations++;
        }

        while (j < right) {
            a[k++] = r[j++];
            arrIterations++;
        }
    }

    public static int llIterations = 0;
    public static void llMergeSort(LinkedList<Integer> a, int n) {
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        LinkedList<Integer> l = new LinkedList<>();
        LinkedList<Integer> r = new LinkedList<>();
        for (int i = 0; i < mid; i++) {
            l.add(i, a.get(i));
        }
        for (int i = mid; i < n; i++) {
            r.add(i - l.size(), a.get(i));
        }

        llMergeSort(l, mid);
        llMergeSort(r, n - mid);

        llMerge(a, l, r, mid, n - mid);
    }

    public static void llMerge(LinkedList<Integer> a, LinkedList<Integer> l, LinkedList<Integer> r, int left, int right) {
        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l.get(i) <= r.get(j)) {
                a.set(k++, l.get(i++));
            }
            else {
                a.set(k++, r.get(j++));
            }
            llIterations++;
        }

        while (i < left) {
            a.set(k++, l.get(i++));
            llIterations++;
        }

        while (j < right) {
            a.set(k++, r.get(j++));
            llIterations++;
        }
    }

    public static void arrayInput () throws IOException {

        File file = new File("arrayDataInput.txt");
        FileWriter fw = new FileWriter(file);
        int arrays = (int) (50 + Math.random() * 51);
        for (int i = 0; i < arrays; i++) {
            int nums = (int)(100 + Math.random() * 9901);
            for (int j = 0; j < nums; j++) {
                fw.write((int) (Math.random() * 10001) + " ");
            }
            fw.write(System.lineSeparator());
        }
    }

      public static void llInput () throws IOException {

          File file = new File("llDataInput.txt");
          FileWriter fw = new FileWriter(file);
          int lists = (int) (50 + Math.random() * 51);
          for (int i = 0; i < lists; i++) {
              int nums = (int)(100 + Math.random() * 9901);
              for (int j = 0; j < nums; j++) {
                  fw.write((int) (Math.random() * 10001) + " ");
              }
              fw.write(System.lineSeparator());
          }
      }
}
