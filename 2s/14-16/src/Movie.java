public class Movie {

    private int ID;
    private String name;
    private int year;
    private int directorID;

    public Movie (int ID, String name, int year, int directorID) {
        this.ID = ID;
        this.name = name;
        this.year = year;
        this.directorID = directorID;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getDirectorID() {
        return directorID;
    }

    public void setDirectorID(int directorID) {
        this.directorID = directorID;
    }
}
