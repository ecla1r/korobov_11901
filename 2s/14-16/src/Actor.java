import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class Actor {

    private int ID;
    private String fullname;
    private int exp;
    private LinkedHashMap<Movie, String> role;

    public Actor(int ID, String fullname, int exp) {
        this.ID = ID;
        this.fullname = fullname;
        this.exp = exp;
        this.role = new LinkedHashMap<>();
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public Map<Movie, String> getRoles() {
        return role;
    }

    public void setRole(Movie mov, String character) {
        this.role.put(mov, character);
    }

    public ArrayList<Integer> getMoviesIDs() {
        ArrayList<Integer> a = new ArrayList<>();
        for (Movie x : role.keySet()) {
            a.add(x.getID());
        }
        return a;
    }
}
