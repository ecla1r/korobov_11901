import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    static LinkedHashMap<Integer, Director> directors = new LinkedHashMap<>();
    static LinkedHashMap<Integer, Actor> actors = new LinkedHashMap<>();
    static LinkedHashMap<Integer, Movie> movies = new LinkedHashMap<>();

    public static void main (String [] args) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(new File("Directors.txt")));
        while (br.ready()) {
            String s = br.readLine();
            int size = directors.size();
            directors.put(size + 1, new Director(size + 1, s));
        }

        br = new BufferedReader(new FileReader(new File("Actors.txt")));
        while (br.ready()) {
            String s = br.readLine();
            String [] arr = s.split("/");
            int size = actors.size();
            actors.put(size + 1, new Actor(size + 1, arr[0], Integer.parseInt(arr[1])));
        }

        br = new BufferedReader(new FileReader(new File("Movies.txt")));
        while (br.ready()) {
            String s = br.readLine();
            String [] arr = s.split("/");
            int size = movies.size();
            movies.put(size + 1, new Movie(size + 1, arr[0], Integer.parseInt(arr[1]), Integer.parseInt(arr[2])));
        }

        br = new BufferedReader(new FileReader(new File("Roles.txt")));
        while (br.ready()) {
            String s = br.readLine();
            String [] arr = s.split("/");
            for (Actor x : actors.values()) {
                if (x.getID() == Integer.parseInt(arr[1])) {
                    x.setRole(movies.get(Integer.parseInt(arr[0])), arr[2]);
                }
            }
        }

        System.out.println(directorsMoviesBeforeYearX("Arslanov Marat Mirzaevich", 2019));
        jointMoviesForAll(directors, actors);
        System.out.println(onlyOneDirAfterYearXForAll(actors, 2012));

    }

    ///////////////////////////////////////// StreamAPI Methods ////////////////////////////////////////////////////////

    public static List directorsMoviesBeforeYearX (String name, int year) {
        return movies.values().stream()
                     .filter(x -> x.getYear() <= year && x.getDirectorID() == directors.values().stream()
                                                                                       .filter(y -> y.getFullName().equals(name))
                                                                                       .mapToInt(Director::getID).sum())
                     .map(Movie::getName)
                     .collect(Collectors.toList());
    }

    public static void jointMoviesForAll (Map<Integer, Director> dirs, Map<Integer, Actor> actrs) {
        dirs.values().forEach(x -> actrs.values().forEach(y -> jointMovies(x, y)));
    }

    public static void jointMovies (Director dir, Actor actr) {
        ArrayList<Integer> list = dir.getMoviesIDs(movies);
        list.retainAll(actr.getMoviesIDs());
        System.out.println(dir.getFullName() + " - " + actr.getFullname() + " : " + list.size());
    }

    public static List onlyOneDirAfterYearXForAll (Map<Integer, Actor> actrs, int year) {
        return actrs.values().stream().filter(x -> onlyOneDirAfterYearX(x, year) == 1).map(Actor::getFullname).collect(Collectors.toList());
    }

    public static int onlyOneDirAfterYearX (Actor actor, int year) {
        return actor.getRoles().keySet().stream().filter(x -> x.getYear() >= year).map(Movie::getDirectorID).collect(Collectors.toSet()).size();
    }

    ////////////////////////////////////////// Default Methods /////////////////////////////////////////////////////////

    public static List directorsMoviesBeforeYearXDefault (String name, int year) {
        ArrayList<String> l = new ArrayList<>();
        for (Movie m : movies.values()) {
            if (m.getYear() <= year && directors.get(m.getDirectorID()).getFullName().equals(name)) {
                l.add(m.getName());
            }
        }
        return l;
    }

    public static void jointMoviesForAllDefault (Map<Integer, Director> dirs, Map<Integer, Actor> actors) {
        for (Director d : dirs.values()) {
            for (Actor a : actors.values()) {
                jointMovies(d, a);
            }
        }
    }

    public static List onlyOneDirAfterYearXForAllDefault (Map<Integer, Actor> actors, int year) {
        ArrayList<String> list = new ArrayList<>();
        for (Actor a : actors.values()) {
            LinkedHashSet<Integer> set = new LinkedHashSet<>();
            for (Movie m : a.getRoles().keySet()) {
                if (m.getYear() >= year) {
                    set.add(m.getDirectorID());
                }
            }
            if (set.size() == 1) list.add(a.getFullname());
        }
        return list;
    }
}
