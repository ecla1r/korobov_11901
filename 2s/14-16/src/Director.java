import java.util.ArrayList;
import java.util.Map;

public class Director {

    private int ID;
    private String fullName;

    public Director(int ID, String fullName) {
        this.ID = ID;
        this.fullName = fullName;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public ArrayList<Integer> getMoviesIDs(Map<Integer, Movie> movies) {
        ArrayList<Integer> a = new ArrayList<>();
        for (Movie m : movies.values()) {
            if (m.getDirectorID() == this.getID()) {
                a.add(m.getID());
            }
        }
        return a;
    }
}
