class User {

    private int id;
    private String name;
    private String city;
    private int year;
    private static int curID = 0;

    User (String name, String city, int year) {
        this.name = name;
        this.city = city;
        this.year = year;
        curID++;
        this.id = curID;
    }

    private int getId() {
        return id;
    }

    boolean equals(User u) {
        return u.getId() == this.id;
    }

    String getName() {
        return name;
    }

    String getCity() {
        return city;
    }

    int getAge(){
        return 2020 - this.year;
    }
}
