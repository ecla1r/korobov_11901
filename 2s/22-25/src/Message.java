class Message {

    private User sender;
    private User receiver;
    private String date;
    private String text;
    private boolean isRead;

    Message (User sender, User receiver, String text, boolean isRead, String date) {
        this.sender = sender;
        this.receiver = receiver;
        this.text = text;
        this.date = date;
        this.isRead = isRead;
    }

    User getSender() {
        return sender;
    }

    User getReceiver() {
        return receiver;
    }

    boolean isRead() {
        return isRead;
    }
}
