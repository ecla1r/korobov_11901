import java.lang.Object;
import java.util.Collection;
import java.util.Iterator;

public class MyArrayCollection<T> implements Collection<T> {

    private T[] a;
    private int size;
    private int capacity;

    public MyArrayCollection() {
        this.size = 0;
        this.capacity = 10;
        this.a = (T[]) new Object [capacity];
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < this.size; i++) {
            if (a[i].equals(o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            private int i = 0;

            @Override
            public boolean hasNext() {
                return a[i] != null && i < size;
            }

            @Override
            public T next() {
                return a[i++];
            }
        };
    }


    @Override
    public Object[] toArray() {
        Object [] newArr = new Object[this.size];
        for (int i = 0; i < this.size; i++) {
            newArr[i] = this.a[i];
        }
        return newArr;
    }

    @Override // Not needed to do this method
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        if (this.capacity < this.size + 1) {
            this.capacity *= 1.5 + 1;
            T[] newArr = (T[]) new Object[this.capacity];
            for (int i = 0; i < this.size; i++) {
                newArr[i] = this.a[i];
            }
            this.a = newArr;
        }
        this.a[size] = t;
        this.size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (this.contains(o)) {
            for (int i = 0; i < this.size; i++) {
                if (this.a[i].equals(o)) {
                    for (int j = i; j < size - 1; j++) {
                        this.a[i] = this.a[i + 1];
                    }
                    this.a[size - 1] = null;
                    this.size--;
                }
            }
        } else return false;
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (T t : a) {
            if (!c.contains(t)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T t : c) {
            this.add(t);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (T t : a) {
            if (c.contains(t)) this.remove(t);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (T t : a) {
            if (!c.contains(t)) this.remove(t);
        }
        return true;
    }

    @Override
    public void clear() {
        this.a = (T[]) new Object[this.capacity];
        this.size = 0;
    }
}
