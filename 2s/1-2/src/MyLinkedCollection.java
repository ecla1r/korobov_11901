import java.util.Collection;
import java.util.Iterator;

public class MyLinkedCollection<T extends Comparable<T>> implements Collection<T> {

    private int size;
    private Node<T> head;

    public MyLinkedCollection() {
        this.size = 0;
        this.head = null;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        Node<T> t = head;
        while(t != null){
            if (t.getValue().equals(o)) {
                return true;
            }
            t = t.getNext();
        }
        return false;
    }

    @Override // Not needed method
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        Node<T> t = head;
        T[] a = (T[]) new Object[size];
        for (int i = 0; i < size && t != null; i++) {
            a[i] = t.getValue();
            t = t.getNext();
        }
        return a;
    }

    @Override //Not needed method
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        Node<T> p = new Node(t, null);
        if (head == null) {
            head = p;
        }
        else {
            Node<T> cur = head;
            while (cur.getNext() != null) {
                cur = cur.getNext();
            }
            cur.setNext(p);
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        Node<T> p = head;
        while (p != null) {
            if (p.getValue().equals(o)) {
                if (p.getNext() != null) {
                    p.setValue(p.getNext().getValue());
                    p.setNext(p.getNext().getNext());
                    return true;
                } else {
                    p = null;
                    return true;
                }
            }
            p = p.getNext();
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        Node<T> p = head;
        while (p != null) {
            if (!c.contains(p.getValue())) return false;
            p = p.getNext();
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T t : c) {
            this.add(t);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Node<T> p = head;
        while (p != null) {
            if (c.contains(p)) this.remove(p);
            p = p.getNext();
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Node<T> p = head;
        while (p != null) {
            if (!c.contains(p)) this.remove(p);
            p = p.getNext();
        }
        return true;
    }

    @Override
    public void clear() {
        this.head = null;
        this.size = 0;
    }
}
