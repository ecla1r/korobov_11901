import org.omg.PortableInterceptor.INACTIVE;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        File wine = new File("wine.csv");
        File appellations = new File("appellations.csv");
        File grapes = new File("grapes.csv");

        List<Wine> wineList = new BufferedReader(new FileReader(wine))
                .lines()
                .skip(1)
                .map(s -> {
                    return new Wine(s.split(","));
                })
                .collect(Collectors.toList());

        List<Appellation> appellationsList = new BufferedReader(new FileReader(appellations))
                .lines()
                .skip(1)
                .map(s -> {
                    return new Appellation(s.split(","));
                })
                .collect(Collectors.toList());

        List<Grape> grapesList = new BufferedReader(new FileReader(grapes))
                .lines()
                .skip(1)
                .map(s -> {
                    return new Grape(s.split(","));
                })
                .collect(Collectors.toList());

        mostWinesAppellation(appellationsList, wineList);
        allWines(grapesList, wineList);
    }

    public static Appellation mostWinesAppellation (List<Appellation> al, List<Wine> wl) {
        Map<Appellation, Integer> map = new HashMap<>();
        al.stream().map(a ->
             map.put(a, wl.stream().filter(w -> w.appellation.equals(a)).mapToInt(w -> w.cases).sum()));
        Map.Entry<Appellation, Integer> maxEntry = null;
        for (Map.Entry<Appellation, Integer> entry : map.entrySet())
        {
            if (maxEntry == null || entry.getValue() > 0)
            {
                maxEntry = entry;
            }
        }
        return maxEntry.getKey();
    }

    public static Map<Grape, Integer> allWines (List<Grape> gl, List<Wine> wl) {
        Map<Grape, Integer> map = new HashMap<>();
        gl.stream().map(g ->
                map.put(g, wl.stream().filter(w -> w.grape.equals(g)).mapToInt(w -> w.cases).sum()));
        return map;
    }
}