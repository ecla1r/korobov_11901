public class Grape {

    int id;
    String grape;
    String color;

    public Grape(String [] arr) {
        this.id = Integer.parseInt(arr[0]);
        this.grape = arr[1];
        this.color = arr[2];
    }
}
