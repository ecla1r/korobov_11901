public class Wine {
    String grape;
    String winery;
    String appellation;
    String state;
    String name;
    int year;
    int price;
    int score;
    int cases;
    String drink;

    public Wine(String [] arr) {
        this.grape = arr[1];
        this.winery = arr[2];
        this.appellation = arr[3];
        this.state = arr[4];
        this.name = arr[5];
        this.year = Integer.parseInt(arr[6]);
        this.price = Integer.parseInt(arr[7]);
        this.score = Integer.parseInt(arr[8]);
        if (arr[9].equals("NULL")) {
            this.cases = 0;
        } else this.cases = Integer.parseInt(arr[9]);
        this.drink = arr[10];
    }
}