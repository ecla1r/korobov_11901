public class Appellation {

    String appellation;
    String county;
    String state;
    String area;
    boolean isAVA;

    public Appellation(String [] arr) {
        this.appellation = arr[1];
        this.county = arr[2];
        this.state = arr[3];
        this.area = arr[4];
        if (arr[5].equals("Yes")) {
            this.isAVA = true;
        } else this.isAVA = false;
    }
}
