public class Main {

    public static void main(String[] args) {

        IntegerArrayCollection a = new IntegerArrayCollection();
        a.add(20);
        a.add(10);
        System.out.println(a.size());
        System.out.println(a.contains(20));
        for (Object o : a.toArray()) {
            System.out.println(o);
        }
        a.remove(10);
        IntegerArrayCollection a2 = new IntegerArrayCollection();
        a2.add(20);
        a2.add(40);
        a.retainAll(a2);
    }
}
