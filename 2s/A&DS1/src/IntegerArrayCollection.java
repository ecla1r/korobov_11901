import java.util.Collection;
import java.util.Iterator;

public class IntegerArrayCollection implements Collection<Integer> {

    private int [] array;
    private int capacity;
    private int size;

    public IntegerArrayCollection() {
        capacity = 1000;
        array = new int[capacity];
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    //++++++++++++
    @Override
    public boolean contains(Object num) {
        for (int i = 0; i < size; i++) {
            if (array[i] == (int)num) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    //+++++++++
    @Override
    public Object [] toArray() {
        Object [] obj = new Object[size];
        for (int i = 0; i < size; i++) {
            obj[i] = array[i];
        }
        return obj;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer num) {
        if (size + 1 < capacity) {
            array[size] = num;
            size++;
        }
        else {
            capacity += 1000;
            array = new int [capacity];
            array[size] = num;
            size++;
        }
        return true;
    }

    //+++
    @Override
    public boolean remove(Object o) {
        if (this.contains(o)) {
            int imax = -1;
            for (int i = 0; i < size; i++) {
                if ((int) o == array[i])
                    imax = i;
            }
            for (int j = imax; j < size - 1; j++) {
                array[j] = array[j + 1];
            }
            array[size - 1] = 0;
            size += -1;
            return true;
        }
        else return false;
    }

    //+++
    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    //+++
    @Override
    public void clear() {
        array = new int [capacity];
    }

    //+++
    @Override
    public boolean retainAll(Collection <?> c) {
        for (int x : array) {
            if (c.contains(x) == false)
                this.remove(x);
        }
        return true;
    }

    //+++
    @Override
    public boolean removeAll(Collection <?> c) {
        for (int i : array) {
            if (c.contains(i) == true)
                this.remove(i);
        }
        return true;
    }

    //+++
    @Override
    public boolean containsAll(Collection <?> c) {
        for (int i : array) {
            if (c.contains(i) == false)
                return false;
        }
        return true;
    }
}
