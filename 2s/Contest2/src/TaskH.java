import java.util.Scanner;

public class TaskH {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int d1 = in.nextInt();
        int d3 = in.nextInt();
        int arr = 201;
        final int p = (int) 1e9 + 9;

        int a [][] = new int[arr][arr];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                if (i == 0 || j == 0) {
                    a[i][j] = 1;
                }
            }
        }

        for (int i = 1; i < a.length; i++) {
            for (int j = 1; j < a.length; j++) {
                a[i][j] = (a[i - 1][j] + a[i][j - 1]) % p;
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        int count = 0;
        if (d1 + d3 * 3 < n) { //проверка на нехватку общей длины плиток
            System.out.println(0);
            return;
        } else {
            while (d3 * 3 > n) { //удаление лишних тройных плиток
                d3--;
            }
            while (n - d3 * 3 <= d1 && d3 >= 0) {
                count = (count + a[d3][n - d3 * 3]) % p;
                d3--;
            }
        }
        System.out.println(count);
    }
}
