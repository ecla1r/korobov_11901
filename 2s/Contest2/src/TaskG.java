import java.util.Scanner;

public class TaskG {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        int [] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        int min = 2 * (int) 1e9;
        int minCurPos = 0;
        if (k == 1) {
            for (int i = 0; i < n; i++) {
                if (a[i] < min) {
                    min = a[i];
                    minCurPos = i;
                }
            }
        } else {
            int curSum = 0;
            for (int i = 0; i < k; i++) {
                curSum += a[i];
            }
            if (curSum < min) {
                min = curSum;
                minCurPos = 0;
            }
            for (int i = 1; i <= n - k; i++) {
                curSum += a[i + k - 1];
                curSum -= a[i - 1];
                if (curSum < min) {
                    min = curSum;
                    minCurPos = i;
                }
            }
        }
        System.out.println(minCurPos + 1);
    }
}
