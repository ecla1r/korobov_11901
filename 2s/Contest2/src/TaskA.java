import java.util.*;

public class TaskA {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int s = sc.nextInt();
        TreeMap<Integer, Integer> buyMap = new TreeMap<>();
        TreeMap<Integer, Integer> sellMap = new TreeMap<>();
        String flag;
        int a1;
        int a2;
        for (int i = 0; i < n; i++) {
            flag = sc.next();
            a1 = sc.nextInt();
            a2 = sc.nextInt();
            if (flag.equals("S")) {
                if (sellMap.get(a1) != null) {
                    sellMap.put(a1, sellMap.get(a1) + a2);
                } else sellMap.put(a1, a2);
            } else {
                if (buyMap.get(a1) != null) {
                    buyMap.put(a1, buyMap.get(a1) + a2);
                } else buyMap.put(a1, a2);
            }
        }
        NavigableMap<Integer, Integer> buyMapEnded = buyMap.descendingMap();
        NavigableMap<Integer, Integer> sellMapEnded = sellMap.descendingMap();

        int curS = 0;
        for (int x : sellMapEnded.keySet()) {
            curS++;
            if (sellMapEnded.size() - curS < s) {
                System.out.println("S " + x + " " + sellMapEnded.get(x));
            }
        }
        int curB = 0;
        for (int x : buyMapEnded.keySet()) {
            curB++;
            if (curB - 1 < s) {
                System.out.println("B " + x + " " + buyMapEnded.get(x));
            }
        }
    }
}
