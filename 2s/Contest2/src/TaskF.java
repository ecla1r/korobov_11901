import java.util.Scanner;

public class TaskF {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if (n == 1) {
            System.out.println(sc.nextInt());
            return;
        }
        int [][] field = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                field[i][j] = sc.nextInt();
            }
        }
        int k = 0;

        for (int i = 1; i < n; i++) {
            field[i][0] += field[i - 1][0];
        }
        for (int j = 1; j < n; j++) {
            field[0][j] += field[0][j - 1];
        }

        while (k < n) {
            for (int i = k; i < n; i++) {
                for (int j = k; j < n; j++) {
                    if ((i <= k || j <= k) && k != 0) {
                        field[i][j] += Math.max(field[i - 1][j], field[i][j - 1]);
                    }
                }
            }
            k++;
        }
        System.out.println(field[n - 1][n - 1]);
    }
}
