import java.util.Scanner;

public class TaskC {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int[][] mood = new int[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                mood[i][j] = sc.nextInt();
            }
        }

        recursive(5, new int[] { 0, 1, 2, 3, 4 }, mood);

        System.out.println(max);
    }

    public static int max = 0;

    public static void recursive(int n, int[] a, int[][] mood) {
        if (n == 0)
            max = Math.max(max, mood[a[0]][a[1]] +
                                mood[a[1]][a[0]] +
                            2 * mood[a[2]][a[3]] +
                            2 * mood[a[3]][a[2]] +
                                mood[a[2]][a[1]] +
                                mood[a[1]][a[2]] +
                            2 * mood[a[3]][a[4]] +
                            2 * mood[a[4]][a[3]]);
        else {
            for (int i = 0; i < n; i++) {
                recursive(n - 1, a, mood);
                int j = n % 2 == 1 ? 0 : i;
                int temp = a[j];
                a[j] = a[n - 1];
                a[n - 1] = temp;
            }
        }
    }
}
