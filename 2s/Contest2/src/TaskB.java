import java.util.HashMap;
import java.util.Scanner;

public class TaskB {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        HashMap<String, Integer> map = new HashMap<>();
        map.put("polycarp", 1);
        for (int i = 0; i < n; i++) {
            String who = sc.next().toLowerCase();
            sc.next();
            String from = sc.next().toLowerCase();
            map.put(who, map.get(from) + 1);
        }
        int max = 0;
        for (int x : map.values()) {
            max = Math.max(max, x);
        }
        System.out.println(max);
    }
}
