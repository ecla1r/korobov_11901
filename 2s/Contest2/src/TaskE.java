import java.util.Scanner;

public class TaskE {

    public static void main(String[] args) {
        putFerz(0, 0);
        System.out.println(answer);
    }

    private static Scanner sc = new Scanner(System.in);
    private static int answer = 0;
    private static int n = sc.nextInt();
    private static int m = sc.nextInt();
    private static int[] a = new int[n];

    public static boolean correct(int x, int[] arr) {
        boolean check = true;
        for (int i = 0; i < x; i++) {
            if (arr[i] == arr[x] || Math.abs(x - i) == Math.abs(arr[x] - arr[i])) {
                check = false;
            }
        }
        return check;
    }

    public static void putFerz(int x, int ones) {
        if (ones == m) {
            answer++;
        } else if (x < n) {
            for (a[x] = 0; a[x] < n; a[x]++) {
                if (correct(x, a)) putFerz(x + 1, ones + 1);
            }
            a[x] *= 10;
            putFerz(x + 1, ones);
        }
    }
}
