import java.util.Scanner;

public class TaskD {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int m = sc.nextInt();

        sc.nextLine();

        int [][] building = new int[n + 1][m + 1];
        String str;
        for (int i = n; i > 0; i--) {
            str = sc.nextLine();
            for (int j = 1; j <= m; j++) {
                building[i][j] = (str.charAt(j) == '1') ? 1 : 0;
            }
        }

        int [] leftData = new int[n + 1];
        int [] rightData = new int[n + 1];

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (building [i][j] == 1) {
                    leftData[i] = j;
                    break;
                }
            }
        }

        for (int i = 1; i <= n; i++) {
            for (int j = m; j > 0; j--) {
                if (building[i][j] == 1) {
                    rightData[i] = j;
                    break;
                }
            }
        }

        int [] leftSize = new int[n + 1];
        int [] rightSize = new int[n + 1];
        leftSize[0] = 0;
        rightSize[0] = -1;
        for (int i = 1; i < n; i++) {
            if (leftData[i] == 0) {
                leftSize[i] = leftSize[i - 1] + 1;
                if (rightSize[i - 1] < 0) {
                    rightSize[i] = -1;
                } else {
                    rightSize[i] = rightSize[i - 1] + 1;
                }
            } else {
                if (rightSize[i - 1] == -1) {
                    leftSize[i] = leftSize[i - 1] + 2 * rightData[i] + 1;
                    rightSize[i] = leftSize[i - 1] + m + 1 + 1;
                } else {
                    leftSize[i] = Math.min(leftSize[i - 1] + 2 * rightData[i] + 1, rightSize[i - 1] + m + 1 + 1);
                    rightSize[i] = Math.min(leftSize[i - 1] + m + 1 + 1, rightSize[i - 1] + 2 * (m - leftData[i] + 1) + 1);
                }
            }
        }
        int maxFloor = n;
        while ((maxFloor > 0) && (leftData[maxFloor] == 0)) {
            maxFloor--;
        }
        if (maxFloor == 0) {
            System.out.println(0);
        } else {
            if (rightSize[maxFloor - 1] == -1) {
                System.out.println(leftSize[maxFloor - 1] + rightData[maxFloor]);
            } else {
                System.out.println(Math.min(leftSize[maxFloor - 1] + rightData[maxFloor], rightSize[maxFloor-1] + (m - leftData[maxFloor] + 1)));
            }
        }
    }
}
