import java.util.EmptyStackException;
import java.util.Scanner;
import java.util.Stack;

public class Main {

    //Task 7b

    public static boolean roundBracketsCheckDefault (String s) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') count++;
            else if (s.charAt(i) == ')') count--;
            if (count < 0) return false;
        }
        return count == 0;
    }

    //Task 7a

    public static boolean roundBracketsCheckStack (String s) {
        Stack<Character> stack = new Stack();
        for (int i = 0; i < s.length(); i++) {
            try {
                if (s.charAt(i) == '(') stack.push(s.charAt(i));
                if (s.charAt(i) == ')') {
                    char ch = stack.pop();
                    if (ch != '(') return false;
                }
            } catch (EmptyStackException e) {
                return false;
            }

        }
        return stack.empty();
    }

    //Task 8

    public static boolean bracketsCheck (String s) {
        Stack<String> stack = new Stack();
        String [] arr = s.split("");
        boolean flag = false;
        for (int i = 0; i < arr.length; i++) {
            String cur = arr[i];
            try {
                switch (cur) {
                    case ")" : {
                        if (!stack.pop().equals("(")) return false;
                        break;
                    }
                    case "}" : {
                        if (!stack.pop().equals("{")) return false;
                        break;
                    }
                    case "]" : {
                        if (!stack.pop().equals("[")) return false;
                        break;
                    }
                    case ">" : {
                        if (!stack.pop().equals("<")) return false;
                        break;
                    }
                    case "\"" : {
                        if (flag) {
                            if (!stack.pop().equals("\"")) return false; }
                        else stack.push(cur);
                        flag = !flag;
                        break;
                    }
                    default:
                        stack.push(cur);
                }
            } catch (EmptyStackException e) {
                return false;
            }
        }
        return stack.isEmpty();
    }

    //Task 9

    public static int counter (Stack<String> stack) {
        Stack<String> stack2 = new Stack<>();
        while (!stack.empty()) {
            String temp = stack.pop();
            switch (temp) {
                case "+":
                    int a = Integer.parseInt(stack2.pop());
                    int b = Integer.parseInt(stack2.pop());
                    stack2.push(Integer.toString(a + b));
                    break;
                case "-":
                    a = Integer.parseInt(stack2.pop());
                    b = Integer.parseInt(stack2.pop());
                    stack2.push(Integer.toString(a - b));
                    break;
                case "*":
                    a = Integer.parseInt(stack2.pop());
                    b = Integer.parseInt(stack2.pop());
                    stack2.push(Integer.toString(a * b));
                    break;
                case "/":
                    a = Integer.parseInt(stack2.pop());
                    b = Integer.parseInt(stack2.pop());
                    stack2.push(Integer.toString(a / b));
                    break;
                default:
                    stack2.push(temp);
                    break;
            }
        }
        return Integer.parseInt(stack2.pop());
    }
}
