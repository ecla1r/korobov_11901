import java.io.*;

public class IOstart {

    public static void main(String [] args) throws IOException {

        // Task 1

        int n = (int)(10 + Math.random() * 11);
        OutputStream os = new FileOutputStream("first.txt");
        for (int i = 0; i < n; i++) {
            os.write((int)(Math.random() * 101));
        }
        os.close();

        //Task 2

        InputStream is = new FileInputStream("first.txt");
        os = new FileOutputStream("second.txt");
        while (is.available() > 0) {
            os.write(is.read());
        }
        is.close();
        os.close();

        //Task 3

        is = new FileInputStream("second.txt");
        os = new FileOutputStream("third.txt");
        int c = (int)(3 + Math.random() * 5);
        byte [] arr = new byte [c];
        while (is.available() >= c) {
            for (int i = 0; i < arr.length; i++) {
                arr[i] = (byte) is.read();
            }
            os.write(arr);
        }
        while (is.available() > 0) {
            os.write(is.read());
        }
        os.close();
        is.close();

    }
}
