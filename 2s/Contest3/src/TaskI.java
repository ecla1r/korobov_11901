import java.util.Arrays;
import java.util.Scanner;

public class TaskI {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String s = sc.next();

        char[] arr = s.toCharArray();
        char[] first = new char[n];
        char[] last = new char[n];

        for (int i = 0; i < n; i++) {
            first[i] = arr[i];
            last[i] = arr[i + n];
        }

        Arrays.sort(first);
        Arrays.sort(last);

        int res = 0;
        for (int i = 0; i < n; i++) {
            if (first[i] > last[i]) {
                res++;
            }
            if (first[i] < last[i]) {
                res--;
            }
        }
        System.out.println(Math.abs(res) == n ? "YES" : "NO");
    }
}
