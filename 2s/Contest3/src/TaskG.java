import java.util.Scanner;

public class TaskG {

    public static void main (String [] args) {
        Scanner sc = new Scanner(System.in);
        String s1 = sc.next();
        String s2 = sc.next();
        boolean flag = true;
        StringBuilder s3 = new StringBuilder();
        for (int i = 0; i < s1.length(); i++) {
            if (flag) {
                s3.append(s1.charAt(i));
                if (s1.charAt(i) != s2.charAt(i)) {
                    flag = false;
                }
            } else {
                s3.append(s2.charAt(i));
                if (s1.charAt(i) != s2.charAt(i)) {
                    flag = true;
                }
            }
        }
        if (!flag) {
            System.out.println("impossible");
        } else System.out.println(s3.toString());
    }
}
