import java.util.Scanner;

public class TaskE {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int sum = 0;
        int [] a = new int[5001];
        for (int i = 0; i < n; i++) {
            int cur = sc.nextInt();
            if (a[cur] == 1 || cur > n) {
                sum++;
            } else a[cur] = 1;
        }
        System.out.println(sum);
    }
}
