import java.util.Scanner;

public class TaskF {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int [][] a = new int[n][m];
        int [] mins = new int[n];
        for (int i = 0; i < n; i++) {
            int minStreet = (int) 1e9 + 1;
            for (int j = 0; j < m; j++) {
                a[i][j] = sc.nextInt();
                if (a[i][j] < minStreet) {
                    minStreet = a[i][j];
                }
            }
            mins[i] = minStreet;
        }
        int maxI = 0;
        int maxStreet = -1;
        for (int i = 0; i < mins.length; i++) {
            if (mins[i] > maxStreet) {
                maxStreet = mins[i];
                maxI = i;
            }
        }
        int minAvenue = (int) 1e9 + 1;
        for (int j = 0; j < m; j++) {
            if (a[maxI][j] < minAvenue) {
                minAvenue = a[maxI][j];
            }
        }
        System.out.println(minAvenue);
    }
}
