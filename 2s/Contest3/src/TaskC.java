import java.util.Scanner;

public class TaskC {

    public static void main (String [] args) {

        Scanner sc = new Scanner (System.in);
        char [] a = sc.next().toCharArray();
        int [] sums = new int[a.length];

        int cur = 0;
        for(int i = 1; i < a.length; i++) {
            if(a[i] == a[i-1]) {
                cur++;
            }
            sums[i] = cur;
        }

        int m = sc.nextInt();
        while(m > 0) {
            int first = sc.nextInt() - 1;
            int last = sc.nextInt() - 1;

            System.out.println(sums[last] - sums[first]);
            m--;
        }
    }
}
