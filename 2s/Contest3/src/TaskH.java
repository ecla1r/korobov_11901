import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class TaskH {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int b = s.nextInt();
        int [] a = new int[n];
        for(int i = 0; i < n; i++) {
            a[i] = s.nextInt();
        }
        int count = 0;
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < n - 1; i++) {
            if (a[i] % 2 == 0) {
                count--;
            } else count++;

            if (count == 0) {
                list.add(Math.abs(a[i] - a[i + 1]));
            }
        }

        Collections.sort(list);

        count = 0;
        int ans = 0;
        for(int i = 0; i < list.size() && count <= b; i++) {
            count += list.get(i);
            if (count > b) {
                break;
            }
            ans++;
        }
        System.out.println(ans);
    }

}
