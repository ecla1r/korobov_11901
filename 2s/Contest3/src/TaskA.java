import java.util.Scanner;

public class TaskA {

    public static void main (String [] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int finalMax = 0;
        int curMax = 0;
        for (int i = 0; i < n; i++) {
            int cur = sc.nextInt();
            if (Math.abs(cur) % 2 == 0 && cur < 0) {
                curMax++;
                if (curMax > finalMax) {
                    finalMax = curMax;
                }
            } else curMax = 0;
        }
        System.out.println(finalMax);
    }
}
