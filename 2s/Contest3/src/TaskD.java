import java.util.Scanner;

public class TaskD {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String str = sc.next();
        int[] a = new int[10];

        int sum = 0;
        for(int i = 0; i < str.length(); i++) {
            int x = str.charAt(i) - '0';
            sum += x;
            a[x]++;
        }

        int ans = 0;
        for (int i = 0; i <= 9; i++)
            while (a[i]-- > 0 && n > sum) {
                sum += 9 - i;
                ans++;
            }

        System.out.println(ans);
    }
}
