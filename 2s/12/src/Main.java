import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String [] args) throws FileNotFoundException {

        File file = new File("C:\\Users\\ecla1\\korobov_11901\\2s\\TextFileMap\\text.txt");
        Scanner sc = new Scanner(file);
        HashMap<String, Integer> m = new HashMap<>();
        String str = sc.nextLine();
        String [] arr = str.split("");
        for (int i = 0; i < arr.length; i++) {
            if ((arr[i].charAt(0) >= 'a' && arr[i].charAt(0) <= 'z') ||
                    (arr[i].charAt(0) >= 'A' && arr[i].charAt(0) <= 'Z')) {
                String s = arr[i];
                if (!m.containsKey(s)) {
                    m.put(s, 1);
                }
                else {
                    int k = m.get(s);
                    k++;
                    m.put(s, k);
                }
            }
        }
        for (Map.Entry<String, Integer> entry: m.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }
}
