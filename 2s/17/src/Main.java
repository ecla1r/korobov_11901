import java.io.*;
import java.net.URL;

public class Main {

    public static void main (String [] args) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader("links.txt"));

        int count = 1;
        while (br.ready()) {
            URL u = new URL(br.readLine());
            BufferedInputStream bis = new BufferedInputStream(u.openStream());
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("image" + count + ".jpg"));
            int bytes = bis.read();
            while (bytes >= 0) {
                bos.write(bytes);
                bytes = bis.read();
            }
            bis.close();
            bos.close();
            count++;
        }
        br.close();
    }
}
