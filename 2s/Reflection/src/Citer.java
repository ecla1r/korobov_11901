public class Citer {

    @say
    public String kavo() { return "hello my name is danya"; }

    @say
    @noEnglish
    public String tavo() { return "я кто who"; }

    @say
    @mayak
    public String skor() { return "Ehal Greka Cherez Reku"; }

    @say
    @loud
    public String goodbye() { return "goodbye"; }

    public static void main (String [] args) {
        Citer citer = new Citer();
        Output o = new Output();
        o.out(citer);
    }
}
