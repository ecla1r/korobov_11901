import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        int n = Integer.parseInt(s.nextLine());

        boolean[][] matrix = new boolean[n][n];
        String connected = s.nextLine();
        while (!connected.equals("0")) {
            String[] a = connected.split(" ");
            matrix[Integer.parseInt(a[0]) - 1] [Integer.parseInt(a[1]) - 1] = true;
            matrix[Integer.parseInt(a[1]) - 1] [Integer.parseInt(a[0]) - 1] = true;
            connected = s.nextLine();
        }

        int [] ans = new int[n];
        paint(matrix, ans, 0);
        for (int i = 1; i <= n; i++) {
            System.out.println(i + " - " + ans[i]);
        }
    }

    public static boolean paint(boolean [][] matrix, int[] ans, int x) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            list.add(i);
        }
        for (int i = 0; i < matrix[0].length; i++) {
            if (!matrix[x][i]) continue;
            if (list.isEmpty()) break;
            if (list.contains(ans[i])){
                list.remove(new Integer(ans[i]));
            }
        }
        if (list.isEmpty()) return false;
        else {
            ans[x] = list.get(0);
            for (int i = 0; i < matrix[x].length; i++) {
                if (matrix[x][i] && ans[i] == 0) {
                    int j = 1;
                    boolean isPainted = paint(matrix, ans, i);
                    while (!isPainted & j < list.size()) {
                        ans[x] = list.get(j);
                        j++;
                        isPainted = paint(matrix, ans, i);
                    }
                    if (list.size() <= j && !isPainted) {
                        ans[x] = 0;
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
