import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class Main {

    public static void main (String [] args) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader("file.txt"));
        LinkedHashMap<String, ArrayList<String>> map = new LinkedHashMap<>();
        while(br.ready()) {
            String s = br.readLine();
            String [] arr = s.split(" ");
            if (!map.containsKey(arr[0])) {
                ArrayList<String> list = new ArrayList<>();
                list.add(arr[1]);
                map.put(arr[0], list);
            }
            else map.get(arr[0]).add(arr[1]);
        }
        br.close();
        map.entrySet().forEach(System.out::println);
    }
}
