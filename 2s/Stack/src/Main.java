import java.util.Scanner;

public class Main {

    public static void main(String [] args) {

        MyArrayStack t = new MyArrayStack();
        t.push("bruh");
        t.push("pop");
        System.out.println(t.peek());

        MyLinkedStack l = new MyLinkedStack();
        l.push(20);
        l.push(30);
        System.out.println(l.peek());

        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();

        System.out.println(checkBracketsDefault(s));
        System.out.println(checkBracketsStack(s));

        String s1 = sc.nextLine();
        System.out.println(checkAllBracketsStack(s1));

        String s2 = sc.nextLine();
        System.out.println(counterStack(s2));

    }

    public static boolean checkBracketsDefault(String s) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                count++;
            }
            else if (s.charAt(i) == ')') {
                count--;
                if (count < 0) {
                    return false;
                }
            }
        }
        return count == 0;
    }

    public static boolean checkBracketsStack (String s) {
        MyArrayStack<String> stack = new MyArrayStack<String>();
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(' || s.charAt(i) == ')') {
                stack.push(String.valueOf(s.charAt(i)));
            }
        }
        String check;
        while (!stack.isEmpty()) {
            check = stack.pop();
            if (check.equals(String.valueOf(')'))) {
                count++;
            }
            else {
                count--;
                if (count < 0) {
                    return false;
                }
            }
        }
        return count == 0;
    }

    public static boolean checkAllBracketsStack(String s) {
        MyArrayStack<String> stack = new MyArrayStack<String>();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(' || s.charAt(i) == ')' ||
                s.charAt(i) == '{' || s.charAt(i) == '}' ||
                s.charAt(i) == '[' || s.charAt(i) == ']' ||
                s.charAt(i) == '<' || s.charAt(i) == '>' ||
                s.charAt(i) == '\"') {
                if (s.charAt(i) == '(' || s.charAt(i) == '{' ||
                    s.charAt(i) == '[' || s.charAt(i) == '<' ||
                    s.charAt(i) == '\"') {
                    stack.push(String.valueOf(s.charAt(i)));
                }
                else {
                    String check = stack.pop();
                    if ((check.equals(String.valueOf('(')) && s.charAt(i) == ')') ||
                        (check.equals(String.valueOf('{')) && s.charAt(i) == '}') ||
                        (check.equals(String.valueOf('[')) && s.charAt(i) == ']') ||
                        (check.equals(String.valueOf('<')) && s.charAt(i) == '>') ||
                        (check.equals(String.valueOf('\"')) && s.charAt(i) == '\"')) {
                    }
                    else return false;
                }
            }
        }
        if (stack.isEmpty()) return true;
        else return false;
    }

    public static int counterStack(String s) {

        MyArrayStack<String> output = new MyArrayStack<>(); // Output Stack
        MyArrayStack<String> input = new MyArrayStack<>(); // Input Stack

        String a [] = s.split(",");
        for (int i = 0; i < a.length; i++) {
            input.push(a[i]);
        }

        while (!input.isEmpty()) {
            String element = input.pop();
            switch (element) {

                case "+":
                    int a1 = Integer.parseInt(output.pop());
                    int b1 = Integer.parseInt(output.pop());
                    output.push(Integer.toString(a1 + b1));
                    break;

                case "-":
                    int a2 = Integer.parseInt(output.pop());
                    int b2 = Integer.parseInt(output.pop());
                    output.push(Integer.toString(a2 - b2));
                    break;

                case "*":
                    int a3 = Integer.parseInt(output.pop());
                    int b3 = Integer.parseInt(output.pop());
                    output.push(Integer.toString(a3 * b3));
                    break;

                case "/":
                    int a4 = Integer.parseInt(output.pop());
                    int b4 = Integer.parseInt(output.pop());
                    output.push(Integer.toString(a4 / b4));
                    break;

                default:
                    output.push(element);
                    break;
            }
        }
        return (Integer.parseInt(output.pop()));
    }
}
