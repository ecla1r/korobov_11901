public class MyArrayStack<T> implements IMyStack<T> {

    private T [] array;
    private int capacity = 1000;
    private int size;

    public MyArrayStack() {
        this.capacity = capacity;
        this.size = 0;
        this.array = (T[]) new Object[capacity];
    }

    @Override
    public void push(T temp) {
        if (capacity > size + 1) {
            array[size] = temp;
            size++;
        }
        else {
            capacity += 1000;
            T[] tarray = (T[]) new Object[capacity];
            for (int i = 0; i < size; i++) {
                tarray[i] = array[i];
            }
            array = tarray;
            array[size] = temp;
            size++;
        }
    }

    @Override
    public T pop() {
        if (this.isEmpty()) {
            return null;
        }
        else {
            T temp = array[size - 1];
            array[size - 1] = null;
            size--;
            return temp;
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public T peek() {
        return array[size - 1];
    }
}
