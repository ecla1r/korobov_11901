public class MyLinkedStack<T> implements IMyStack<T> {

    private Elem<T> head;

    public MyLinkedStack() {
        head = null;
    }

    @Override
    public void push(T temp) {
        Elem<T> elem = new Elem<T>();
        elem.next = head;
        elem.value = temp;
        head = elem;
    }

    @Override
    public T pop() {
        T value = (T) head.value;
        head = head.next;
        return value;
    }

    @Override
    public boolean isEmpty() {
        return (head == null);
    }

    @Override
    public T peek() {
        return head.value;
    }
}
