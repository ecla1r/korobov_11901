public interface IMyStack<T> {

    public void push(T temp);
    public T pop();
    public boolean isEmpty();
    public T peek();


}
