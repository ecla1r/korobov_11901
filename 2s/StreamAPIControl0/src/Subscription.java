class Subscription {

    private User subscriber;
    private User resub;

    Subscription(User u1, User u2) {
        this.subscriber = u1;
        this.resub = u2;
    }

    User getSubscriber() {
        return subscriber;
    }

    User getResub() {
        return resub;
    }

}

