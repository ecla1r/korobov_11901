import java.util.*;
import java.util.stream.*;

public class Main {

    public static void main(String[] args) {

        User u1 = new User("Danil", "Kazan", 2001);
        User u2 = new User("Bulat", "Kazan", 2002);
        User u3 = new User("Grigory", "Kazan", 2001);
        User u4 = new User("John", "London", 2003);
        User u5 = new User("Mike", "Toronto", 2000);

        Message m1 = new Message(u1, u2, "Hello", false, "11 may");
        Message m2 = new Message(u3, u1, "Who", true, "12 may");
        Message m3 = new Message(u4, u2, "Yooo", false, "11 may");
        Message m4 = new Message(u1, u4, "Good job!", false, "11 may");
        Message m5 = new Message(u1, u5, "Broo", false, "10 may");

        Subscription s1 = new Subscription(u1, u2);
        Subscription s2 = new Subscription(u1, u3);
        Subscription s3 = new Subscription(u4, u1);
        Subscription s4 = new Subscription(u4, u3);
        Subscription s5 = new Subscription(u5, u3);
        Subscription s6 = new Subscription(u5, u2);

        List<Message> messages = new ArrayList<>();
        List<User> users = new ArrayList<>();
        List<Subscription> subs = new ArrayList<>();

        users.add(u1);
        users.add(u2);
        users.add(u3);
        users.add(u4);
        users.add(u5);

        messages.add(m1);
        messages.add(m2);
        messages.add(m3);
        messages.add(m4);
        messages.add(m5);

        subs.add(s1);
        subs.add(s2);
        subs.add(s3);
        subs.add(s4);
        subs.add(s5);
        subs.add(s6);

        System.out.println(countMessagesFromKazan(messages));
        System.out.println(sentMessagesMoreThanReceived(users, messages).toString());
        System.out.println(moreAdultAndOtherCitySubs(users, subs));
        System.out.println(unreadInCities(users, messages));
    }

    public static long countMessagesFromKazan(List<Message> messages) {
        return messages.stream().filter(m -> m.getSender().getCity().equals("Kazan")).count();
    }

    public static List<String> sentMessagesMoreThanReceived(List<User> users, List<Message> messages) {
        return users.stream().
                filter(u -> countSentMessages(u, messages) > countReceivedMessages(u, messages)).
                map(User::getName).
                collect(Collectors.toList());
    }

    public static long countSentMessages(User user, List<Message> messages) {
        return messages.stream().filter(u -> u.getSender().equals(user)).count();
    }

    public static long countReceivedMessages(User user, List<Message> messages) {
        return messages.stream().filter(u -> u.getReceiver().equals(user)).count();
    }

    public static List<String> moreAdultAndOtherCitySubs (List<User> users, List<Subscription> subs) {
        return users.stream().filter(
                u -> (countAdultSubs(u, subs) > countNotAdultSubs(u, subs) && countOtherCitySubs(u, subs) > countHomeCitySubs(u, subs))).
                map(User::getName).
                collect(Collectors.toList());
    }

    public static long countAdultSubs (User u, List<Subscription> subs) {
        return subs.stream().filter(s -> s.getResub().equals(u) && s.getSubscriber().getAge() >= 18).count();
    }

    public static long countNotAdultSubs (User u, List<Subscription> subs) {
        return subs.stream().filter(s -> s.getResub().equals(u) && s.getSubscriber().getAge() < 18).count();
    }

    public static long countHomeCitySubs (User u, List<Subscription> subs) {
        return subs.stream().filter(s -> s.getResub().equals(u) && s.getSubscriber().getCity().equals(u.getCity())).count();
    }

    public static long countOtherCitySubs (User u, List<Subscription> subs) {
        return subs.stream().filter(s -> s.getResub().equals(u) && !s.getSubscriber().getCity().equals(u.getCity())).count();
    }

    public static List<String> unreadInCities (List<User> users, List<Message> messages) {
        return users.stream().filter(
                u -> unreadMessagesInDifCities(u, messages).size() <= 2)
                .map(User::getName)
                .collect(Collectors.toList());
    }

    public static List<String> unreadMessagesInDifCities (User u, List<Message> messages) {
        List<String> a = messages.stream().filter(
                m -> !m.isRead() && m.getSender().equals(u))
                .map(Message::getReceiver).map(User::getCity)
                .collect(Collectors.toList());
        return a.stream()
                .distinct()
                .collect(Collectors.toList());
    }
}
