import java.util.*;

public class Main {

    public static void main (String[] args) {

        System.out.println(countLetter("Hello my name is Danya"));

        ArrayList<String> list = new ArrayList<>();
        list.add("Hello");
        list.add("World");
        list.add("kavooooooooo");
        System.out.println(countAverageStringLength(list));
    }

    public static int countAverageStringLength (List<String> list) { //Task 13a
        return list.stream().mapToInt(String::length).sum() / list.size();
    }

    public static Map<Character, Integer> countLetter (String s) { //Task 13b
        LinkedHashMap<Character, Integer> map = new LinkedHashMap<>();
        for (int i = "a".charAt(0); i <= "z".charAt(0); i++) {
            map.put((char) i, 0);
        }
        List<Character> list1 = new ArrayList<>();
        char[] arr = s.toLowerCase().toCharArray();
        for (char x : arr) {
            list1.add(x);
        }
        list1.stream().filter(ch -> ch >= 'a' && ch <= 'z').forEach(ch -> {
            Integer x = map.get(ch);
            x++;
            map.put(ch, x);
        });
        return map;
    }

}
