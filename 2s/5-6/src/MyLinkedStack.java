public class MyLinkedStack<T> implements IMyStack<T> {

    private int size;
    private Elem<T> head;

    public MyLinkedStack() {
        this.size = 0;
        this.head = null;
    }

    @Override
    public void push(T t) {
        Elem<T> elem = new Elem<>(t, head);
        head = elem;
        size++;
    }

    @Override
    public T pop() {
        Elem<T> elem = head;
        head = head.getNext();
        size--;
        return elem.getValue();
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public T peek() {
        return head.getValue();
    }
}