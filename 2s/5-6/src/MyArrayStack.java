public class MyArrayStack<T> implements IMyStack<T> {

    private T[] a;
    private int size;
    private int capacity;

    public MyArrayStack() {
        this.size = 0;
        this.capacity = 10;
        this.a = (T[]) new Object[capacity];
    }

    @Override
    public void push(T t) {
        if (this.capacity < this.size + 1) {
            this.capacity *= 1.5 + 1;
            T[] newArr = (T[]) new Object[this.capacity];
            for (int i = 0; i < this.size; i++) {
                newArr[i] = this.a[i];
            }
            this.a = newArr;
        }
        this.a[size] = t;
        this.size++;
    }

    @Override
    public T pop() {
        if (!this.isEmpty()) {
            T cur = this.a[this.size - 1];
            this.a[this.size - 1] = null;
            size--;
            return cur;
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public T peek() {
        T cur = this.pop();
        this.push(cur);
        return cur;
    }
}
