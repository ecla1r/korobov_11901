public class Elem<T> {

    private T value;
    private Elem<T> next;

    public Elem(T value, Elem<T> next) {
        this.value = value;
        this.next = next;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public Elem<T> getNext() {
        return next;
    }

    public void setNext(Elem<T> next) {
        this.next = next;
    }
}
