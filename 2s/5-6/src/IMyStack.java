public interface IMyStack<T> {

    void push(T t);
    T pop();
    boolean isEmpty();
    T peek();

}
