import operator
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import lxml
import requests
from graphviz import Digraph
import csv

DOMAIN = 'youtube.com'
HOST = 'http://' + DOMAIN
graph = Digraph(comment='graph')
linksMap = []
namesMap = {}
FORBIDDEN_PREFIXES = ['#', 'tel:', 'mailto:']
headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
response = requests.get(HOST, headers=headers)


def get_key(a, value):
    for k, v in a.items():
        if v == value:
            return k


def add_all_links(depth, url, max_depth):
    if depth > max_depth:
        return
    all_links = []  # создаем массив ссылок для рекурсии
    request = requests.get(url, headers=headers)  # получаем html код страницы
    soup = BeautifulSoup(request.content, 'lxml')  # парсим его с помощью BeautifulSoup
    for tag_a in soup.find_all('a', href=lambda v: v is not None):  # рассматриваем все теги <a>
        link = tag_a['href']
        if all(not link.startswith(prefix) for prefix in FORBIDDEN_PREFIXES):
            if link.startswith('/') and not link.startswith('//'):  # проверяем, является ли ссылка относительной
                link = HOST + link  # преобразуем относительную ссылку в абсолютную
            if urlparse(link).netloc == DOMAIN:  # проверяем домен
                if depth != max_depth:
                    if link in namesMap.values():  # проверяем, что мы ещё не обрабатывали такую ссылку
                        graph.edge(str(get_key(namesMap, url)), str(get_key(namesMap, link)))
                        linksMap.append([get_key(namesMap, url), get_key(namesMap, link)])
                    else:
                        graph.node(str(len(namesMap)), link)
                        graph.edge(str(get_key(namesMap, url)), str(len(namesMap)))
                        linksMap.append([get_key(namesMap, url), len(namesMap)])
                        namesMap[len(namesMap)] = link
                        all_links.append(link)
                else:
                    if link in namesMap.values():
                        graph.edge(str(get_key(namesMap, url)), str(get_key(namesMap, link)))
                        linksMap.append([get_key(namesMap, url), get_key(namesMap, link)])
    if depth < max_depth:
        for link in all_links:
            add_all_links(depth + 1, link, max_depth=max_depth)


def links_start(root_url, max_depth):
    namesMap[0] = root_url
    graph.node(str(0), root_url)
    add_all_links(0, root_url, max_depth=max_depth)


def trasponent_matrix(matrix):
    for i in range(len(matrix)):
        for j in range(len(matrix)):
            if i < j:
                matrix[i][j], matrix[j][i] = matrix[j][i], matrix[i][j]


def multiply_matrix(matrix, beta):
    for i in range(len(matrix)):
        for j in range(len(matrix)):
            matrix[i][j] *= beta
    return matrix


def multiply_vector(vect, beta):
    for i in range(len(vect)):
        vect[i] *= beta
    return vect


def first_vector(matrix):
    length = len(matrix)
    vect = [[0] for i in range(length)]
    for i in range(length):
        vect[i] = 1 / length
    return vect


def matrix_x_vector(matrix, vect):
    length = len(matrix)
    new_vect = [[0] for i in range(length)]
    for i in range(length):
        new_vect_val = 0
        for j in range(length):
            new_vect_val += vect[j] * matrix[i][j]
        new_vect[i] = new_vect_val
    return new_vect


def vect_sum(vect1, vect2):
    vect3 = [[0] for i in range(len(vect1))]
    for i in range(len(vect1)):
        vect3[i] = vect1[i] + vect2[i]
    return vect3


def calculate_pagerank(matrix, beta, iter):
    trasponent_matrix(matrix)
    matrix = multiply_matrix(matrix, beta)
    v = first_vector(matrix)
    add_v = multiply_vector(v, 1 - beta)
    for i in range(iter):
        v = matrix_x_vector(matrix, v)
        v = vect_sum(v, add_v)
    return v


def main():
    links_start(HOST + '/', 3)
    graph.render('graph', view=True)
    count = 0
    for i in range(len(linksMap)):
        if linksMap[i][1] > count:
            count = linksMap[i][1]
    count += 1
    matrix = [[0] * count for i in range(count)]
    outLinks = [0] * count
    for i in range(len(linksMap)):
        matrix[linksMap[i][0]][linksMap[i][1]] += 1
        outLinks[linksMap[i][0]] += 1
    file = open('Transition Matrix.csv', 'w')
    with file:
        writer = csv.writer(file, delimiter=',')
        writer.writerows(matrix)
    for i in range(len(matrix)):
        for j in range(len(matrix)):
            if outLinks[i] != 0:
                matrix[i][j] /= outLinks[i]
    beta = 0.8
    pg = calculate_pagerank(matrix, beta, 20)
    pgmap = {}
    for i in range(len(namesMap)):
        pgmap[namesMap.get(i)] = pg[i]
    sorted_pgmap = sorted(pgmap.items(), key=operator.itemgetter(1), reverse=True)
    file1 = open('Most Ranked Links.csv', 'w')
    with file1:
        writer = csv.writer(file1, delimiter=',')
        writer.writerows(sorted_pgmap)


if __name__ == '__main__':
    main()
