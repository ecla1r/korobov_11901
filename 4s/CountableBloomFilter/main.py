import math
import zlib

est = 0.0001
k = math.ceil(math.log(est, 0.5))


def main():
    with open("text.txt", encoding="utf-8") as file:
        text = file.read()
    words = text.split()
    wordsset = set()
    for word in words:
        word = word.lower().strip(".").strip(",").strip("?").strip(":").strip(";").strip("'").strip("(").strip(")")
        wordsset.add(word)
    m = len(wordsset)
    n = math.ceil(k * m / math.log(2, math.e))
    cbf = [0 for i in range(n)]
    for word in wordsset:
        indexes = add_hash(word, k, n)
        for ind in indexes:
            cbf[ind] += 1

    check = ['alexander', 'india', 'feather', 'cyrus', 'greek', 'empire', 'datamining', 'roxane', 'cavalry', 'conquer']
    for word in check:
        bool = True
        indexes = add_hash(word, k, n)
        for ind in indexes:
            if cbf[ind] == 0:
                bool = False
                break
        if not bool:
            print('Word "' + word + '" is not in the text')
        else:
            print('Word "' + word + '" is in the text with probability of', fp_prob(n, m, k), '%')


def add_hash(input_word, hash_count, n):
    indexes = list()
    if hash_count % 2 == 1:
        indexes.append(math.ceil(n / len(input_word)))
    hash_count = math.floor(hash_count / 2)
    for j in range(hash_count):
        ind1 = math.ceil(int(zlib.adler32(input_word.encode('utf-8'))) / (j + 1)) % n
        indexes.append(ind1)
        ind2 = math.ceil(int(zlib.crc32(input_word.encode('utf-8'))) / (j + 1)) % n
        indexes.append(ind2)
    return indexes


def fp_prob(n, m, k):
    power = -1 * k * m / n
    temp = 1 - pow(math.e, power)
    return pow(temp, k) * 100


if __name__ == '__main__':
    main()
