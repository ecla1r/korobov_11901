package ru.itis.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.itis.dto.UserDto;

@Controller
public class SignInController {

    @GetMapping("/signIn")
    public String getSignInPage(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "signIn";
    }
}
