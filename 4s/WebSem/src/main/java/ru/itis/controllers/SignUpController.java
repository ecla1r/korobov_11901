package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.UserDto;
import ru.itis.services.SignUpService;

import javax.transaction.Transactional;
import javax.validation.Valid;

@Transactional
@Controller
public class SignUpController {


    @Autowired
    private SignUpService signUpService;

    @GetMapping("/success_signUp")
    public String getSuccessPage() {
        return "success_signUp";
    }

    @GetMapping("/signUp")
    public String registration(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "signUp";
    }

    @RequestMapping(value="/confirm/{code}", method=RequestMethod.GET)
    public String verificationToken(@PathVariable("code") String code){
        if (!signUpService.findFirstByConfirmCode(code).isPresent()) {
            return "signUp";
        } else {
            signUpService.updateStatus(code);
            return "redirect:/signIn";
        }
    }

    @RequestMapping(value = "/signUp", method = RequestMethod.POST)
    public String addUser(@Valid UserDto user, BindingResult result, Model model) {
        if (!result.hasErrors()) {
            signUpService.signUp(user);
            return "redirect:/success_signUp";
        } else {
            model.addAttribute("userDto", user);
            return "signUp";
        }
    }
}
