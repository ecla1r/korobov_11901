package ru.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.models.User;
import ru.itis.validation.ValidName;
import ru.itis.validation.ValidPassword;

import javax.validation.constraints.Email;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    @ValidName(message = "{errors.incorrect.firstName}")
    private String firstName;
    @ValidName(message = "{errors.incorrect.lastName}")
    private String lastName;
    @Email(message = "{errors.incorrect.email}")
    private String email;
    @ValidPassword(message = "{errors.invalid.password}")
    private String password;

    public static UserDto convert(User user) {
        if (user == null) {
            return null;
        }
        return UserDto.builder()
                .email(user.getEmail())
                .password(user.getPassword())
                .email(user.getEmail())
                .password(user.getPassword())
                .build();
    }

    public static List<UserDto> from(List<User> users) {
        return users.stream()
                .map(UserDto::convert)
                .collect(Collectors.toList());
    }

}