package ru.itis.services;

import ru.itis.dto.MethodCallDto;

public interface MethodCallService {
    void methodCall(MethodCallDto methodLogDto);
}

