package ru.itis.services;

import org.springframework.stereotype.Component;
import ru.itis.dto.UserDto;
import ru.itis.models.User;

import java.util.List;

@Component
public interface UsersService {
    List<UserDto> getAllUsers();
    //List<UserDto> getAllUsers(int page, int size);
    void addUser(UserDto userDto);
    void addUser(User user);
    UserDto getUser(Long userId);

    void banAll();
}
