package ru.itis.services;

import org.springframework.stereotype.Component;
import ru.itis.dto.UserDto;
import ru.itis.models.User;

import java.util.Optional;

@Component
public interface SignUpService {
    void signUp(User user);
    void signUp(UserDto user);
    Optional<User> findFirstByConfirmCode(String code);
    void updateStatus(String code);
}
