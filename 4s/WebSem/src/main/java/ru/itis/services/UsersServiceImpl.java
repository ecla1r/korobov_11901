package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.dto.UserDto;
import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public List<UserDto> getAllUsers() {
        return UserDto.from(usersRepository.findAll());
    }

    @Override
    public void addUser(UserDto userDto) {
        usersRepository.save(User.builder()
        .email(userDto.getEmail())
        .password(userDto.getPassword())
        .build());
    }

    @Override
    public void addUser(User user) {
        usersRepository.save(User.builder()
        .firstName(user.getFirstName())
        .lastName(user.getLastName())
        .email(user.getEmail())
        .password(user.getPassword())
        .build());
    }

    @Override
    public UserDto getUser(Long userId) {
        return UserDto.convert(usersRepository.findById(userId).orElse(null));
    }

    @Override
    public void banAll() {
        List<User> users = usersRepository.findAll();
        for (User user : users) {
            if (!user.isAdmin()) {
                user.setBanState(User.State.BANNED);
                usersRepository.save(user);
            }
        }
    }

}
