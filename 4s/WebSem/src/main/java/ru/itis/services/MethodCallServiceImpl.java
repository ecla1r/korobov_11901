package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.dto.MethodCallDto;
import ru.itis.models.MethodCall;
import ru.itis.repositories.MethodCallRepository;

@Service
public class MethodCallServiceImpl implements MethodCallService {

    private MethodCallRepository methodCallRepository;

    public MethodCallServiceImpl(MethodCallRepository methodCallRepository) {
        this.methodCallRepository = methodCallRepository;
    }

    @Override
    public void methodCall(MethodCallDto methodCallDto) {
        methodCallRepository.save(MethodCall
                .builder()
                .methodName(methodCallDto.getMethodName())
                .build());
    }
}
