package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itis.models.MethodCall;

@Repository
public interface MethodCallRepository extends JpaRepository<MethodCall, Long> {
}

