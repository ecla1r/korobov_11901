package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.models.User;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {

    Optional<User> findFirstByConfirmCode (String confirmCode);

    Optional<User> findByEmail (String email);

    @Modifying
    @Query("UPDATE User user SET user.state = 'CONFIRMED' WHERE user.confirmCode like concat('%', :code, '%')")
    void updateStatus(@Param("code") String code);
}
