package ru.itis.models;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "account")
public class User implements Serializable {

    private static final long serialVersionUID = 4925653472661638001L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String confirmCode;

    @Enumerated(EnumType.STRING)
    private Status state;

    @Enumerated(EnumType.STRING)
    private State banState;

    @Enumerated(EnumType.STRING)
    private Role role;

    public enum Status {
        CONFIRMED, UNCONFIRMED;
    }

    public enum State {
        ACTIVE, BANNED;
    }

    public enum Role {
        USER, ADMIN;
    }

    public boolean isActive() {
        return this.banState == State.ACTIVE;
    }

    public boolean isBanned() {
        return this.banState == State.BANNED;
    }

    public boolean isAdmin() {
        return this.role == Role.ADMIN;
    }
}
