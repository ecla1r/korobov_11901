package ru.itis.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.dto.MethodCallDto;
import ru.itis.services.MethodCallService;

@Component
@Aspect
public class MethodsCaller {
    @Autowired
    private MethodCallService methodsCallerService;

    @Around(value = "execution(* ru.itis.controllers.*.*(..))")
    public Object methodCallerLog(ProceedingJoinPoint joinPoint) throws Throwable {
        Object object = joinPoint.proceed();
        String methodName = joinPoint.getTarget().getClass().getName() + " " + joinPoint.getSignature().getName();

        methodsCallerService.methodCall(MethodCallDto
                .builder()
                .methodName(methodName)
                .build());

        return object;
    }

}
