import math
from random import randint
import itertools


def main():
    goodsList = ['milk', 'bread', 'water', 'cheese', 'tomatoes', 'potatoes', 'lettuce', 'pasta', 'chocolate', 'beer', 'wine', 'bacon']

    goods = {}
    for i in range(len(goodsList)):
        goods[i + 1] = goodsList[i]

    single = {}
    for i in range(len(goods)):
        single[i + 1] = 0

    double = {}
    for i in range(1, len(goods)):
        for j in range(i + 1, len(goods) + 1):
            pair = (i, j)
            double[pair] = 0

    n_buckets = randint(100, 1000)
    support = math.floor(n_buckets * 0.4)
    buckets = [0 in range(n_buckets)]

    hash1 = [[] for i in range(len(goodsList))]
    hash2 = [[] for i in range(len(goodsList))]

    for i in range(n_buckets):
        cur = []
        for j in range(len(goods)):
            rand = randint(0, 100)
            if rand >= randint(50, 60 + math.floor(2 * j)):
                cur.append(j + 1)
                single[j + 1] += 1
            for pair in itertools.combinations(cur, 2):
                double[pair] += 1
                modulus1 = (pair[0] + pair[1]) % (len(goodsList))
                modulus2 = (pair[0] + 2 * pair[1]) % (len(goodsList))
                hash1[modulus1].append(pair)
                hash2[modulus2].append(pair)
        buckets.append(cur)

    hash_bucket_1 = set()
    for pairs in hash1:
        if len(pairs) < 6 * support:
            pairs.clear()
        for pair in pairs:
            hash_bucket_1.add(pair)
    hash_bucket_2 = set()
    for pairs in hash2:
        for pair in pairs:
            if pair not in hash_bucket_1:
                pairs.remove(pair)
        if len(pairs) < 6 * support:
            pairs.clear()
        for pair1 in pairs:
            hash_bucket_2.add(pair1)
    result = set()
    for pair in hash_bucket_2:
        if single[pair[0]] >= support and single[pair[1]] >= support:
            result.add((goodsList[pair[0]], goodsList[pair[1]]))
    for i in range(len(single)):
        if single[i + 1] >= support:
            result.add(goodsList[i + 1])
    for item in result:
        print(item)


if __name__ == '__main__':
    main()
