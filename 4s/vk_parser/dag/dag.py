import requests
import psycopg2
from airflow.models import DAG
from airflow.utils.dates import days_ago
from airflow.operators.python import PythonOperator

args = {
    'owner': 'airflow',
    'start_date': days_ago(1)
}
dag = DAG(dag_id='vk_dag', default_args=args, schedule_interval='@daily')


token = '80c5907c80c5907c80c5907cd780b23cf0880c580c5907ce048abe190ba15c2323dba27'
domain = 'itis_kfu'
posts_count = 200

database = 'postgres'
user = 'postgres'
password = 'Ecla1r4111'
host = 'postgres.cz9udtqpf4tm.us-east-1.rds.amazonaws.com'
port = 5432


def take_posts():
    offset = 0
    posts = []
    while offset < posts_count:
        response = requests.get('https://api.vk.com/method/wall.get',
                                params={
                                    'access_token': token,
                                    'v': 5.126,
                                    'domain': domain,
                                    'count': 100,
                                    'offset': offset
                                })
        data = response.json()['response']['items']
        offset += 100
        posts.extend(data)
    return posts


def count_words():
    words_dict = {}
    posts = take_posts()
    for post in posts:
        text = post['text']
        words = text.split()
        for word in words:
            word.lower()
            if word in words_dict:
                words_dict[word] += 1
            else:
                words_dict[word] = 1
    return words_dict


def db_add():
    connect = psycopg2.connect(database=database, user=user, password=password, host=host, port=port)
    cur = connect.cursor()
    cur.execute("create table if not exists words(word varchar, count integer)")
    connect.commit()
    cur.execute("truncate table words")
    connect.commit()
    words = count_words()
    for word in words.keys():
        name = word
        count = words[word].__str__()
        cur.execute("insert into words (word, count) values ('" + name + "', " + count + ")")
    connect.commit()


with dag:
    parser_task = PythonOperator(
        task_id='parser',
        python_callable=db_add,
        provide_context=True
    )
parser_task
