package ru.itis.security.token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.itis.redis.repositories.RedisUsersRepository;
import ru.itis.repositories.TokensRepository;
import ru.itis.repositories.UsersRepository;
import ru.itis.utils.TokenProvider;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AccessTokenAuthenticationFilter extends OncePerRequestFilter {

    @Value("${jwt.secret}")
    private String secretWord;

    @Value("${jwt.accessExpInMs}")
    private Long accessExp;

    private TokenProvider provider;

    private TokensRepository tokensRepository;

    private UsersRepository usersRepository;

    private RedisUsersRepository redisUsersRepository;

    @Autowired
    public AccessTokenAuthenticationFilter(TokenProvider provider, TokensRepository tokensRepository, UsersRepository usersRepository, RedisUsersRepository redisUsersRepository) {
        this.provider = provider;
        this.tokensRepository = tokensRepository;
        this.usersRepository = usersRepository;
        this.redisUsersRepository = redisUsersRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String accessToken = request.getHeader("access-token");
        if (accessToken != null) {
            if (provider.isValid(accessToken)) {
                TokenAuthentication tokenAuthentication = new TokenAuthentication(provider.getClaim("redisId", accessToken).asString());
                SecurityContextHolder.getContext().setAuthentication(tokenAuthentication);
            } else {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                response.addHeader("user-id", provider.decode(accessToken).getSubject());
            }
        }
        filterChain.doFilter(request, response);
    }
}
