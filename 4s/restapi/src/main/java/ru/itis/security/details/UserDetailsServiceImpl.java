package ru.itis.security.details;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.itis.models.User;
import ru.itis.redis.models.RedisUser;
import ru.itis.redis.repositories.RedisUsersRepository;
import ru.itis.repositories.UsersRepository;

import java.util.List;

@Component("tokenUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private UsersRepository usersRepository;

    private RedisUsersRepository redisUsersRepository;

    @Autowired
    public UserDetailsServiceImpl(RedisUsersRepository redisUsersRepository, UsersRepository usersRepository) {
        this.redisUsersRepository = redisUsersRepository;
        this.usersRepository = usersRepository;
    }

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String redis_id) throws UsernameNotFoundException {
        User user;
        try {
            RedisUser redisUser = redisUsersRepository.findById(redis_id)
                    .orElseThrow(() -> new UsernameNotFoundException("User not found"));
            List<String> tokens = redisUser.getTokens();
            if (tokens != null && !tokens.isEmpty()) {
                user = usersRepository.findById(redisUser.getUserId())
                        .orElseThrow(() -> new UsernameNotFoundException("User not found"));
                return new UserDetailsImpl(user);
            }
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
        return new UserDetailsImpl(new User());
    }
}
