package ru.itis.security.token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.itis.models.Token;
import ru.itis.redis.services.RedisUsersService;
import ru.itis.utils.TokenProvider;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RefreshTokenAuthenticationFilter extends OncePerRequestFilter {

    private RedisUsersService redisUsersService;

    private TokenProvider provider;

    @Autowired
    public RefreshTokenAuthenticationFilter(RedisUsersService redisUsersService, TokenProvider provider) {
        this.redisUsersService = redisUsersService;
        this.provider = provider;
    }

    private final RequestMatcher refreshRequest = new AntPathRequestMatcher("/refresh/**", "POST");

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (refreshRequest.matches(request)) {
            String access = request.getHeader("access-token");
            if (access != null) {
                Token refresh = redisUsersService.getUserRefresh(access);
                request.setAttribute("refresh-state", provider.isValid(refresh.getToken()));
            }
        }
        filterChain.doFilter(request, response);
    }
}
