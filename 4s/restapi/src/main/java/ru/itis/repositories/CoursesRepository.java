package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.Course;

public interface CoursesRepository extends JpaRepository<Course, Long> {
}

