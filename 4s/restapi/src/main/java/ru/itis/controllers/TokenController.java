package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.dto.TokenDto;
import ru.itis.redis.services.RedisUsersService;

import javax.servlet.http.HttpServletRequest;

@RestController
public class TokenController {

    private RedisUsersService redisUsersService;

    @Autowired
    public TokenController(RedisUsersService redisUsersService) {

        this.redisUsersService = redisUsersService;
    }

    @PostMapping("/refresh/{user-id}")
    public ResponseEntity<TokenDto> refresh(@PathVariable("user-id") Long id, HttpServletRequest request) {
        Boolean status = (Boolean) request.getAttribute("refresh-state");

        if (status) {
            String token = redisUsersService.updateUserTokens(id);

            return ResponseEntity.ok(TokenDto.builder()
                    .token(token)
                    .build());
        }
        return ResponseEntity.status(401).build();
    }
}
