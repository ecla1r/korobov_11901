package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.dto.EmailPasswordDto;
import ru.itis.dto.TokenDto;
import ru.itis.services.LoginService;


@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public TokenDto login(@RequestBody EmailPasswordDto emailPassword) {
        return loginService.login(emailPassword);
    }
}

