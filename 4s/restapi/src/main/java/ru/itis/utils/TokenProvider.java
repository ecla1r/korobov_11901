package ru.itis.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.itis.models.Token;
import ru.itis.models.User;
import ru.itis.redis.models.RedisUser;
import ru.itis.redis.repositories.RedisUsersRepository;

import java.util.Date;
import java.util.Map;

@Component
public class TokenProvider {

    private RedisUsersRepository redisUsersRepository;

    @Autowired
    public TokenProvider(RedisUsersRepository redisUsersRepository) {
        this.redisUsersRepository = redisUsersRepository;
    }

    @Value("${jwt.max.user.tokens}")
    private Integer maxAmount;

    @Value("${jwt.secret}")
    private String secretWord;

    @Value("${jwt.accessExpInMs}")
    private Long accessExp;

    @Value("${jwt.refreshExpInMs}")
    private Long refreshExp;

    public String getAccessToken(User user) {

        Date accessExpDate = new Date();
        accessExpDate.setTime(System.currentTimeMillis() + accessExp);

        String access = JWT.create()
                .withSubject(user.getId().toString())
                .withClaim("redisId", user.getRedisId())
                .withClaim("role", String.valueOf(user.getRole()))
                .withClaim("expires", accessExpDate.getTime())
                .sign(Algorithm.HMAC256(secretWord));

        return access;
    }

    public String getRefreshToken(User user) {

        Date refreshExpDate = new Date();
        refreshExpDate.setTime(System.currentTimeMillis() + refreshExp);

        String refresh = JWT.create()
                .withSubject(user.getId().toString())
                .withClaim("expires", refreshExpDate.getTime())
                .sign(Algorithm.HMAC256(secretWord));

        return refresh;
    }

    public Token getUserRefresh(String token) {

        RedisUser redisUser = redisUsersRepository
                .findById(getClaim("redisId", token).asString())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        return Token.builder()
                .token(redisUser.getTokens().get(0))
                .build();
    }

    public boolean isValid(String token) {

        Long currentDate = new Date().getTime();
        Long expires = decode(token).getClaim("expires").asLong();

        return currentDate < expires;
    }

    public DecodedJWT decode(String token) {
        return JWT.require(Algorithm.HMAC256(secretWord))
                .build()
                .verify(token);
    }

    public Claim getClaim(String claim, String token) {

        Map<String, Claim> claims = decode(token).getClaims();

        return claims.get(claim);
    }

}
