package ru.itis.redis.services;

import ru.itis.models.Token;
import ru.itis.models.User;

public interface RedisUsersService {
    void addTokenToUser(User user, String token);
    void deleteToken(User user, String token);
    String updateUserTokens(Long userId);
    Token getUserRefresh(String token);
}
