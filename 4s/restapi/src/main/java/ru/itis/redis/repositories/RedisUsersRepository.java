package ru.itis.redis.repositories;

import org.springframework.data.keyvalue.repository.KeyValueRepository;
import ru.itis.redis.models.RedisUser;

public interface RedisUsersRepository extends KeyValueRepository<RedisUser, String> {
}
