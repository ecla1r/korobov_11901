package ru.itis.redis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.itis.models.Token;
import ru.itis.models.User;
import ru.itis.redis.models.RedisUser;
import ru.itis.redis.repositories.RedisUsersRepository;
import ru.itis.repositories.UsersRepository;
import ru.itis.utils.TokenProvider;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Service
public class RedisUsersServiceImpl implements RedisUsersService {

    private RedisUsersRepository redisUsersRepository;
    private UsersRepository usersRepository;
    private TokenProvider provider;

    @Autowired
    public RedisUsersServiceImpl(RedisUsersRepository redisUsersRepository, UsersRepository usersRepository, TokenProvider provider) {

        this.redisUsersRepository = redisUsersRepository;
        this.usersRepository = usersRepository;
        this.provider = provider;
    }

    @Override
    public void addTokenToUser(User user, String token) {
        String redisId = user.getRedisId();
        RedisUser redisUser;
        if (redisId != null) {
            redisUser = redisUsersRepository.findById(redisId)
                    .orElseThrow(() -> new UsernameNotFoundException("User not found"));
            List<String> tokens = redisUser.getTokens();
            if (tokens == null) {
                redisUser.setTokens(new LinkedList<>());
            }
            assert tokens != null;
            if (tokens.isEmpty()) {
                redisUser.getTokens().add(token);
            } else {
                tokens.remove(tokens.get(0));
                tokens.add(token);
            }
        } else {
            redisUser = RedisUser.builder()
                    .userId(user.getId())
                    .tokens(Collections.singletonList(token))
                    .build();
        }
        redisUsersRepository.save(redisUser);
        user.setRedisId(redisUser.getId());
        usersRepository.save(user);
    }

    @Override
    public void deleteToken(User user, String token) {
        RedisUser redisUser = redisUsersRepository.findById(user.getRedisId())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
        redisUser.getTokens().remove(token);
        redisUsersRepository.save(redisUser);
    }

    @Override
    public String updateUserTokens(Long userId) {
        User user = usersRepository.findById(userId)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
        String access = provider.getAccessToken(user);
        String refresh = provider.getRefreshToken(user);
        addTokenToUser(user, refresh);

        return access;
    }

    @Override
    public Token getUserRefresh(String token) {
        RedisUser redisUser = redisUsersRepository
                .findById(provider.getClaim("redisId", token).asString())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        return Token.builder()
                .token(redisUser.getTokens().get(0))
                .build();
    }
}
