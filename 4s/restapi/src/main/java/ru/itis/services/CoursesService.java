package ru.itis.services;

import org.springframework.http.ResponseEntity;
import ru.itis.dto.CourseDto;
import ru.itis.dto.TeacherDto;
import ru.itis.models.Course;
import java.util.List;

public interface CoursesService {
    List<Course> getAllCourses();

    Course addCourse(CourseDto course);

    Course addTeacherIntoCourse(Long courseId, TeacherDto teacher);
}

