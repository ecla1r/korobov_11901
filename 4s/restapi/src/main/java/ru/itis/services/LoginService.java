package ru.itis.services;

import ru.itis.dto.EmailPasswordDto;
import ru.itis.dto.TokenDto;

public interface LoginService {
    TokenDto login(EmailPasswordDto emailPassword);
}

