package ru.itis.services;


import ru.itis.dto.TeacherDto;
import ru.itis.models.Teacher;
import java.util.List;

public interface TeachersService {
    List<TeacherDto> getAllTeachers();

    TeacherDto addTeacher(TeacherDto teacher);

    TeacherDto updateTeacher(Long teacherId, TeacherDto teacher);

    void deleteTeacher(Long teacherId);
}

