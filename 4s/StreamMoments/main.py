from random import randint
from datetime import datetime
import csv

variables_number = 500
n = 1000000
nums_count_map = {}
var_list = [[0, 0] for i in range(variables_number)]


def countMoments():
    varCount = 0
    for pos in range(n):
        num = randint(0, 1000)
        if num not in nums_count_map.keys():
            nums_count_map[num] = 1
        else:
            nums_count_map[num] += 1
        for i in range(len(var_list)):
            if var_list[i][0] == num:
                var_list[i][1] += 1
        if pos % (n / variables_number) == (variables_number / 2):
            var_list[varCount][0] = num
            var_list[varCount][1] = 1
            varCount += 1
    return var_list


def countSecondMoment(var_list):
    summ = 0
    for i in range(len(var_list)):
        summ += n * (2 * var_list[i][1] - 1)
    summ /= len(var_list)
    return summ


def sqSecondMoment(nums_count_map):
    summ = 0
    for i in nums_count_map.values():
        summ += i * i
    return summ


def firstMoment(nums_count_map):
    summ = 0
    for i in nums_count_map.values():
        summ += i
    return summ


def zeroMoment(nums_count_map):
    return len(nums_count_map)


def main():
    start = datetime.now()
    countMoments()
    a1 = countSecondMoment(var_list)
    a2 = sqSecondMoment(nums_count_map)
    print('Estimated 2nd moment for ', variables_number, ' variables: ', a1, ' --- True: ', a2)
    print('Diff: ', abs(a1 - a2))
    print('1st moment for ', variables_number, ' variables: ', firstMoment(nums_count_map))
    print('Zero moment for ', variables_number, ' variables: ', zeroMoment(nums_count_map))
    filename = 'NumCount' + variables_number.__str__() + '.csv'
    file = open(filename, 'w', newline='')
    maplist = []
    for key, value in nums_count_map.items():
        temp = [key, value]
        maplist.append(temp)
    with file:
        writer = csv.writer(file, delimiter=',')
        writer.writerows(maplist)
    end = datetime.now()
    execTime = end - start
    print('Time elapsed: ', execTime.total_seconds())


if __name__ == '__main__':
    main()
