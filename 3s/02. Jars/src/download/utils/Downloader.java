package download.utils;

import java.net.*;
import java.io.*;
import java.util.*;

public class Downloader {

    public static void download(String url, String path) {

        try {
            URL link  = new URL(url);

            InputStream bin = new BufferedInputStream(link.openStream());
            File file = new File(String.valueOf(UUID.randomUUID()));

            OutputStream bout = new BufferedOutputStream(new FileOutputStream(path + "/" + file.getName() + ".jpg"));
            int i = bin.read();
            while (i >= 0) {
                bout.write(i);
                i = bin.read();
            }
            bin.close();
            bout.close();
            System.out.println("Done!");

        } catch (MalformedURLException e1) {
            throw new IllegalArgumentException(e1);
        } catch (FileNotFoundException e2) {
            throw new IllegalArgumentException(e2);
        } catch (IOException e3) {
            throw new IllegalArgumentException(e3);
        }
    }
}
