package download.utils;

import com.beust.jcommander.*;
 
@Parameters(separators = "=")
public class Args {

	@Parameter(names = "--mode", description = "Mode")
	public String mode = "one-thread";

	@Parameter(names = "--count", description = "Threads Count")
	public int count = 1;

	@Parameter(names = "--files", description = "URLs")
	public String files;

	@Parameter(names = "--folder", description = "Download Folder")
	public String folder = "/Users/ecla1/GitRep/korobov_11901/3s/02. Jars";
}