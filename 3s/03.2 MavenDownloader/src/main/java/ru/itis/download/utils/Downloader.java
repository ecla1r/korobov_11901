package ru.itis.download.utils;

import java.net.*;
import java.io.*;
import java.util.*;

public class Downloader {

    public static void download(String url, String path) {

        try {
            URL link  = new URL(url);
            InputStream bis = new BufferedInputStream(link.openStream());
            File file = new File(String.valueOf(UUID.randomUUID()));

            OutputStream bos = new BufferedOutputStream(new FileOutputStream(path + "/" + file.getName() + ".jpg"));
            int i = bis.read();
            while (i >= 0) {
                bos.write(i);
                i = bis.read();
            }
            bis.close();
            bos.close();
            System.out.println("Done!");

        } catch (MalformedURLException e1) {
            throw new IllegalArgumentException(e1);
        } catch (FileNotFoundException e2) {
            throw new IllegalArgumentException(e2);
        } catch (IOException e3) {
            throw new IllegalArgumentException(e3);
        }
    }
}
