import com.google.auto.service.AutoService;
import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@AutoService(Processor.class)
@SupportedAnnotationTypes(value = {"HtmlForm", "HtmlInput"})

public class HtmlProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(HtmlForm.class);
        for (Element element : annotatedElements) {

            String path = HtmlProcessor.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            path = path.substring(1) + element.getSimpleName().toString() + ".html";
            Path out = Paths.get(path);

            Configuration cfg = new Configuration(Configuration.VERSION_2_3_30);
            cfg.setDefaultEncoding("UTF-8");

            try {
                String templateFile = element.getSimpleName().toString() + ".ftlh";
                cfg.setTemplateLoader(new FileTemplateLoader(new File("src\\main\\resources\\ftlh")));
                Template template = cfg.getTemplate(templateFile);

                HtmlForm htmlFormAnnotation = element.getAnnotation(HtmlForm.class);
                Form form = new Form(htmlFormAnnotation.method(), htmlFormAnnotation.action());
                List<Input> inputList = new ArrayList<>();
                for (Element elem : element.getEnclosedElements()) {
                    HtmlInput htmlInputAnnotation = elem.getAnnotation(HtmlInput.class);
                    if (htmlInputAnnotation != null) inputList.add(new Input(htmlInputAnnotation.type(), htmlInputAnnotation.name(), htmlInputAnnotation.placeholder()));
                }

                Map<String, Object> attributes = new HashMap<>();

                attributes.put("inputs", inputList);
                attributes.put("form", form);

                BufferedWriter writer = new BufferedWriter(new FileWriter(out.toFile().getAbsolutePath()));
                try {
                    template.process(attributes, writer);
                    writer.close();
                } catch (TemplateException e) {
                    throw new IllegalStateException(e);
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
        return true;
    }
}