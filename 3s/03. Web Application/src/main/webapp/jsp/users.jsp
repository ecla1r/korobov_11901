<%@ page import="java.util.List" %>
<%@ page import="ru.itis.models.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Users</title>
</head>
<body>
<h1 style="color: ${cookie.get("color").value}">Users</h1>
<form method="post" action="/users" >
    <select name="color">
        <option value="red">RED</option>
        <option value="green">GREEN</option>
        <option value="blue">BLUE</option>
    </select>
    <input type="submit" value="OK">
</form>
<table>
    <th>ID</th>
    <th>USERNAME</th>
    <th>FIRST NAME</th>
    <th>LAST NAME</th>
    <th>AGE</th>
    <%
        List<User> users = (List<User>) request.getAttribute("usersForJsp");
        for (int i = 0; i < users.size(); i++) {
    %>
    <tr>
        <td><%=users.get(i).getId()%>
        </td>
        <td><%=users.get(i).getUserName()%>
        </td>
        <td><%=users.get(i).getFirst_name()%>
        </td>
        <td><%=users.get(i).getLast_name()%>
        </td>
        <td><%=users.get(i).getAge()%>
        </td>
    </tr>
    <%}%>
</table>
</body>
</html>
