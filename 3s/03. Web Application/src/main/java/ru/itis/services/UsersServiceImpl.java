package ru.itis.services;

import ru.itis.models.User;
import ru.itis.repositories.UsersRepository;

import java.util.List;

public class UsersServiceImpl implements UsersService {
    private UsersRepository usersRepository;

    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public List<User> findByName(String first_name, String last_name) {
        return this.usersRepository.findByName(first_name, last_name);
    }

    @Override
    public List<User> findAll() {
        return this.usersRepository.findAll();
    }

    @Override
    public User findById(Long id) {
        return this.usersRepository.findById(id);
    }

    @Override
    public void addUser(User user) {
        this.usersRepository.save(user);
    }

    @Override
    public User getUserByUserName(String username) {
        if (this.usersRepository.getUserByUsername(username) != null) {
            return this.usersRepository.getUserByUsername(username);
        }
        else {
            return null;
        }
    }

    @Override
    public boolean containsUser(String username) {
        return this.usersRepository.containsUser(username);
    }

}

