package ru.itis.services;

import ru.itis.models.User;

import java.util.List;

public interface UsersService {
    List<User> findByName(String first_name, String last_name);
    User findById(Long id);
    List<User> findAll();
    void addUser(User user);
    User getUserByUserName(String username);
    boolean containsUser(String username);
}

