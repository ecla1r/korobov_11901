package ru.itis.models;

import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Builder
public class User {
    private Long id;
    private String userName;
    private String password;
    private String first_name;
    private String last_name;
    private int age;

}
