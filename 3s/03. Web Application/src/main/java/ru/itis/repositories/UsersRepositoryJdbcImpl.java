package ru.itis.repositories;

import ru.itis.models.User;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryJdbcImpl implements UsersRepository {

    //language=SQL
    private static final String SQL_FIND_BY_NAME = "select * from users where first_name = ? and last_name = ?";

    //language=SQL
    private static final String SQL_FIND_ALL = "select * from users";

    //language=SQL
    private static final String SQL_FIND_BY_ID = "select from users where id = ?";

    //language=SQL
    private static final String SQL_FIND_BY_USERNAME = "select from users where username = ?";

    //language=SQL
    private static final String SQL_ADD_USER = "insert into users (username, password, first_name, last_name, age) values (?, ?, ?, ?, ?)";

    private DataSource dataSource;
    private SimpleJdbcTemplate simpleJdbcTemplate;

    public UsersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
    }

    private RowMapper<User> userRowMapper = row -> User.builder()
            .id(row.getLong("id"))
            .userName(row.getString("username"))
            .password(row.getString("password"))
            .first_name(row.getString("first_name"))
            .last_name(row.getString("last_name"))
            .age(row.getInt("age"))
            .build();

    @Override
    public void save(User entity) {
        this.simpleJdbcTemplate.execute(SQL_ADD_USER, entity.getUserName(), entity.getPassword(), entity.getFirst_name(), entity.getLast_name(), entity.getAge());    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void delete(User entity) {

    }

    @Override
    public List<User> findByName(String first_name, String last_name) {
        return this.simpleJdbcTemplate.query(SQL_FIND_BY_NAME, userRowMapper, first_name, last_name);
    }

    @Override
    public List<User> findAll() {
        return this.simpleJdbcTemplate.query(SQL_FIND_ALL, userRowMapper);
    }

    @Override
    public User findById(Long id) {
        ArrayList<User> list = (ArrayList<User>) this.simpleJdbcTemplate.query(SQL_FIND_BY_ID, userRowMapper, id);
        if (!list.isEmpty()) return list.get(0);
        else return null;
    }

    @Override
    public User getUserByUsername(String username) {
        ArrayList<User> list = (ArrayList<User>) this.simpleJdbcTemplate.query(SQL_FIND_BY_USERNAME, userRowMapper, username);
        if (!list.isEmpty()) return list.get(0);
        else return null;
    }

    @Override
    public boolean containsUser(String username) {
        return !this.simpleJdbcTemplate.query(SQL_FIND_BY_USERNAME, userRowMapper, username).isEmpty();
    }


}
