package ru.itis.repositories;

import ru.itis.models.User;

import java.util.List;

public interface UsersRepository extends CrudRepository<User> {
    List<User> findByName(String first_name, String last_name);
    User findById(Long id);
    List<User> findAll();
    User getUserByUsername(String username);
    boolean containsUser(String username);
}
