package ru.itis.repositories;

import javax.sql.DataSource;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EntityManager {
    private DataSource dataSource;
    private SimpleJdbcTemplate simpleJdbcTemplate;

    public EntityManager(DataSource dataSource) {
        this.dataSource = dataSource;
        this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
    }

    // createTable("account", User.class);
    public <T> void createTable(String tableName, Class<T> entityClass) {
        StringBuilder sql = new StringBuilder("CREATE TABLE " + tableName + " (");
        Field[] fields = entityClass.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field f = fields[i];
            if (!f.isAccessible()) f.setAccessible(true);
            String type = "";
            switch (f.getType().getSimpleName()) {
                case ("Integer") :
                    type = "int";
                    break;
                case ("Boolean") :
                    type = "boolean";
                    break;
                case ("String") :
                    type = "varchar(255)";
                    break;
                case ("Double") :
                    type = "float";
                    break;
                case ("Long") :
                    type = "bigint";
                    break;
                default: throw new IllegalStateException();
            }
            if (i == fields.length - 1) {
                sql.append(f.getName() + " " + type + "\n");
            } else sql.append(f.getName() + " " + type + ",\n");

        }
        sql.append(");");
        this.simpleJdbcTemplate.execute(sql.toString());
    }

    public void save(String tableName, Object entity) {
        Class<?> classOfEntity = entity.getClass();
        StringBuilder sql = new StringBuilder("INSERT INTO " + tableName + "VALUES (");
        Field [] fields = classOfEntity.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field f = fields[i];
            if (!f.isAccessible()) f.setAccessible(true);
            String value = "";
            switch (f.getType().getSimpleName()) {
                case ("Integer") :
                case ("Long") :
                case ("Double") :
                    try {
                        value = (String) f.get(entity);
                    } catch (IllegalAccessException e) {
                        throw new IllegalStateException(e);
                    }
                    break;
                case ("Boolean") :
                case ("String") :
                    try {
                        value = "'" + f.get(entity) + "'";
                    } catch (IllegalAccessException e) {
                        throw new IllegalStateException(e);
                    }
                    break;
                default: throw new IllegalStateException();
            }
            if (i == fields.length - 1) {
                sql.append(value);
            } else
            sql.append(value + ", ");
        }
        sql.append(");");
        this.simpleJdbcTemplate.execute(sql.toString());
    }

    // User user = entityManager.findById("account", User.class, Long.class, 10L);
    public <T, ID> T findById(String tableName, Class<T> resultType, Class<ID> idType, ID idValue) {
        String sql = "SELECT FROM " + tableName + " WHERE id = " + idValue.toString() + ";";
        ResultSet rs;
        try {
            rs = this.dataSource.getConnection().prepareStatement(sql).executeQuery();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        T entity;
        try {
            entity = resultType.getConstructor().newInstance();
            Field [] fields = resultType.getDeclaredFields();
            for (Field f : fields) {
                String name = f.getName();
                String value = rs.getString(name);
                f.set(entity, value);
            }
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | SQLException | InvocationTargetException e) {
            throw new IllegalStateException(e);
        }
        return entity;
    }
}
