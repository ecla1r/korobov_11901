package ru.itis.filters;

import ru.itis.services.UsersService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/users")
public class AuthFilter implements Filter {

    private UsersService usersService;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        this.usersService = (UsersService) servletContext.getAttribute("usersService");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        /*
        Cookie [] cookies = request.getCookies();
        Cookie auth = null;
        for (Cookie c : cookies) {
            if (c.getName().equals("Auth")) {
                auth = c;
                break;
            }
        }
        if (auth != null) {
            if (usersService.findById(Long.valueOf(auth.getValue())) != null) {
                response.sendRedirect("/users");
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            } else {
                response.sendRedirect("/login");
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }

        }
        else {
            filterChain.doFilter(request, response);
            return;
        }
        */
        if (request.getSession() == null || request.getSession().getAttribute("Auth") == null) {
            response.sendRedirect("/login");
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        response.sendRedirect("/users");
        filterChain.doFilter(servletRequest, servletResponse);
    }


    @Override
    public void destroy() {

    }
}
