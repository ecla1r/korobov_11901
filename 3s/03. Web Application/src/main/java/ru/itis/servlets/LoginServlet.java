package ru.itis.servlets;

import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itis.models.User;
import ru.itis.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private UsersService usersService;
    private PasswordEncoder passwordEncoder;

    @Override
    public void init(ServletConfig config) {
        ServletContext servletContext = config.getServletContext();
        this.usersService = (UsersService) servletContext.getAttribute("usersService");
        this.passwordEncoder = (PasswordEncoder) servletContext.getAttribute("passwordEncoder");
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher("/ftlh/login.ftlh").forward(request, response);
        } catch (ServletException | IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession(false);
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String truePassword = "";
        User user = usersService.getUserByUserName(username);

        if (user != null) {
            truePassword = user.getPassword();
        }
        if (passwordEncoder.matches(password, truePassword)) {
            /*
            Cookie cookie = new Cookie("Auth", user.getId().toString());
            cookie.setMaxAge(60 * 60 * 24);
            response.addCookie(cookie);
            */
            session.setAttribute("Auth", user.getId());
            try {
                response.sendRedirect("/users");
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else {
            try {
                response.sendRedirect("/registration");
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}

