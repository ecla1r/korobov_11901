package ru.itis.servlets;

import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itis.models.User;
import ru.itis.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {

    private UsersService usersService;
    private PasswordEncoder passwordEncoder;

    @Override
    public void init(ServletConfig config) {
        ServletContext context = config.getServletContext();
        this.usersService = (UsersService) context.getAttribute("usersService");
        this.passwordEncoder = (PasswordEncoder) context.getAttribute("passwordEncoder");
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher("/ftlh/registration.ftlh").forward(request, response);
        } catch (ServletException | IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)  {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String first_name = request.getParameter("first_name");
        String last_name = request.getParameter("last_name");
        int age = Integer.parseInt(request.getParameter("age"));
        String hashPassword = passwordEncoder.encode(password);
        User user = User
                .builder()
                .userName(username)
                .password(hashPassword)
                .first_name(first_name)
                .last_name(last_name)
                .age(age)
                .build();
        if (usersService.containsUser(username)) {
            try {
                request.getRequestDispatcher("/ftlh/login.ftlh").forward(request, response);
            } catch (ServletException | IOException e) {
                throw new IllegalStateException(e);
            }
        } else {
            usersService.addUser(user);
            request.getSession().setAttribute("Auth", user.getId());
            try {
                response.sendRedirect("/users");
                return;
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

}

