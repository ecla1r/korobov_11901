import java.util.Scanner;

public class Task1 {

	public static void main (String [] args) {

		boolean check = true;
		final int n = 5;

		String [] arr = new String [n];
		Scanner scan = new Scanner (System.in);
		for (int i = 0; i < n; i++) {
			arr[i] = scan.nextLine();
		}

		for (int i = 0; i < n; i++) {
			String [] letters;
			letters = arr[i].split("");
			int countCap = 0;
			int countLow = 0;
			for (int j = 0; j < letters.length; j++) {
				if ((letters[j].charAt(0) >= "A".charAt(0)) && (letters[i].charAt(0) <= "Z".charAt(0))) {
					countCap++;
				}
				if ((letters[j].charAt(0) >= "a".charAt(0)) && (letters[i].charAt(0) <= "z".charAt(0))) {
					countLow++;
				}
			}

			if (countCap >= countLow) {
				check = false;
				break;
			}
		}

		if (check == true) {
			for (int i = 0; i < n; i++) {
				String [] letter;
				letter = arr[i].split("");
				for (int j = 0; j < letter.length; j++) {
					if (letter[j].charAt(0) == "a".charAt(0) || 
						letter[j].charAt(0) == "A".charAt(0) || 
						letter[j].charAt(0) == "e".charAt(0) || 
						letter[j].charAt(0) == "E".charAt(0) || 
						letter[j].charAt(0) == "o".charAt(0) || 
						letter[j].charAt(0) == "O".charAt(0) || 
						letter[j].charAt(0) == "i".charAt(0) || 
						letter[j].charAt(0) == "I".charAt(0) || 
						letter[j].charAt(0) == "u".charAt(0) || 
						letter[j].charAt(0) == "U".charAt(0) || 
						letter[j].charAt(0) == "y".charAt(0) || 
						letter[j].charAt(0) == "Y".charAt(0)) {
						letter[j] = "a";
					}
					System.out.print(letter[j]); 
				}
				System.out.println("");
			}
		}
		else for (int i = 0; i < n; i++) {
				String [] letters1;
				letters1 = arr[i].split("");
				for (int j = 0; j < letters1.length; j++) {
					System.out.print(letters1[j]);
				}
				System.out.println("");
			}
	}
}