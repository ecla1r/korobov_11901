public class Rectangle extends Figure {

    private double length, width;

    public Rectangle(double l, double w) {
        this.length = l;
        this.width = w;
    }
    @Override
    public double area() {
        return width * length;
    }

    @Override
    public double perimeter() {
        return 2 * (width + length);
    }
}