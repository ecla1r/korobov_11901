public class Task1 {

	public static void main (String[]args) {

		double n = Double.parseDouble(args[0]);
		double m = Double.parseDouble(args[1]);
		double i = 1;
		double j = 1;
		double ifact = 1;
		double jfact = 1;
		double sum = 0;

		while (i <= n)
		{

            i++;

            while (j <= m)
            {

                j++;
            	sum += ifact + jfact;
            	jfact *= j;

            }

            j = 1;
            jfact = 1;
            ifact *= i;

		}

		System.out.println(sum);

	}
	
}