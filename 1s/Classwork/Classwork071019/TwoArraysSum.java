import java.util.Scanner;

public class TwoArraysSum {

	public static void main (String [] args) {

		int n = Integer.parseInt(args[0]);

		int [] a = new int [n];

		Scanner sca = new Scanner (System.in);
		for (int i = 0; i < n; i++) 
		{
			a[i] = sca.nextInt();
		}

		int [] b = new int [n];


		Scanner scb = new Scanner (System.in);
		for (int i = 0; i < n; i++) 
		{
			b[i] = scb.nextInt();
		}

		int [] c = new int [n];


		for (int i = 0; i < n; i++) 
		{
			c[i] = a[i] + b[i];
		}


		for (int i = 0; i < n; i++) 
		{
			System.out.print(c[i] + " ");
		}
	}
}
