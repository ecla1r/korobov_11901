import java.util.Scanner;

public class MoveArrayPart {

	public static void main (String [] args) {

		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
        int [] a = new int [n];
        int temp = 0;

        Scanner sca = new Scanner (System.in);
        for (int i = 0; i < n; i++) 
        {
        	a[i] = sca.nextInt();
        }

        
        for (int i = 0; i < k; i++) 
        {
        	temp = a[0];
            for (int j = 0; j < n-1; j++) 
            {
                a[j] = a[j+1];
            }
            a[n-1] = temp;
        }

        for (int i = 0; i < n; i++) 
        {
			System.out.print(a[i] + " ");
		}
	}
}