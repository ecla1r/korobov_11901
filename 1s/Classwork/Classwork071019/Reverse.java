import java.util.Scanner;

public class Reverse {

	public static void main (String [] args) {

		int n = Integer.parseInt(args[0]);
        int [] a = new int [n];
        int temp = 0;

        Scanner sca = new Scanner (System.in);
        for (int i = 0; i < n; i++) {
        	a[i] = sca.nextInt();
        }
        for (int i = 0; i < n/2; i++) {
        	temp = a[i];
        	a[i] = a[n-1-i];
        	a[n-i-1] = temp;
        }

        for (int i = 0; i < n; i++) 
        {
			System.out.print(a[i] + " ");
		}

	}
}   