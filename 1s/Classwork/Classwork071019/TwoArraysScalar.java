import java.util.Scanner;

public class TwoArraysScalar {

	public static void main (String [] args) {

		double [] a = new double [3];

		Scanner sca = new Scanner (System.in);
		for (int i = 0; i < 3; i++) 
		{
			a[i] = sca.nextDouble();
		}


		double [] b = new double [3];

		Scanner scb = new Scanner (System.in);
		for (int i = 0; i < 3; i++) 
		{
			b[i] = scb.nextDouble();
		}

		double scal = (a[0]*b[0] + a[1]*b[1] + a[2]*b[2]);
		double lengtha = Math.sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
		double lengthb = Math.sqrt(b[0]*b[0] + b[1]*b[1] + b[2]*b[2]);
		double cos = scal/lengtha/lengthb;

		System.out.println("Scalar Product = " + scal);
		System.out.println("Length of Vector a " + lengtha);
		System.out.println("Length of Vector b " + lengthb);
		System.out.println("Cosinus = " + cos);

	}

}

