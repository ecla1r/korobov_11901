import java.util.Scanner;

public class Task0 {

	public static void main(String [] args) {
        
        int n = Integer.parseInt(args[0]);
        int [] a = new int [n];

        Scanner sc = new Scanner (System.in);
        for (int i = 0; i < n; i++) {
        	a[i] = i * i;
        	System.out.print(a[i] + " ");
        }

	}
}