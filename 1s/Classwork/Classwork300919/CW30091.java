public class CW30091 {

	public static void main (String[]args) {

		double x = Double.parseDouble(args[0]);
		double k = 2;
		double kavo = 1;
		double d = x;
		double sum = 1;
		final double EPS = 1e-9;

		while (Math.abs(kavo * d / k) > EPS) 
		{

			d *= x;
			kavo *= -1;
			sum += kavo * d / k;
			k++;
			
		}

	System.out.println(sum);

    }

}
