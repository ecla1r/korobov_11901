public class CW30092 {

	public static void main (String[]args) {

		double x = Double.parseDouble(args[0]);
		double k = 2;
		double f = 1;
		double kavo = -1;
		double sum = -1;
		double fact = 1;
		double d = 1;
		final double EPS = 1e-12;

		while (Math.abs(kavo * d / fact) > EPS) 
		{

			fact *= f * (f + 1);
			kavo *= -1;
			f += 2;
			d *= x * x;
			sum += kavo * d / fact;
			k++;  

		}

        System.out.println(sum);
        
    }
    
}