package segment;

import java.util.Scanner;
import segment.segment;
import segment.check;

public class Main {

	public static void main (String [] args) {

		int n = Integer.parseInt(args[0]);
		Scanner sc = new Scanner (System.in);
		Segment [] segments = new Segment[n];
		for (int i = 0; i < n; i++) {
			Segment[i] = sc.nextLine();			
		}
		totalLength();
		checkPairCross();
	}
}