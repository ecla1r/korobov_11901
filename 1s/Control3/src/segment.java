package segment;

public class segment {

	private double x1;
	private double y1;
	private double x2;
	private double y2;

	public Segment (double x1, double y1, double x2, double y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}

	public double segLength() {
		return Math.sqrt((this.x2 - this.x1) * (this.x2 - this.x1) + (this.y2 - this.y1) * (this.y2 - this.y1));
	}

	public double area() {
		return Math.abs((this.x2 - this.x1) * (this.y2 - this.y1));
	}

	public boolean cross (Segment c) {
		if (((c.x2 - c.x1) * (this.y1 - c.y1) - (c.y2 - c.y1) * (this.x1 - c.x1)) * ((c.x2 - c.x1) * (this.y2 - c.y1) - (c.y2 - c.y1) * (this.x2 - c.x1)) < 0 &&
				((this.x2 - this.x1) * (c.y1 - this.y1) - (this.y2 - this.y1) * (c.x1 - this.x1)) * ((this.x2 - this.x1) * (c.y2 - this.y1) - (this.y2 - this.y1) * (c.x2 - this.x1)) < 0) {
			return true;
		}
		else return false;
	}

	@Override
	public boolean equals (Segment c) {
		if (this.x1 == c.x1 && this.x2 == c.x2 && this.y1 == c.y1 && this.y2 == c.y2) {
			return true;
		}
		else return false;
	}

}