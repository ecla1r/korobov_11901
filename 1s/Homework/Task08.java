/**
* @author Danil Korobov
* 11-901
* Task 08
*/
public class Task08 {

	public static void main (String [] args) {

		double n = Double.parseDouble(args[0]);
		double x = -n;
		double y = n;

		while (y >= -n) {
			while (x <= n) {
				if ((x * x + y * y) <= n * n) {
						System.out.print("1");
					}
					else System.out.print(" ");
				x++;
			}

			System.out.println();
			x = -n;
			y--;
		}
	}
}