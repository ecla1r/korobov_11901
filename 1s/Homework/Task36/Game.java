/**
 * @author Danil Korobov
 * 11-901
 * Task 36
 */

package Task36;

import java.util.Scanner;

public class Game {

    private Player p1;
    private Player p2;
    private int hit = 0;
    private Player turn;
    private Player nextturn;
    private Player swap;

    public Game(Player p1, Player p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public void start() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Which Player Starts? ");
        if(sc.nextInt() == 1) {
            turn = this.p1;
            nextturn = this.p2;
        }
        else {
            turn = this.p2;
            nextturn = this.p1;
        }

        while(p1.getHp() > 0 && p2.getHp() > 0) {
            System.out.println("Type Hit Power (1-9): ");
            hit = sc.nextInt();
            if(hit >= 1 && hit <= 9) {
                turn.hit(nextturn, hit);
                System.out.println("Player 1 hp: "+ p1.getHp() + " Player 2 hp: " + p2.getHp());
                swap = turn;
                turn = changeTurns(turn, nextturn);
                nextturn = swap;
            }
            else {
                System.out.println("Invalid!");
            }
        }
        System.out.println("Game Over");
        if (p1.getHp() > p2.getHp()) {
            System.out.println(p1.getName() + " wins!");
        }
        else{
            System.out.println(p2.getName() + " wins!");
        }
    }

    public Player changeTurns(Player turn, Player nextturn) {
        turn = nextturn;
        return turn;
    }
}
