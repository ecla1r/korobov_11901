/**
 * @author Danil Korobov
 * 11-901
 * Task 36
 */

package Task36;

public class Player {
    private String name;
    private double hp;
    public Player (String name) {
        this.name = name;
        this.hp = 10;
    }

    public void hit (Player p, double hp) {
        double chance = 1/hp;
        if (Random(chance) == true) {
            p.setHp(p.getHp() - hp);
            System.out.println("Hit!");
        }
        else System.out.println("Miss");

        System.out.println("");
    }

    public boolean Random(double chance) {
        return Math.random() <= chance ? true : false;
    }
    public double getHp(){
        return this.hp;
    }
    public void setHp(double hp){
        this.hp = hp;
    }
    public String getName(){
        return this.name;
    }
}
