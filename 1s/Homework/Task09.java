/**
* @author Danil Korobov
* 11-901
* Task 09
*/
public class Task09 {

	public static void main (String [] args) {

		double x = Double.parseDouble(args[0]);
		double y = Double.parseDouble(args[1]);

		if ((y <= x + 1) & (y <= 1 - x) & (y >= 0)) {
			System.out.println("True");
		}
		else System.out.println("False");

	}
}