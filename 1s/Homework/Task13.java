/**
* @author Danil Korobov
* 11-901
* Task 13
*/
public class Task13 {

	public static void main (String [] args) {

		double x = Double.parseDouble(args[0]);
		double y = Double.parseDouble(args[1]);
		double z = Double.parseDouble(args[2]);
		double u = 0;

		u += y * y;
		u += (z + 1.1) / (z - 2.0);
		u += (x + y + 2.1) / (x - z - 5.6) * z * y;

		System.out.println(u);
	}
}