/**
* @author Danil Korobov
* 11-901
* Task 05
*/
public class Task05 {

	public static void main (String [] args) {

		int n = Integer.parseInt(args[0]);
		int j = 0;
		int k = 0;

		for (int i = 0; i < n; i++) {
			k = 0;
			j = i;

			while (j < n) {
				System.out.print(" ");
				j++;
			}

			while (k < n) {
				System.out.print("1");
				k++;
			}
			
		System.out.println("");
		}
	} 
} 