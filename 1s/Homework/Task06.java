/**
* @author Danil Korobov
* 11-901
* Task 06
*/
public class Task06 {

	public static void main (String [] args) {

		int n = Integer.parseInt(args[0]);
		int i = 1;
		int j = 1;
		int k = 1;

		while (i <= n) {
			
			while (j <= n) {
				j++;
				System.out.print(" ");
			}

			while (k <= (2 * i - 1)) {
				k++;
				System.out.print("1");
			}

			System.out.println();
			i++;
			j = i;
			k = 1;
		}
	}
}