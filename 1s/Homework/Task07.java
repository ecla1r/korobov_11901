/**
* @author Danil Korobov
* 11-901
* Task 07
*/
public class Task07 {

	public static void main (String [] args) {

		int n = Integer.parseInt(args[0]);
		int i = 1;
		int j = 1;
		int k = 1;
		int l = 1;
		int m = 1;

		while (i <= n) {
			while (j <= (2 * n)) {
				j++;
				System.out.print(" ");
			}

			while (k <= (2 * i - 1)) {
				k++;
				System.out.print("1");
			}

			System.out.println();
			i++;
			j = i;
			k = 1;
		}
		
		System.out.println();

		i = 1;
		j = 1;
		k = 1;

		while (i <= n) {
			while (j <= n) {
				j++;
				System.out.print(" ");
			}

			while (k <= (2 * i - 1)) {
				k++;
				System.out.print("1");
			}

			while (l <= (2 * n + 1 - 2 * i)) {
				l++;
				System.out.print(" ");
			}

			while (m <= (2 * i - 1)) {
				m++;
				System.out.print("1");
			}

			System.out.println();

			i++;
			j = i;
			k = 1;
			l = 1;
			m = 1;
		}
	}
}