/**
* @author Danil Korobov
* 11-901
* Task 12
*/
public class Task12 {

	public static void main (String [] args) {

		int x = Integer.parseInt(args[0]);

		while (x > 0) {
			if (x % 10 == 5) { 
				System.out.println("true");
				break;
			}
			x /= 10;
		}

		if (x == 0) {
			System.out.println("false");
		}

	}
}