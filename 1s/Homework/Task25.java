/**
 * @author Danil Korobov
 * 11-901
 * Task 25
 */

import java.util.Scanner;
import java.util.regex.*;

public class Task25 {
    public static void main(String[] args) {
        Pattern p = Pattern.compile("");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        Matcher m = p.matcher(s);
        System.out.println(m.matches());
    }
}