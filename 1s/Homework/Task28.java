/**
 * @author Danil Korobov
 * 11-901
 * Task 28
 */

import java.util.Random;

public class Task28 {

    public static void main(String[] args) {
        Random random = new Random();
        int countGood = 0;
        int count = 0;
        while (countGood < 10) {
            int number = random.nextInt(1000000);
            if ((Integer.toString(number)).matches("([02468]{0,2}[13579]+)*([02468]{0,2})")) {
                System.out.println(number);
                countGood++;
            }
            count++;
        }
        System.out.println();
        System.out.println(count);
    }
}