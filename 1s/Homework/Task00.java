/**
* @author Danil Korobov
* 11-901
* Task 00
*/
public class Task00 {

	public static void main (String [] args) {

		System.out.println("0000       0      0    0  0  0     ");
		System.out.println("0   0     0 0     00   0  0  0     ");
		System.out.println("0   0    0   0    0 0  0  0  0     ");
		System.out.println("0   0   0000000   0  0 0  0  0     ");
		System.out.println("0   0  0       0  0   00  0  0     ");
		System.out.println("0000  0         0 0    0  0  000000");
		
	}
}