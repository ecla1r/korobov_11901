/**
 * @author Danil Korobov
 * 11-901
 * Task 23
 */

public class Task23 {

    public static void replace (String s, String total) {
        int t = 0;
        s += "   ";
        for (int i = 0; i < s.length() - 3; i++) {
            if (s.charAt(i) == 't' && s.charAt(i + 1) == 'r' && s.charAt(i + 2) == 'u' && s.charAt(i + 3) == 'e') {
                t++;
            }
            if (t == 2) {
                total += "false";
                t = 0;
                i += 3;
            }
            else {
                total += s.charAt(i);
            }

        }
        System.out.println(total);
    }

    public static void main (String [] args) {
        String s = "truetrueWHOfalseWHOtrueWHOtrue";
        String total = "";
        replace(s, total);
    }
}