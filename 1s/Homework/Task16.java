/**
* @author Danil Korobov
* 11-901
* Task 16
*/
import java.util.Scanner;

public class Task16 {

	public static void main (String [] args) {

		Scanner sca = new Scanner (System.in);

		System.out.println("Type 'x' in range of (-1;1)");
		double x = sca.nextDouble();
		double sum = 0;
		double ev = 1;
		double k = 1;
		final double EPS = 1e-9;

		if (x <= 0) {
			System.out.println("Can't count ln");
		}
		else {
			x -= 1;
			double pow = x;
			while (Math.abs(pow / k) > EPS) {
				sum += ev * pow / k;
				pow *= x;
				ev *= -1;
				k++;
			}
			
			System.out.println(sum);
		}
	}
}