/**
* @author Danil Korobov
* 11-901
* Task 03
*/
public class Task03 {

	public static void main (String [] args) {

		int n = Integer.parseInt(args[0]);
		int s = 0;

		while (n > 0) {
			s += n % 10;
			n /= 10;
		}

		System.out.println(s);

	}
}