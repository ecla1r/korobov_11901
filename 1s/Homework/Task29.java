/**
 * @author Danil Korobov
 * 11-901
 * Task 29
 */

import java.util.Random;

public class Task29 {

    public static void main(String[] args) {
        Random random = new Random();
        int countGood = 0;
        int count = 0;
        while (countGood < 10) {
            int number = random.nextInt(1000000);
            if ((Integer.toString(number)).matches("[2468][02468]{3,4}")) {
                System.out.println(number);
                countGood++;
            }
            count++;
        }
        System.out.println();
        System.out.println(count);
    }
}