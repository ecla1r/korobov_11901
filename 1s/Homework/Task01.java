/**
* @author Danil Korobov
* 11-901
* Task 01
*/
public class Task01 {

	public static void main (String [] args) {

		double x = Double.parseDouble(args[0]);

		x *= x;
		double p = x;
		p *= p;
		p *= p;
		p *= p;
		p *= p * p;
		p *= x;

		System.out.println(p);

	}
}