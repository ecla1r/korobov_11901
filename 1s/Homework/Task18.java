/**
* @author Danil Korobov
* 11-901
* Task 18
*/
import java.util.Scanner;

public class Task18 {

	public static void main (String [] args) {

		Scanner sca = new Scanner (System.in);

		System.out.println("Type 'x' in range of (-1;1)");
		double x = sca.nextDouble();
		double sum = -1;
		double pow = x * x;
		double fact = 2;
		double ev = 1;
		double k = 2;
		double factup = 3;
		final double EPS = 1e-9;

		while (Math.abs(pow / fact) > EPS) {
				sum += ev * pow / fact;
				pow *= pow;
				fact *= factup * (factup + 1);
				factup += 2;
				ev *= -1;
				k++;
			}
			
			System.out.println(sum);
		}
	}