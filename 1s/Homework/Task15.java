/**
* @author Danil Korobov
* 11-901
* Task 15
*/
import java.util.Scanner;

public class Task15 {

	public static void main (String [] args) {

		Scanner sca = new Scanner (System.in);

		System.out.println("Type 'x' in range of (-1;1)");
		double x = sca.nextDouble();
		double sum = 1;
		double fact = 1;
		double pow = x;
		double k = 1;
		final double EPS = 1e-9;

		while (Math.abs(pow / fact) > EPS) {
			sum += pow / fact;
			pow *= x;
			k++;
			fact *= k;
		}

		System.out.println(sum);

	}
}