/**
 * @author Danil Korobov
 * 11-901
 * Task 20
 */

public class Task20 {

    public static int compare(String s1, String s2) {
        int size = (s1.length() > s2.length()) ? s2.length() : s1.length();
        for (int i = 0; i < size; i++) {
            if (s1.charAt(i) > s2.charAt(i)) {
                return 2;
            }
            else if (s1.charAt(i) < s2.charAt(i)) {
                return 1;
            }
        }

        if (s1.length() < s2.length()) {
            return 1;
        }
        else if (s1.length() > s2.length()) {
            return 2;
        }
        else {
            return 0;
        }
    }

    public static void sort (String [] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (compare(array[j], array[j + 1]) == 2) {
                    String temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
}