/**
* @author Danil Korobov
* 11-901
* Task 02
*/
import java.util.Scanner;

public class Task02 {
	
	public static void main (String [] args) {
		
		int n = Integer.parseInt(args[0]);
		int s = 0;
		Scanner sca = new Scanner (System.in);

		for (int i = 0; i < n; i++) {
			int a = sca.nextInt();
			s += a;
		}

		System.out.println(s);

	}
}