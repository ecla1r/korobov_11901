/**
 * @author Danil Korobov
 * 11-901
 * Task 44
 */

public class ComplexMatrix2x2 {
    private ComplexNumber a11, a12, a21, a22;

    public ComplexMatrix2x2(ComplexNumber a11, ComplexNumber a12, ComplexNumber a21, ComplexNumber a22) {
        this.a11 = a11;
        this.a12 = a12;
        this.a21 = a21;
        this.a22 = a22;
    }

    public ComplexMatrix2x2() {
        this(new ComplexNumber(), new ComplexNumber(), new ComplexNumber(), new ComplexNumber());
    }

    public ComplexMatrix2x2(ComplexNumber c) {
        this(c, c, c, c);
    }

    public ComplexMatrix2x2 add(ComplexMatrix2x2 c) {
        return new ComplexMatrix2x2(this.a11.add(c.a11), this.a12.add(c.a12),
                                    this.a21.add(c.a21), this.a22.add(c.a22));
    }

    public ComplexMatrix2x2 mult(ComplexMatrix2x2 c) {
        return new ComplexMatrix2x2(this.a11.mult(c.a11), this.a12.mult(c.a12),
                                    this.a21.mult(c.a21), this.a22.mult(c.a22));
    }

    public ComplexNumber det() {
        return a11.mult(a22).sub(a12.mult(a21));
    }

    public ComplexVector2D multVector(ComplexVector2D v) {
        return new ComplexVector2D
                (v.getX().mult(this.a11).add(v.getY().mult(this.a21)),
                 v.getX().mult(this.a12).add(v.getY().mult(this.a22)));
    }
}