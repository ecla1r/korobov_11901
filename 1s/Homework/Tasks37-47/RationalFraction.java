/**
 * @author Danil Korobov
 * 11-901
 * Task 38
 */

public class RationalFraction {
    private int ch;
    private int zn;
    private int nod;

    public RationalFraction() {
        this(0, 0);
    }

    public RationalFraction(int ch, int zn) {
        this.ch = ch;
        this.zn = zn;
    }

    public int nod(int i, int j) {
        while (i * j > 0) {
            if (i > j)
                i %= j;
            else j %= i;
        }
        return i + j;
    }

    public void reduce() {
        this.ch /= nod(ch, zn);
        this.zn /= nod(ch, zn);
    }

    public RationalFraction add(RationalFraction r) {
        RationalFraction r1 = new RationalFraction(this.ch * r.zn + r.ch * this.zn, r.zn * this.zn);
        r1.reduce();
        return r1;
    }

    public void add2(RationalFraction r) {
        this.ch = this.ch * r.zn + r.ch * this.zn;
        this.zn = r.zn * this.zn;
        this.reduce();
    }

    public RationalFraction sub(RationalFraction r) {
        RationalFraction r1 = new RationalFraction(this.ch * r.zn - r.ch * this.zn, r.zn * this.zn);
        r1.reduce();
        return r1;
    }

    public void sub2(RationalFraction r) {
        this.ch = this.ch * r.zn - r.ch * this.zn;
        this.zn = r.zn * this.zn;
        this.reduce();
    }

    public RationalFraction mult(RationalFraction r) {
        RationalFraction r1 = new RationalFraction(this.ch * r.ch, r.zn * this.zn);
        r1.reduce();
        return r1;
    }

    public void mult2(RationalFraction r) {
        this.ch = this.ch * r.ch;
        this.zn = r.zn * this.zn;
        this.reduce();
    }

    public RationalFraction div(RationalFraction r) {
        RationalFraction r1 = new RationalFraction(this.ch * r.zn, this.zn * r.ch);
        r1.reduce();
        return r1;
    }

    public void div2(RationalFraction r ) {
        this.ch = this.ch * r.zn;
        this.zn = r.ch * this.zn;
        this.reduce();
    }

    public String toString() {
        return ch + " / " + zn;
    }

    public double value() {
        return (double) ch/zn;
    }

    public boolean equals(RationalFraction r) {
        r.reduce();
        this.reduce();
        return this.ch == r.ch && this.zn == r.zn;
    }

    public int numberPart() {
        return (int)(ch/zn);
    }

    public int getCh() {
        return ch;
    }

    public int getZn() {
        return zn;
    }
}