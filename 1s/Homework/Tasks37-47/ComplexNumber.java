/**
 * @author Danil Korobov
 * 11-901
 * Task 39
 */

public class ComplexNumber {

    private double a;
    private double b;

    public ComplexNumber() {
        this(0, 0);
    }

    public ComplexNumber(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public ComplexNumber add(ComplexNumber c) {
        return new ComplexNumber(this.a + c.a, this.b + c.b);
    }

    public void add2(ComplexNumber c) {
        this.a += c.a;
        this.b += c.b;
    }

    public ComplexNumber sub(ComplexNumber c) {
        return new ComplexNumber(this.a - c.a, this.b - c.b);
    }

    public void sub2(ComplexNumber c) {
        this.a -= c.a;
        this.b -= c.b;
    }

    public ComplexNumber multNum(double m) {
        return new ComplexNumber(this.a * m, this.b * m);
    }

    public void multNum2(double m) {
        this.a *= m;
        this.b *= m;
    }

    public ComplexNumber mult(ComplexNumber c) {
        return new ComplexNumber(this.a * c.a - this.b * c.b, this.a * c.b + this.b * c.a);
    }

    public void mult2(ComplexNumber c) {
        this.a = this.a * c.a - this.b * c.b;
        this.b = this.a * c.b + this.b * c.a;
    }

    public ComplexNumber div(ComplexNumber c) {
        return new ComplexNumber((this.a * c.a + this.b * c.b) / (c.a * c.a) + (c.b * c.b),
                (this.b * c.a - this.a * c.b) / (c.a * c.a) + (c.b * c.b));
    }

    public void div2(ComplexNumber c) {
        this.a = (this.a * c.a + this.b * c.b) / (c.a * c.a) + (c.b * c.b);
        this.b = (this.b * c.a - this.a * c.b) / (c.a * c.a) + (c.b * c.b);
    }

    public double length() {
        return Math.sqrt(this.a * this.a + this.b * this.b);
    }

    public String toString() {
        if(this.b < 0) {
            return this.a + " - " + this.b + "*i";
        }
        else if(this.b == 0) {
            return " " + this.a;
        }
        else {
            return this.a + " + " + this.b + "*i";
        }
    }

    public double arg() {
        if (this.a == 0) {
            if (this.b > 0) {
                return Math.PI / 2;
            }
            else if (this.b < 0) {
                return Math.PI * 3 / 2;
            }
            else return 0;
        }
        else return Math.atan(this.b / this.a);
    }

    public ComplexNumber pow(double n) {
        return new ComplexNumber(Math.round(Math.pow(this.length(), n) * Math.cos(n * this.arg())),
                Math.round(Math.pow(this.length(), n) * Math.sin(n * this.arg())));
    }

    public boolean equals(ComplexNumber c) {
        return this.a == c.a && this.b == c.b;
    }
}
