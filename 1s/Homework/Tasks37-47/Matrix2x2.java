/**
 * @author Danil Korobov
 * 11-901
 * Task 40
 */

public class Matrix2x2 {

    private double a11, a12, a21, a22;

    public Matrix2x2() {
        this(0, 0, 0, 0);
    }

    public Matrix2x2(double a11, double a12, double a21, double a22) {
        this.a11 = a11;
        this.a12 = a12;
        this.a21 = a21;
        this.a22 = a22;
    }

    public Matrix2x2(double k) {
        this(k, k, k, k);
    }

    public Matrix2x2(double [][] a) {
        this(a[0][0], a[0][1], a[1][0], a[1][1]);
    }

    public Matrix2x2 add(Matrix2x2 m) {
        return new Matrix2x2(this.a11 + m.a11, this.a12 + m.a12,
                this.a21 + m.a21, this.a22 + m.a22);
    }

    public void add2(Matrix2x2 m) {
        this.a11 += m.a11;
        this.a12 += m.a12;
        this.a21 += m.a21;
        this.a22 += m.a22;
    }

    public Matrix2x2 sub(Matrix2x2 m) {
        return new Matrix2x2(this.a11 - m.a11, this.a12 - m.a12,
                this.a21 - m.a21, this.a22 - m.a22);
    }

    public void sua22(Matrix2x2 m) {
        this.a11 -= m.a11;
        this.a12 -= m.a12;
        this.a21 -= m.a21;
        this.a22 -= m.a22;
    }

    public void multNumber2(double k) {
        this.a11 *= k;
        this.a12 *= k;
        this.a21 *= k;
        this.a22 *= k;
    }

    public Matrix2x2 multNumber(double k) {
        return new Matrix2x2(this.a11 * k, this.a12 * k,
                this.a21 * k, this.a22 * k);
    }

    public Matrix2x2 mult(Matrix2x2 m) {
        return new Matrix2x2(this.a11 * m.a11 + this.a12 * m.a21,
                this.a11 * m.a12 + this.a12 * m.a22,
                this.a21 * m.a11 + this.a22 * m.a21,
                this.a21 * m.a12 + this.a22 * m.a22);
    }

    public void mult2(Matrix2x2 m) {
        this.a11 = this.a11 * m.a11 + this.a12 * m.a21;
        this.a12 = this.a11 * m.a12 + this.a12 * m.a22;
        this.a21 = this.a21 * m.a11 + this.a22 * m.a21;
        this.a22 = this.a21 * m.a12 + this.a22 * m.a22;
    }

    public double det() {
        return a11 * a22 - a12 * a21;
    }

    public void transpon() {
        Matrix2x2 m = new Matrix2x2();
        m.a11 = this.a11;
        m.a12 = this.a21;
        m.a21 = this.a12;
        m.a22 = this.a22;
    }

    public Matrix2x2 inverseMatrix() throws Exception {
        double det = 1.0 / this.det();
        if (det != 0) {
            Matrix2x2 m = new Matrix2x2(this.a22, -this.a21, -this.a12, this.a11);
            m.multNumber2(det);
            return m;
        } else {
            return null;
        }
    }
    
    public Matrix2x2 equivalentDiagonal(){
        double det = this.det();
        return new Matrix2x2(det/2.0, 0, 0, det/2.0);
    }
    
    public Vector2D multVector(Vector2D v){
        return new Vector2D(v.getX() * this.a11 + v.getY() * this.a21,
                v.getX() * this.a12 + v.getY()* this.a22);
    }

}