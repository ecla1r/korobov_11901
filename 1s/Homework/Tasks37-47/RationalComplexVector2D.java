/**
 * @author Danil Korobov
 * 11-901
 * Task 46
 */

public class RationalComplexVector2D {
    private RationalComplexNumber x, y;

    public RationalComplexVector2D(RationalComplexNumber x, RationalComplexNumber y) {
        this.x = x;
        this.y = y;
    }

    public RationalComplexVector2D() {
        this(new RationalComplexNumber(), new RationalComplexNumber());
    }

    public RationalComplexVector2D add(RationalComplexVector2D rcv) {
        return new RationalComplexVector2D(this.x.add(rcv.x), this.y.add(rcv.y));
    }

    public String toString() {
        return "( " + x.toString() + " , " + y.toString() + " )";
    }

    public RationalComplexNumber scalarProduct(RationalComplexVector2D rcv) {
        return (this.x.mult(rcv.x).add(this.y.mult(rcv.y)));
    }
}