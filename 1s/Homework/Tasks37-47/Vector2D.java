/**
 * @author Danil Korobov
 * 11-901
 * Task 37
 */

public class Vector2D {

    private double x, y;

    public Vector2D() {
        this(0, 0);
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D add(Vector2D v) {
        return new Vector2D(this.x + v.x, this.y + v.y);
    }

    public void add2(Vector2D v) {
        this.x += v.x;
        this.y += v.y;
    }

    public Vector2D sub(Vector2D v) {
        return new Vector2D(this.x - v.x, this.y - v.y);
    }

    public void sub2(Vector2D v) {
        this.x -= v.x;
        this.y -= v.y;
    }

    public Vector2D mult(double k) {
        return new Vector2D(this.x * k, this.y * k);
    }

    public void multiply(double k) {
        this.x *= k;
        this.y *= k;
    }

    public double length() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    public double scalarProduct(Vector2D v) {
        return this.x * v.x + this.y * v.y;
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }

    public double cos(Vector2D v) {
        return (scalarProduct(v)/(this.length() * v.length()));
    }

    public boolean equals(Vector2D v) {
        if (this.x == v.x && this.y == v.y) {
            return true;
        }
        return false;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}