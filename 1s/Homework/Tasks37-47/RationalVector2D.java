/**
 * @author Danil Korobov
 * 11-901
 * Task 41
 */

public class RationalVector2D {

    private RationalFraction x, y;

    public RationalFraction getX() {
        return x;
    }

    public RationalFraction getY() {
        return y;
    }

    public RationalVector2D() {
        this(new RationalFraction(), new RationalFraction());
    }

    public RationalVector2D(RationalFraction rf1, RationalFraction rf2) {
        this.x = rf1;
        this.y = rf2;
    }

    public RationalVector2D add(RationalVector2D rv) {
        return new RationalVector2D(this.x.add(rv.x),
                this.y.add(rv.y));
    }

    public String toString() {
        return "( " + this.x.toString() + " , " + this.y.toString() + " )";
    }

    public double length(){
        return Math.sqrt(this.x.value() * this.x.value()
                + this.y.value() * this.y.value());
    }

    public RationalFraction scalarProduct(RationalVector2D rv){
        return this.x.mult(rv.x).add(this.y.mult(rv.y));
    }

    public boolean equals(RationalVector2D rv){
        return this.x.equals(rv.x) && this.y.equals(rv.y);
    }
}