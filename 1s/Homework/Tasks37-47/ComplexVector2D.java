/**
 * @author Danil Korobov
 * 11-901
 * Task 42
 */

public class ComplexVector2D {

    private ComplexNumber x, y;

    public ComplexNumber getX() {
        return x;
    }

    public ComplexNumber getY() {
        return y;
    }

    public ComplexVector2D() {
        this(new ComplexNumber(), new ComplexNumber());
    }

    public ComplexVector2D(ComplexNumber cv1, ComplexNumber cv2) {
        this.x = cv1;
        this.y = cv2;
    }

    public ComplexVector2D add(ComplexVector2D cv) {
        return new ComplexVector2D(this.x.add(cv.x), this.y.add(cv.y));
    }

    public String toString() {
        return "( " + this.x.toString() + ", " + this.y.toString();
    }

    public ComplexNumber scalarProduct(ComplexVector2D cv) {
        return this.x.mult(cv.x).add(this.y.mult(cv.y));
    }

    public boolean equals(ComplexVector2D cv) {
        return this.x.equals(cv.x) && this.y.equals(cv.y);
    }
}