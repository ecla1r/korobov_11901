/**
 * @author Danil Korobov
 * 11-901
 * Task 43
 */

public class RationalMatrix2x2 {

    prirvate RationalFraction a11, a12, a21, a22;

    public RationalMatrix2x2() {
        this(new RationalFraction(), new RationalFraction(),
                new RationalFraction(), new RationalFraction());
    }

    public RationalMatrix2x2(RationalFraction a11, RationalFraction a12,
                             RationalFraction a21, RationalFraction a22) {
        this.a11 = a11;
        this.a12 = a12;
        this.a21 = a21;
        this.a22 = a22;
    }

    public RationalMatrix2x2(RationalFraction r) {
        this(r, r, r, r);
    }

    public RationalMatrix2x2 add(RationalMatrix2x2 rm) {
        return new RationalMatrix2x2(this.a11.add(rm.a11), this.a12.add(rm.a12),
                this.a21.add(rm.a21), this.a22.add(rm.a22));
    }

    public RationalMatrix2x2 mult(RationalMatrix2x2 rm) {
        return new RationalMatrix2x2(this.a11.mult(rm.a11), this.a12.mult(rm.a12),
                this.a21.mult(rm.a21), this.a22.mult(rm.a22));
    }

    public RationalFraction det() {
        return a11.mult(a22).sub(a12.mult(a21));
    }

    public Rationalvector2D multvector(Rationalvector2D rv) {
        return new Rationalvector2D
                (rv.getX().mult(this.a11).add(rv.getY().mult(this.a21)),
                 rv.getX().mult(this.a12).add(rv.getY().mult(this.a22)));
    }
}