/**
 * @author Danil Korobov
 * 11-901
 * Task 45
 */

public class RationalComplexNumber {

    private RationalFraction a, b;

    public RationalComplexNumber(RationalFraction a, RationalFraction b) {
        this.a = a;
        this.b = b;
    }
    
    public RationalComplexNumber() {
        this(new RationalFraction(), new RationalFraction());
    }
    
    public RationalComplexNumber add(RationalComplexNumber c) {
        return new RationalComplexNumber(this.a.add(c.a), this.b.add(c.b));
    }
    
    public RationalComplexNumber sub(RationalComplexNumber c) {
        return new RationalComplexNumber(this.a.sub(c.a), this.b.sub(c.b));
    }
    
    public RationalComplexNumber mult(RationalComplexNumber c) {
        return new RationalComplexNumber
                (this.a.mult(c.a).sub(this.b.mult(c.b)),
                 this.a.mult(c.b).add(this.a.mult(c.a)));
    }
    
    public String toString() {
        if (b.getCh() < 0 && b.getZn() > 0 ||
            b.getCh() > 0 && b.getZn() < 0)
            return a.toString() + b.toString();
        else {
            return a.toString() + " + " + b.toString();
        }
    }
}