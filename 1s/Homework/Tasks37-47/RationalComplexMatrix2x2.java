/**
 * @author Danil Korobov
 * 11-901
 * Task 47
 */

public class RationalComplexMatrix2x2 {
    private RationalComplexNumber a11, a12, a21, a22;

    public RationalComplexMatrix2x2(RationalComplexNumber a11, RationalComplexNumber a12,
                                    RationalComplexNumber a21, RationalComplexNumber a22) {
        this.a11 = a11;
        this.a12 = a12;
        this.a21 = a21;
        this.a22 = a22;
    }

    public RationalComplexMatrix2x2() {
        this(new RationalComplexNumber(), new RationalComplexNumber(),
                new RationalComplexNumber(), new RationalComplexNumber());
    }

    public RationalComplexMatrix2x2(RationalComplexNumber n) {
        this(n, n, n, n);
    }

    public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 m) {
        return new RationalComplexMatrix2x2(this.a11.add(m.a11), this.a12.add(m.a12),
                this.a21.add(m.a21), this.a22.add(m.a22));
    }

    public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 rcm) {
        return new RationalComplexMatrix2x2(this.a11.mult(rcm.a11), this.a12.mult(rcm.a12),
                this.a21.mult(rcm.a21), this.a22.mult(rcm.a22));
    }

    public RationalComplexNumber det() {
        return a11.mult(a22).sub(a12.mult(a21));
    }

    public RationalComplexVector2D multVector(RationalComplexVector2D rcv) {
        return new RationalComplexVector2D
                (rcv.getX().mult(this.a11).add(rcv.getY().mult(this.a21)),
                        rcv.getX().mult(this.a12).add(rcv.getY().mult(this.a22)));
    }
}