/**
 * @author Danil Korobov
 * 11-901
 * Task 26
 */

import java.util.Scanner;
import java.util.regex.*;

public class Task26 {
    public static void main(String[] args) {
        Pattern p = Pattern.compile("0|((\\+|\\-)?[1-9][0-9]*)|" +
                                    "((\\+|\\-)?[0-9]\\.[0-9]*[1-9])|" +
                                    "([0-9]*\\([0-9]*[1-9][0-9]*\\))");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        Matcher m = p.matcher(s);
        System.out.println(m.matches());
    }
}