/**
* @author Danil Korobov
* 11-901
* Task 10
*/
public class Task10 {

	public static void main (String [] args) {

		double x = Double.parseDouble(args[0]);
		double y = Double.parseDouble(args[1]);

		if ((y * y <= 16 - x * x) & (y >= 0) & (y <= 4 - 2 * x)) {
			System.out.println("True");
		}
		else System.out.println("False");

	}
}