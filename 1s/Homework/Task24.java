/**
 * @author Danil Korobov
 * 11-901
 * Task 24
 */

public class Task24 {

    public static void replace (String s, String total) {
        int t = 0;
        s += "  ";
        for (int i = 0; i < s.length() - 2; i++) {
            if (s.charAt(i) == 'm' && s.charAt(i + 1) == 'o' && s.charAt(i + 2) == 'm') {
                total += "dad";
                i += 2;
            }
            else {
                total += s.charAt(i);
            }
        }
        System.out.println(total);
    }

    public static void main (String [] args) {
        String s = "momYEEEETdadYEEEETbruhYEEEETmoomdadmomBRUHmom";
        String total = "";
        replace(s, total);
    }
}