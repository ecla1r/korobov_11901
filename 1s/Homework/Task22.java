/**
 * @author Danil Korobov
 * 11-901
 * Task 22
 */

public class Task22 {

    public static void countLetters(String s, int [] count) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z') {
                count[(int)(s.charAt(i)) - (int)('A')]++;
            }
            else if(s.charAt(i) <= 'z' && s.charAt(i) >= 'a') {
                count[(int)(s.charAt(i)) - (int)('a')]++;
            }
        }
    }

    public static void words(String s, int [] count) {
        String [] words = s.split(" ");
        for (int i = 0; i < words.length; i++) {
            countLetters(words[i], count);
        }
    }

    public static void main(String [] args) {
        String s = "Certified Bruh Moment";
        int [] output = new int[26];
        String letterList = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        words(s, output);
        int temp = 0;
        for (int i = 0; i < output.length; i++) {
            System.out.println(letterList.charAt(i) + " - " + output[i]);
        }
    }
}