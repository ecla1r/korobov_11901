/**
* @author Danil Korobov
* 11-901
* Task 14
*/
public class Task14 {

	public static void main (String [] args) {

		double x = Double.parseDouble(args[0]);
		double y = 0;

		if (x > 2) {
			y = (x * x - 1) / (x + 2);
		}

		else if (x <= 0) {
			 y = (1 + 2 * x) * x * x;
		}

		else y = (x * x - 1) * (x + 2);

		System.out.println(y);
	}
}