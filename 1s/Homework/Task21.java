/**
 * @author Danil Korobov
 * 11-901
 * Task 21
 */

public class Task21 {

    public static int caps(String s) {
        int caps = 0;

        String [] words = s.split(" ");
        for (int i = 0; i < words.length; i++) {
            if (words[i].charAt(0) >= 'A' && words[i].charAt(0) <= 'Z') {
                caps++;
            }
        }
        return caps;
    }
}