/**
 * @author Danil Korobov
 * 11-901
 * Task 27
 */

import java.util.Scanner;
import java.util.regex.*;

public class Task27 {

    public static void counter(String regex, String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].matches(regex)) {
                System.out.println(i);
            }
        }
    }

    public static void main(String [] args) {
        String regex = "0+|1+|(1(01)*(0?))|(0(10)*(1?))";
        String [] array = new String[10];
        Scanner sc = new Scanner (System.in);
        for (int i = 0; i < array.length; i++) {
            array[i] = sc.nextLine();
        }
        counter(regex, array);
    }
}
