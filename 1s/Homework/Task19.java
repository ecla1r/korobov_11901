/**
 * @author Danil Korobov
 * 11-901
 * Task 19
 */

public class Task19 {

    public static int compare(String s1, String s2) {
        int size = (s1.length() > s2.length()) ? s2.length() : s1.length();
        for (int i = 0; i < size; i++) {
            if (s1.charAt(i) > s2.charAt(i)) {
                return 2;
            }
            else if (s1.charAt(i) < s2.charAt(i)) {
                return 1;
            }
        }

        if (s1.length() < s2.length()) {
            return 1;
        }
        else if (s1.length() > s2.length()) {
            return 2;
        }
        else {
            return 0;
        }
    }
}