/**
* @author Danil Korobov
* 11-901
* Task 2 (1 variant)
*/
import java.util.Scanner;

public class Task2 {

	public static void main(String [] args) {

		System.out.println("Type n and x");

		Scanner scn = new Scanner (System.in);
		Scanner scx = new Scanner (System.in);
		int n = scn.nextInt();
		double x = scx.nextDouble();
		double pow = x * x * x;
		double zn = -1;
		double fact = 1;
		double sum = 0;

		for (int k = 1; k <= n; k++) {
			sum += (zn * pow / (fact + x));
			pow *= x * x;
			zn *= -1;
			fact *= (k+1);
		}
		System.out.println(sum);
	}
}