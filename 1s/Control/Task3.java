/**
* @author Danil Korobov
* 11-901
* Task 3 (1 variant)
*/
import java.util.Scanner;

public class Task3 {

	public static void main(String [] args) {

		int n = Integer.parseInt(args[0]);
		int s = 0;
		int temp = 0;
		Scanner sca = new Scanner (System.in);

		int [] f = new int [10];
		for (int j = 0; j < 10; j++) {
			f[j] = 0;
		}

		for (int k = 0; k < n; k++) {
			System.out.print("Type Array ");
			System.out.println(k+1);
			int [] a = new int [n];
			for (int i = 0; i < n; i++) {
				a[i] = sca.nextInt();
				temp = Math.abs(a[i]);

				if (a[i] == 0) {
					f[0]++;
				}
				else
				while (temp > 0) {
					s = temp % 10;
					f[s]++;
					temp /= 10;
				}
			}
		}
		for (int l = 0; l < 10; l++) {
			System.out.print(l);
			System.out.print(": ");
			System.out.print(f[l]);
			System.out.println(" times");
		}
	}
}