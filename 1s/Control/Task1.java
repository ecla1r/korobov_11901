/**
* @author Danil Korobov
* 11-901
* Task 1 (1 variant)
*/
import java.util.Scanner;

public class Task1 {

	public static void main (String [] args) {

		int n = Integer.parseInt(args[0]);
		int [] a = new int [n];
		int count = 0;
		Scanner sc = new Scanner (System.in);

		for (int i = 0; i < n; i++) {

			a[i] = sc.nextInt();
			while (a[i] > 0) {
				if (a[i] % 2 == 0) {
					a[i] /= 10;
				}
				else break;
			}
			if (a[i] == 0) {
				count++;
			}
		}
		if (count == 0) {
			System.out.println("No");
		}
		else System.out.println("Yes");
	}
}